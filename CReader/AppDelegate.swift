
import UIKit
import WeexSDK
import UserNotifications
import Firebase


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, WXApiDelegate {

    var window: UIWindow?



    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
		
		configWeex()
		
		configShare()
		
//		configMobPush()
		
//        guard let gai = GAI.sharedInstance() else {
//            assert(false, "Google Analytics not configured correctly")
//            return true//Base on your function return type, it may be returning something else
//        }
//        gai.tracker(withTrackingId: "UA-43690716-8")
//        // Optional: automatically report uncaught exceptions.
//        gai.trackUncaughtExceptions = true
//
//        // Optional: set Logger to VERBOSE for debug information.
//        // Remove before app release.
//        gai.logger.logLevel = .verbose;
//
//        //阿里百川的SDK
//        ALBBSDK.sharedInstance().asyncInit({
//            print("success")
//        }) { (error) in
//            print("failure : ",error!)
//        }
//        //第一次获得GMO的visitorID
//        AnalyticsTracker.shared.getGMOVisitorId()
//
//        //微信API
//		WXApi.registerApp("wx10c861b22505cf46")
		
		
		
        return true
    }
	

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
		
		
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
	
	//  微信回调
	func onResp(_ resp: BaseResp!) {
//		var strTitle = "支付结果"
//		var strMsg = "what:\(resp.errCode)"
		print(resp.errCode)
		print("qingsongzhifu")
		//  微信支付回调
		if resp.isKind(of: BaseResp.self)
		{
//			print("retcode = \(resp.errCode), retstr = \(resp.errStr)")
			switch resp.errCode
			{
			//  支付成功
			case 0 :
				NotificationCenter.default.post(name: NotificationKey.openAlertThanks, object: nil)
			//  支付失败
			default:
//				WXPayFail
				break
			}
		}
		//  微信登录回调
//		if resp.errCode == 0 && resp.type == 0{//授权成功
//			let response = resp as! SendAuthResp
//			NotificationCenter.default.post(name: NSNotification.Name(rawValue: "WXLoginSuccessNotification"), object: response.code)
//		}
	}



}


extension AppDelegate {
	
	fileprivate func configWeex(){
		WXAppConfiguration.setAppGroup("CReader")
		WXAppConfiguration.setAppName("CReader")
		WXAppConfiguration.setAppVersion("1.0.0")
		
		WXLog.setLogLevel(WeexLogLevel.all)
		
		// register event module
		WXSDKEngine.registerModule("event", with: NSClassFromString("WXEventModule"))
		
		// register extra component
		WXSDKEngine.registerComponent("commonFun", with: NSClassFromString("WXCommonFunComponent"));
		WXSDKEngine.registerComponent("webView", with: NSClassFromString("WXWebViewComponent"));
		
//		WXSDKEngine.registerComponent("LoadingDots", with: WXLoadingComponent.self)
		
		
		// register handler
		WXSDKEngine.registerHandler(WXImageLoaderDefaultImplement(), with:NSProtocolFromString("WXImgLoaderProtocol"))
		
		//init WeexSDK
		WXSDKEngine.initSDKEnvironment()
		
		//判断是否第一次运行程序
		isFirstLaunch()
	}
	
	
	fileprivate func isFirstLaunch(){
		if UserDefaultsUtils.hasKey(key: SETTINGS_APP_FIRST_LAUNCH) {
			let vc: TabbarViewController = TabbarViewController()
			window?.rootViewController = UINavigationController.init(rootViewController: vc)
		}else {
			let vc: WXViewController = WXViewController()
			vc.url = URL(string: WeexBundleFolder + "introduce.js")
			window?.rootViewController = UINavigationController.init(rootViewController: vc)
			UserDefaultsUtils.saveBoolValue(value: true, key: SETTINGS_APP_FIRST_LAUNCH)
		}
	}
	
	fileprivate func configShare(){
		ShareSDK.registPlatforms { register in
			register?.setupWeChat(withAppId: "wx10c861b22505cf46", appSecret: "bee3023d717435e8afac2a7cfdc9ec5a")
			register?.setupSinaWeibo(withAppkey: "2813894757", appSecret: "1f8b1233e3177b364b2b6229a0bb05dd", redirectUrl: "http://www.sharesdk.cn")
			register?.setupQQ(withAppId: "1101022213", appkey: "2elVLmSZ8en2WgMb")
		}
	}
	

	
	
}



