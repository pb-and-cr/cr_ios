//
//  MessageGateway.m
//  Whatsapp
//
//  Created by Rafael Castro on 7/4/15.
//  Copyright (c) 2015 HummingBird. All rights reserved.
//

#import "MessageGateway.h"
#import "MessageLocalStorage.h"
#import "AFHTTPSessionManager.h"
//#import "Properties.h"
#import "AppConfig.h"//#import "Constants.h"
#import "NetworkManager.h"
//#import "AppDelegate.h"
#import "AFNetWorking.h"

@interface MessageGateway()
@property (strong, nonatomic) NSString *messagesUrlStr;
@property (strong, nonatomic) NSString *firstLoadOldMessagesDate;
@property (strong, nonatomic) MessageLocalStorage *localStorage;
@end


@implementation MessageGateway

+(id)sharedInstance
{
    static MessageGateway *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}
-(id)init
{
    self = [super init];
    if (self)
    {
//        self.messages_to_send = [[NSMutableArray alloc] init];
        self.messagesUrlStr = GMO_CN_MESSAGES;
        _localStorage = [MessageLocalStorage sharedInstance];
    }
    return self;
}



/*
 messages =     (
 {
 date = "2015-09-01 06:30:38";
 direction = in;
 id = 20524;
 message = "I am a Chinese ";
 read = 0;
 },
 {
 date = "2015-09-01 06:30:02";
 direction = in;
 id = 20523;
 message = "Hello ";
 read = 0;
 }
 );
 */
-(NSArray*)loadOldMessagesWithPageIndex:(NSUInteger)index
{
    NSLog(@"loadOldMessagesWithPageIndex %lu",(unsigned long)index);
    NSArray *messages = [self.localStorage getMessagesByPage:index Count:10];
    return messages;
}

-(void)loadOldMessagesWithPageIndex:(NSUInteger)index IsOnlyNew:(NSString*)isOnlyNew
{
    NSNumber *pageNumber = [NSNumber numberWithUnsignedInteger:index];
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:PropGMOUserKey] length]>0) {
        NSString *toDate;
        if (_firstLoadOldMessagesDate.length != 0 && [isOnlyNew isEqualToString:@"false"]) {
            toDate = _firstLoadOldMessagesDate;
        }else if (_firstLoadOldMessagesDate.length == 0 && [isOnlyNew isEqualToString:@"false"]) {
            toDate = [self getUTCFormateDate:[NSDate date]];
        }else if (_firstLoadOldMessagesDate.length != 0 && [isOnlyNew isEqualToString:@"true"]) {
            toDate = [self getUTCFormateDate:[NSDate date]];
        }else{
            toDate = [self getUTCFormateDate:[NSDate date]];
        }
//        AFHTTPSessionManager *network = [AFHTTPSessionManager manager];
        NetworkManager *network = [NetworkManager manager];
        NSDictionary *postInfo = @{
                                   @"user_key":[[NSUserDefaults standardUserDefaults] objectForKey:PropGMOUserKey],
//                                   @"from_date":[self getUTCFormateDate:[NSDate dateWithTimeIntervalSinceNow:-24*60*60]],
                                   @"to_date":toDate,
                                   @"only_new":@"false",
                                   @"count":@5,
                                   @"page":pageNumber
                                   };
//        network.securityPolicy.allowInvalidCertificates = YES;
        [network GET:self.messagesUrlStr parameters:postInfo
             success:^(NSURLSessionDataTask *task, id responseObject) {
                 if ([responseObject isKindOfClass:[NSDictionary class]]) {
                     NSLog(@"loadOldMessagesWithPageIndex API %@",responseObject);
                     NSArray *tmpArr = [NSArray arrayWithArray:responseObject[@"messages"]];
                     if (tmpArr.count>0) {
                         NSMutableArray *messages = [@[] mutableCopy];
                         for (int i = 0; i<tmpArr.count; i++) {
                             NSDictionary *tmpDict = [NSDictionary dictionaryWithDictionary:tmpArr[i]];
                             Message *message = [[Message alloc] init];
                             message.serverId = [tmpDict objectForKey:@"id"];
                             message.text = [tmpDict objectForKey:@"message"];
                             message.direction = [tmpDict objectForKey:@"direction"];
                             message.isRead = [[tmpDict objectForKey:@"read"] boolValue];
                             //                         message.sender = MessageSenderSomeone;
                             if ([[tmpDict objectForKey:@"read"] boolValue] == YES) {
                                 message.status = MessageStatusRead;
                             }else {
                                 if ([[tmpDict objectForKey:@"direction"] isEqualToString:@"out"]) {
                                     message.status = MessageStatusReceived;
                                 }else {
                                     if ([isOnlyNew isEqualToString:@"true"]) {
                                         break;
                                     }
                                     message.status = MessageStatusSent;
                                 }
                             }
                             
                             //message.chat_id = _chat.identifier;
                             message.date = [self dateFromString:[tmpDict objectForKey:@"date"]];
                             [messages addObject:message];
                         }
                         if (self.delegate)
                         {
                             [self.delegate gatewayDidReceiveMessages:messages];
                         }
                         NSArray *unreadMessages = [self queryUnreadMessagesInArray:messages];
                         if (unreadMessages.count>0) {
                             [self updateStatusToReadInArray:unreadMessages];
                         }
                     }else {
                         if (self.delegate)
                         {
                             [self.delegate gatewayDidNoMessagesWithIsSucceed:YES];
                         }
                     }
                 }
             }
             failure:^(NSURLSessionDataTask *task, NSError *error) {
                 NSLog(@"API error: %@", error.description);
                 if (self.delegate)
                 {
                     [self.delegate gatewayDidNoMessagesWithIsSucceed:NO];
                 }
             }];
    }else {
//        [(AppDelegate*)[[UIApplication sharedApplication] delegate] getGMOUserKey];
        if (self.delegate) {
            [self.delegate gatewayDidNoMessagesWithIsSucceed:NO];
        }
    }
}

-(void)loadNewMessage
{
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:PropGMOUserKey] length]>0) {
        NSString *toDate = [self getUTCFormateDate:[NSDate date]];
        NetworkManager *network = [NetworkManager manager];
        NSDictionary *postInfo = @{
                                   @"user_key":[[NSUserDefaults standardUserDefaults] objectForKey:PropGMOUserKey],
                                   @"to_date":toDate,
                                   @"only_new":@"true",
                                   @"count":@10,
                                   @"page":@1
                                   };
        [network GET:self.messagesUrlStr parameters:postInfo
             success:^(NSURLSessionDataTask *task, id responseObject) {
                 if ([responseObject isKindOfClass:[NSDictionary class]]) {
                     NSLog(@"loadNewMessage API %@",responseObject);
                     NSArray *tmpArr = [NSArray arrayWithArray:responseObject[@"messages"]];
                     if (tmpArr.count>0) {
                         NSMutableArray *messages = [@[] mutableCopy];
                         for (int i = 0; i<tmpArr.count; i++) {
                             NSDictionary *tmpDict = [NSDictionary dictionaryWithDictionary:tmpArr[i]];
                             Message *message = [[Message alloc] init];
                             message.serverId = [NSString stringWithFormat:@"%@",[tmpDict objectForKey:@"id"]] ;
                             message.text = [tmpDict objectForKey:@"message"];
                             message.direction = [tmpDict objectForKey:@"direction"];
                             message.isRead = [[tmpDict objectForKey:@"read"] boolValue];
                             message.date = [self dateFromString:[tmpDict objectForKey:@"date"]];
                             //                         message.sender = MessageSenderSomeone;
                             if ([[tmpDict objectForKey:@"read"] boolValue] == NO && [[tmpDict objectForKey:@"direction"] isEqualToString:@"out"])
                             {
                                 message.status = MessageStatusReceived;
                                 [_localStorage storeMessage:message];
                                 
                             }
                             [messages addObject:message];
                         }
                         NSLog(@"%i",messages.count);
                         if (self.delegate)
                         {
                             [self.delegate gatewayDidReceiveMessages:messages];
                         }else {
                             NSMutableDictionary *userInfo = [NSMutableDictionary dictionary];
                             userInfo[@"count"] = [NSString stringWithFormat:@"%lu",(unsigned long)messages.count];
                             [[NSNotificationCenter defaultCenter] postNotificationName:@"showRedPoint" object:self userInfo:userInfo];
                         }
                         if (messages.count>0) {
                             [self updateStatusToReadInArray:messages];
                         }
                     }else {
                         if (self.delegate)
                         {
                             [self.delegate gatewayDidNoMessagesWithIsSucceed:YES];
                         }
                     }
                 }
             }
             failure:^(NSURLSessionDataTask *task, NSError *error) {
                 NSLog(@"loadNewMessage API error: %@", error.description);
                 if (self.delegate) {
                     [self.delegate gatewayDidNoMessagesWithIsSucceed:NO];
                 }
             }];
    }else {
//        [(AppDelegate*)[[UIApplication sharedApplication] delegate] getGMOUserKey];
        //        if (self.delegate) {
        //            [self.delegate gatewayDidNoMessagesWithIsSucceed:NO];
        //        }
    }
}

-(NSArray *)queryUnreadMessagesInArray:(NSArray *)array
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.status == %d", MessageStatusReceived];
    return [array filteredArrayUsingPredicate:predicate];
}

-(void)updateStatusToReadInArray:(NSArray *)unreadMessages
{
    NSMutableArray *read_ids = [[NSMutableArray alloc] init];
    for (Message *message in unreadMessages)
    {
        
        message.status = MessageStatusRead;
        [read_ids addObject:message.serverId];
    }
    [self putMessageAsRead:read_ids];
}


-(void)dismiss
{
    self.delegate = nil;
}


#pragma mark - Exchange data with API

-(void)sendMessage:(Message *)message
{
    //TODO
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:PropGMOUserKey] length]>0) {
        message.status = MessageStatusSending;
        if (self.delegate && [self.delegate respondsToSelector:@selector(gatewayDidUpdateStatusForMessage:)])
        {
            [self.delegate gatewayDidUpdateStatusForMessage:message];
        }
//        AFHTTPSessionManager *network = [AFHTTPSessionManager manager];
        NetworkManager *network = [NetworkManager manager];
        NSDictionary *postInfo = @{
                                   @"user_key":[[NSUserDefaults standardUserDefaults] objectForKey:PropGMOUserKey],
                                   @"message":message.text,
                                   @"language":@"zh"
                                   };
//        network.securityPolicy.allowInvalidCertificates = YES;
        [network POST:self.messagesUrlStr parameters:postInfo
             success:^(NSURLSessionDataTask *task, id responseObject) {
                 if ([responseObject isKindOfClass:[NSDictionary class]]) {
                     NSLog(@"sendMessage API %@",responseObject);
                     if (![message.serverId isEqualToString:@"-1"]) {
                         if ([responseObject[@"success"] integerValue] == 1) {
                             message.status = MessageStatusSent;
                         }else {
                             message.status = MessageStatusFailed;
                         }
                         if (self.delegate && [self.delegate respondsToSelector:@selector(gatewayDidUpdateStatusForMessage:)])
                         {
                             [self.delegate gatewayDidUpdateStatusForMessage:message];
                         }
                         message.serverId = [responseObject objectForKey:@"id"];
                         [_localStorage storeMessage:message];
                     }
                 }
             }
             failure:^(NSURLSessionDataTask *task, NSError *error) {
                 NSLog(@"API error: %@", error.description);
                 if (![message.serverId isEqualToString:@"-1"]) {
                     message.status = MessageStatusFailed;
                     if (self.delegate && [self.delegate respondsToSelector:@selector(gatewayDidUpdateStatusForMessage:)])
                     {
                         [self.delegate gatewayDidUpdateStatusForMessage:message];
                     }
                     if (![message.serverId isEqualToString:@"-1"]) {
                         [_localStorage storeMessage:message];
                     }
                 }
             }];
    }else {
//        [(AppDelegate*)[[UIApplication sharedApplication] delegate] getGMOUserKey];
    }
}

-(void)putMessageAsRead:(NSArray *)read_ids
{
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:PropGMOUserKey] length]>0) {
        
        //        AFHTTPSessionManager *network = [AFHTTPSessionManager manager];
        NetworkManager *network = [NetworkManager manager];
        NSDictionary *postInfo = @{
                                   @"user_key":[[NSUserDefaults standardUserDefaults] objectForKey:PropGMOUserKey],
                                   @"ids":read_ids
                                   };
        //        network.securityPolicy.allowInvalidCertificates = YES;
        [network PUT:self.messagesUrlStr parameters:postInfo
             success:^(NSURLSessionDataTask *task, id responseObject) {
                 //                  if ([responseObject isKindOfClass:[NSDictionary class]]) {
                 NSLog(@"putMessageAsRead API %@",responseObject);
                 //                  }
             }
             failure:^(NSURLSessionDataTask *task, NSError *error) {
                 NSLog(@"putMessageAsRead API error: %@", error.description);
                 
             }];
    }else {
//        [(AppDelegate*)[[UIApplication sharedApplication] delegate] getGMOUserKey];
    }
}

-(void)sendReadStatusToMessages:(NSArray *)message_ids
{
    if ([message_ids count] == 0) return;
    //TODO
}
-(void)sendReceivedStatusToMessages:(NSArray *)message_ids
{
    if ([message_ids count] == 0) return;
    //TODO
}

-(NSString *)getUTCFormateDate:(NSDate *)localDate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    [dateFormatter setTimeZone:timeZone];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateString = [dateFormatter stringFromDate:localDate];
    return dateString;
}

- (NSDate *)dateFromString:(NSString *)dateString
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    [dateFormatter setTimeZone:timeZone];
    [dateFormatter setDateFormat: @"yyyy-MM-dd HH:mm:ss"];
    NSDate *destDate= [dateFormatter dateFromString:dateString];
    return destDate;
    
}

#pragma mark userInfo put mathod
-(void)putUserInfoWithName:(NSString*)name Decision:(NSString*)decision AndResultCallback:(void(^)(BOOL result))resultBlock{
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:PropGMOUserKey] length]>0) {
        NetworkManager *network = [NetworkManager manager];
        NSDictionary *postInfo;
        if (decision == nil) {
            postInfo = @{
                            @"user_key":[[NSUserDefaults standardUserDefaults] objectForKey:PropGMOUserKey],
                            @"name":name
                        };
        }else{
            postInfo = @{
                            @"user_key":[[NSUserDefaults standardUserDefaults] objectForKey:PropGMOUserKey],
                            @"name":name,
                            @"decision":decision
                         };
        }
        
        //    network.securityPolicy.allowInvalidCertificates = YES;
        NSString *urlStr = GMO_CN_USERS;
        [network PUT:urlStr parameters:postInfo success:^(NSURLSessionDataTask *task, id responseObject) {
            if ([responseObject isKindOfClass:[NSDictionary class]]) {
                NSLog(@"putUserInfoWithName API put %@",responseObject);
                if ([responseObject[@"success"] integerValue] == 1) {
                    [[NSUserDefaults standardUserDefaults] setObject:name forKey:PropGMOUserName];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    resultBlock(YES);
                }else{
                    resultBlock(NO);
                }
            }
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            NSLog(@"putUserInfoWithName API put error: %@", error.description);
            resultBlock(NO);
        }];
    }else {
//        [(AppDelegate*)[[UIApplication sharedApplication] delegate] getGMOUserKey];
        resultBlock(NO);
    }
    
}

-(void)submitFindChurchInfoWithName:(NSString*)name Mobile:(NSString*)mobile Address:(NSString*)address AndResultCallback:(void(^)(BOOL result))resultBlock
{
    NSDictionary *postInfo   = @{
                                 @"name":name,
                                 @"phone":mobile,
                                 @"address":address,
                                 @"app_name":@"jdt",
                                 @"category_id":@"4"
                                 };
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];//
    NSString *url = [NSString stringWithFormat:@"%@/api/chat/enter",kHostingSite];
    [manager POST:url parameters:postInfo success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //        NSLog(@"submitFindChurchInfoWithName API %@",responseObject);
        resultBlock(YES);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //        NSLog(@"submitFindChurchInfoWithName API error: %@", error.description);
        resultBlock(NO);
    }];
}

-(void) getGMOUserKey
{
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:PropGMOUserKey] length] > 0) {
        return;
    }
    
    NetworkManager *network = [NetworkManager manager];
    NSDictionary *postInfo = @{@"app_key":GMO_API_KEY,@"platform":@"iOS"};
    NSString *urlStr = GMO_CN_USERS;
    [network POST:urlStr parameters:postInfo
          success:^(NSURLSessionDataTask *task, id responseObject) {
              if ([responseObject isKindOfClass:[NSDictionary class]]) {
                  [[NSUserDefaults standardUserDefaults] setObject:[(NSDictionary*)responseObject objectForKey:@"user_key"] forKey:PropGMOUserKey];
                  [[NSUserDefaults standardUserDefaults] synchronize];
              }
              NSLog(@"getGMOUserKey success %@",[(NSDictionary*)responseObject objectForKey:@"user_key"]);
          }
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              NSLog(@"getGMOUserKey failure %@",error);
          }];
}

@end
