//
//  NetworkManager.m
//  SSL
//
//  Created by Ilter Cengiz on 15/07/14.
//  Copyright (c) 2014 Ilter Cengiz. All rights reserved.
//

#import "NetworkManager.h"
#import "AFURLConnectionOperation.h"

@interface NetworkManager ()

@end

@implementation NetworkManager

+ (instancetype)manager {
    
    static NetworkManager *_manager = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        _manager = [[NetworkManager alloc] initWithBaseURL:nil];
        _manager.requestSerializer = [AFHTTPRequestSerializer serializer];
        _manager.responseSerializer = [AFHTTPResponseSerializer serializer];

        AFSecurityPolicy *securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeCertificate ];
        // 2.设置证书模式
        NSString * cerPath = [[NSBundle mainBundle] pathForResource:@"xinxiwangCert" ofType:@"cer"];
        NSData * cerData = [NSData dataWithContentsOfFile:cerPath];
        [securityPolicy setPinnedCertificates:[[NSArray alloc] initWithObjects:cerData, nil]];
            [securityPolicy setAllowInvalidCertificates:YES];
            securityPolicy.validatesDomainName = NO;
//            securityPolicy.validatesCertificateChain = NO;
            _manager.responseSerializer=[AFJSONResponseSerializer serializer];
            [_manager setSecurityPolicy:securityPolicy];
            [_manager.requestSerializer setTimeoutInterval:20.0];
//        }
    });
    
    return _manager;
    
}

@end
