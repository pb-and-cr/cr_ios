//
//  AppConfig.h
//  CanonIraq
//  用于项目共有的宏配置
//  Created by onskyline on 14-6-10.
//  Copyright (c) 2014年 onskyline. All rights reserved.
//

#import <Foundation/Foundation.h>

//IOS版本号
#define versionNO [[[UIDevice currentDevice] systemVersion] doubleValue]
//屏幕的宽
#define screenWidth  [[UIScreen mainScreen] bounds].size.width
//屏幕的高
#define screenHight [[UIScreen mainScreen] bounds].size.height
//适配的frame
#define fitFrame versionNO>=7.0?CGRectMake(0, [[UIScreen mainScreen]bounds].size.height -[[UIScreen mainScreen]applicationFrame].size.height , [[UIScreen mainScreen]bounds].size.width ,[[UIScreen mainScreen]applicationFrame].size.height):[[UIScreen mainScreen]bounds];

#define screenFrame [[UIScreen mainScreen] bounds]

#define SYSREM_VERSION [[UIDevice currentDevice] systemVersion].floatValue

#define kHostingSite @"https://wx.biblemooc.net"

//文件是否存在
#define isfileExist @"fileExist"

//文件是否更新
#define isUpdate @"updateState"

//书籍的版本
#define bookVersion @"bookVersion"

#define kAPPName    [[[NSBundle mainBundle]infoDictionary] objectForKey:@"CFBundleDisplayName"]
#define kDocuments  [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject]

//数据库地址，用于保存书签等
#define kEPubDBPath  [NSString stringWithFormat:@"%@/%@", kDocuments, @"creader.db"]

//GMO需要的API接口
#define GMO_API_BASE_URL @"https://commchannels.globalmediaoutreach.com/api/app"
#define GMO_USERS (GMO_API_BASE_URL @"/users")
#define GMO_MESSAGES (GMO_API_BASE_URL @"/messages")
#define GMO_NOTIFICATIONS (GMO_API_BASE_URL @"/notifications")
#define GMO_EVENTS @"https://stats.globalmediaoutreach.com/api/v2/events/app"


//GMO_CN需要的API接口
#define GMO_CN_API_BASE_URL @"https://xinxiwang.me"
#define GMO_CN_USERS (GMO_CN_API_BASE_URL @"/users")
#define GMO_CN_MESSAGES (GMO_CN_API_BASE_URL @"/messages")
#define GMO_CN_NOTIFICATIONS (GMO_CN_API_BASE_URL @"/notifications")
#define GMO_CN_VISITOR (GMO_CN_API_BASE_URL @"/notifications")

#define GMO_CN_EVENTS (GMO_CN_API_BASE_URL @"/events")

#define GMO_API_KEY @"TsvdyWAlkjmi3MJfdkqCbihc8kNiCUrfkq8zshrditafPEu4DbF3E1PZmNBGmyT"

#define PropGMOUserKey @"GMOUser.key"
#define PropGMOUserName @"GMOUser.name"

#define PropDeviceLanguage @"device.language"
#define PropDeviceToken @"device.token"
#define PropAppHasLaunchedBefore @"app.hasLaunchedBefore"
#define PropDeviceCarrier @"device.carrier"
#define PropDeviceNotificationIsSetup @"device.notification.isSetup"
#define PropUserSetFontSize @"userSet.font.size"
#define PropGMOVisitorId @"gmo.visitor.id"
#define PropGMOUserFirstChat @"gmo.user.first.chat"

#define PropAppUpdateIgnore @"app.update.ignore"

// 开发版 BK2urkkGh57Ub2OyzX7bO6  发布版 JumaZubjX08bWWdQNiiF43
#define kGtAppId           @"JumaZubjX08bWWdQNiiF43"

// 开发版 qp6ORTWhub8YR0HgYwoG45  发布版 2FqUS9GrMq6z2WscYzQ127
#define kGtAppKey          @"2FqUS9GrMq6z2WscYzQ127"

// 开发版 HlhSjXzpZk9dgfnPzd38AA  发布版 VORRZAKNHDAAif7v7JGnK5
#define kGtAppSecret       @"VORRZAKNHDAAif7v7JGnK5"
