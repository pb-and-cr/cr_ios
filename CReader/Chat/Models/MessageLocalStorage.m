//
//  MessageLocalStorage.m
//  Whatsapp
//
//  Created by Rafael Castro on 7/24/15.
//  Copyright (c) 2015 HummingBird. All rights reserved.
//

#import "MessageLocalStorage.h"
#import "FMDatabase.h"
#import "FMDatabaseAdditions.h"

@interface MessageLocalStorage ()

@property (nonatomic,readonly) NSString *databasePath;
@property (nonatomic,readonly) FMDatabase *database;
@property (strong, nonatomic) NSMutableDictionary *mapChatToMessages;
@end


@implementation MessageLocalStorage

+(id)sharedInstance
{
    static MessageLocalStorage *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}
-(id)init
{
    self = [super init];
    if (self)
    {
        self.mapChatToMessages = [[NSMutableDictionary alloc] init];
        [self closeDatabase];
    }
    return self;
}

- (void)dealloc
{
    [self closeDatabase];
}

- (void)closeDatabase
{
    if (_database) {
        [_database close], _database = nil;
    }
}

-(void)storeMessage:(Message *)message
{
    int userId = [_database intForQuery:@"SELECT id FROM UserMessages WHERE server_id=?",message.serverId];
    if (userId == 0) {
        [self insertMessage: message];
    }
    
}

-(void)insertMessage:(Message *)message
{
    NSString *sql = [NSString stringWithFormat:@"insert into UserMessages (chat_id, status, text, date, heigh, is_read, direction, server_id) values ( %li, %li,\"%@\",%lf, %f, %i, \"%@\",%li)",
                     (long)[message.chat_id integerValue],
                     (long)message.status,
                     message.text,
                     [message.date timeIntervalSinceReferenceDate],
                     message.heigh,
                     message.isRead,
                     message.direction,
                     (long)[message.serverId integerValue]
                     ];
    [self.database executeUpdate:sql];
}
-(void)storeMessages:(NSArray *)messages
{
    if (messages.count == 0) return;
    
}
-(NSArray *)getMessagesByPage:(NSInteger)page Count:(NSInteger)count
{
    NSMutableArray *messages = [@[] mutableCopy];
    NSString *sql = [NSString stringWithFormat:@"SELECT id, chat_id, status, text, date, heigh, is_read, direction, server_id FROM UserMessages LIMIT %lu OFFSET %lu",(unsigned long)count,(unsigned long)(page*count)];
    FMResultSet *rs = [self.database executeQuery:sql];
    while ([rs next]) {
        Message *message = [[Message alloc] init];
        message.text = [rs stringForColumn:@"text"];
        message.identifier = [rs stringForColumn:@"id"];
        message.status = [rs intForColumn:@"status"];
        message.direction = [rs stringForColumn:@"direction"];
        message.serverId = [rs stringForColumn:@"server_id"];
        message.isRead = [rs boolForColumn:@"is_read"];
        message.heigh = [rs doubleForColumn:@"heigh"];
        message.chat_id = [NSString stringWithFormat:@"%i",[rs intForColumn:@"chat_id"]];
        message.date = [NSDate dateWithTimeIntervalSinceReferenceDate:[rs doubleForColumn:@"date"]];//[rs dateForColumn:@"date"];
        [messages addObject:message];
    }
    return messages;
}

- (NSString *)databasePath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *databasePath = [documentsDirectory stringByAppendingPathComponent:@"UserRecords.db"];
    
    return databasePath;
}


- (FMDatabase *)database
{
    NSFileManager *fm = [NSFileManager defaultManager];
    BOOL isExist = [fm fileExistsAtPath:self.databasePath];
    if (!_database) {
        _database = [FMDatabase databaseWithPath:self.databasePath];
        [_database open];
//        int dbVersion = [self getSchemaVersionWithDatabase:_database];
        
        //!!!:’last_login‘ be used to ‘updated_at’
        [_database executeUpdate:@"CREATE TABLE IF NOT EXISTS UserMessages (id INTEGER PRIMARY KEY AUTOINCREMENT, chat_id UNSIGNED INTEGER, status UNSIGNED INTEGER, text TEXT , date FLOAT, heigh FLOAT, is_read UNSIGNED INTEGER, direction TEXT, server_id TEXT)"];
        
    }
    return _database;
}

@end
