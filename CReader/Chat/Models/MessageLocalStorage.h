//
//  MessageLocalStorage.h
//  Whatsapp
//
//  Created by Rafael Castro on 7/24/15.
//  Copyright (c) 2015 HummingBird. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Message.h"
@class FMDatabase;

// This class is responsable to store messages
// For now, it stores in memory only
//
@interface MessageLocalStorage : NSObject
{
    NSString *_databaseName;
    FMDatabase *_database;
}
+(id)sharedInstance;
-(void)storeMessage:(Message *)message;
-(void)storeMessages:(NSArray *)messages;
-(NSArray *)getMessagesByPage:(NSInteger)page Count:(NSInteger)count;
@end
