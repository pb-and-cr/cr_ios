//
//  MessageController.m
//  Whatsapp
//
//  Created by Rafael Castro on 7/23/15.
//  Copyright (c) 2015 HummingBird. All rights reserved.
//

#import "MessageController.h"
#import "MessageCell.h"
#import "TableArray.h"
//#import "LocalStorage.h"
#import "Inputbar.h"
#import "AppConfig.h"
#import "AFHTTPSessionManager.h"
//#import "AlertPickView.h"
#import "CCKeyboardControl.h"
#import "UIView+TKGeometry.h"
#import "MJRefresh.h"
#import "MBProgressHUD.h"
#import "FindChurchVC.h"

@interface MessageController() <InputbarDelegate,MessageGatewayDelegate,
                                    UITableViewDataSource,UITableViewDelegate/*,AlertPickViewDataSource, AlertPickViewDelegate*/>
{
    NSInteger pageIndex;
    BOOL isOpened_;
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet Inputbar *inputbar;
@property (strong, nonatomic) TableArray *tableArray;
@property (strong, nonatomic) NSArray *item;
@property (strong, nonatomic) NSArray *enItem;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *inputbarBottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *inputbarHeightConstraint;

@end


@implementation MessageController

-(void)viewDidLoad
{
    [super viewDidLoad];
    self.gateway = [MessageGateway sharedInstance];
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:PropGMOUserKey] == nil) {
        [self.gateway getGMOUserKey];
    }else {
        [self.gateway loadNewMessage];
    }

    pageIndex = 0;
    [self setInputbar];
    [self setTableView];
    self.item = @[
                  @"prayed",
                  @"recommitted",
                  @"grow",
                  @"none"
                    ];
    
    [self setGateway];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receivedNotificationMessage:)
                                                 name:@"ReceivedNotificationMessage"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(showChurchVC)
                                                 name:@"showFindChurch"
                                               object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.gateway.delegate = self;
    [self setStatusBarAndNaviView];
    [self.gateway loadNewMessage];
//    [(AppDelegate*)[[UIApplication sharedApplication] delegate] updateActionInfoToGoogleAnalytics:@"open-chat"];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    __weak Inputbar *inputbar = _inputbar;
    __weak MessageController *controller = self;
    CGFloat tabBarHight = 0;
    if(self.tabBarController.tabBar.isHidden == NO){
        tabBarHight = self.tabBarController.tabBar.height;
    }
    
    self.view.keyboardTriggerOffset = inputbar.frame.size.height;
    [self.view addKeyboardPanningWithFrameBasedActionHandler:^(CGRect keyboardFrameInView, CCKeyboardControlState keyboardState) {
        if (keyboardState != CCKeyboardControlStatePanning){
            [controller updateTableViewInsetWithKeyboardFrame:keyboardFrameInView];
        }
        
    } constraintBasedActionHandler:^(CGRect keyboardFrameInView, CCKeyboardControlState keyboardState) {
        if (keyboardState == CCKeyboardControlStateOpening) {
            controller.inputbarBottomConstraint.constant = controller.view.height - keyboardFrameInView.origin.y - tabBarHight;
        }else if (keyboardState == CCKeyboardControlStateClosing) {
            controller.inputbarBottomConstraint.constant = controller.view.height - keyboardFrameInView.origin.y;
        }
        controller.inputbarHeightConstraint.constant = inputbar.height;
    }];
    
    if (_isDecision) {
        [self showNamePrompt];
        _isDecision = NO;
    }
    
    UITabBarItem *tbi = (UITabBarItem*)[[[self.tabBarController tabBar] items] objectAtIndex:2];
    [tbi setBadgeValue:nil];
    
}

- (void)updateTableViewInsetWithKeyboardFrame:(CGRect)keyboardFrame
{
    dispatch_async(dispatch_get_main_queue(), ^{//这里将下面的方法推入到主线程中
        [self tableViewScrollToBottomAnimated:YES];
    });
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    self.gateway.delegate = nil;
    [self.view endEditing:YES];
    [self.view removeKeyboardControl];
//    [self.gateway dismiss];
    [self.tableView.mj_header endRefreshing];
}


-(void)setInputbar
{
    self.inputbar.placeholder = @"请输入少于140个字符";
    self.inputbar.delegate = self;
    self.inputbar.leftButtonImage = [UIImage imageNamed:@"keyboardDown"];
    self.inputbar.rightButtonText = @"发送";
    self.inputbar.rightButtonTextColor = [UIColor colorWithRed:0 green:124/255.0 blue:1 alpha:1];
}

-(void)setTableView
{
    self.tableArray = [[TableArray alloc] init];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f,self.view.frame.size.width, 10.0f)];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = [UIColor clearColor];
    [self.tableView registerClass:[MessageCell class] forCellReuseIdentifier: @"MessageCell"];
    
    MJRefreshStateHeader *header = [MJRefreshStateHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadOldData)];
    [header setTitle:@"下拉加载更多聊天记录" forState:MJRefreshStateIdle];
    [header setTitle:@"释放加载更多聊天记录" forState:MJRefreshStatePulling];
    [header setTitle:@"加载中 ..." forState:MJRefreshStateRefreshing];
    
    self.tableView.mj_header = header;
}

-(void)loadWelcomeData
{
    Message *message = [[Message alloc] init];
    message.serverId = 0;
    message.text = @"你好，有什么是我们可以为你祷告的？";
    message.direction = @"out";
    message.isRead = YES;
    //message.chat_id = _chat.identifier;
    message.date = [NSDate date];
    [self.tableArray addObject:message];
    [self.tableView reloadData];
}

-(void)loadOldData
{
    NSArray *arr = [self.gateway loadOldMessagesWithPageIndex:pageIndex];
    [self.tableView.mj_header endRefreshing];
    [self.tableArray addObjectsFromArray:arr];
    [self.tableView reloadData];
    if (pageIndex == 0) {
        dispatch_async(dispatch_get_main_queue(), ^{//这里将下面的方法推入到主线程中，唯有等reloadData完成后才执行下面的方法
            [self tableViewScrollToBottomAnimated:YES];
        });
    }
    pageIndex ++;
    if (arr.count == 0) {
        pageIndex --;
        [self showPregressHUDWithString:@"没有更多的消息！"];
    }
}

-(void)setGateway
{
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:PropGMOUserFirstChat] length]>0) {
//        [self.tableView headerBeginRefreshing];
    }else {
        [self loadWelcomeData];
    }
}

#pragma mark - Actions
- (IBAction)userDidTapScreen:(id)sender {
    [_inputbar resignFirstResponder];
}

#pragma mark - TableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.tableArray numberOfSections];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.tableArray numberOfMessagesInSection:section];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"MessageCell";
    MessageCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell)
    {
        cell = [[MessageCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    cell.message = [self.tableArray objectAtIndexPath:indexPath];
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"MessageCell";
    MessageCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    cell.message = [self.tableArray objectAtIndexPath:indexPath];
    return cell.height;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40.0;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [self.tableArray titleForSection:section];
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    CGRect frame = CGRectMake(0, 0, tableView.frame.size.width, 40);
    
    UIView *view = [[UIView alloc] initWithFrame:frame];
    view.backgroundColor = [UIColor clearColor];
    view.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    UILabel *label = [[UILabel alloc] init];
    label.text = [self tableView:tableView titleForHeaderInSection:section];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont fontWithName:@"Helvetica" size:20.0];
    [label sizeToFit];
    label.center = view.center;
    label.font = [UIFont fontWithName:@"Helvetica" size:13.0];
    label.backgroundColor = [UIColor colorWithRed:207/255.0 green:220/255.0 blue:252.0/255.0 alpha:1];
    label.layer.cornerRadius = 10;
    label.layer.masksToBounds = YES;
    label.autoresizingMask = UIViewAutoresizingNone;
    [view addSubview:label];
    
    return view;
}
- (void)tableViewScrollToBottomAnimated:(BOOL)animated
{
    NSInteger numberOfSections = [self.tableArray numberOfSections];
    NSInteger numberOfRows = [self.tableArray numberOfMessagesInSection:numberOfSections-1];
    if (numberOfRows)
    {
        [_tableView scrollToRowAtIndexPath:[self.tableArray indexPathForLastMessage]
                                         atScrollPosition:UITableViewScrollPositionBottom animated:animated];
    }
}

#pragma mark - InputbarDelegate

-(void)inputbarDidPressRightButton:(Inputbar *)inputbar
{
    
        Message *message = [[Message alloc] init];
        message.text = inputbar.text;
        message.date = [NSDate date];
//    message.chat_id = _chat.identifier;
        message.direction = @"in";
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:PropGMOUserFirstChat] length]<=0) {
        [self.tableArray removeAllObjects];
        [self.tableView reloadData];
    }
    //Store Message in memory
        [self.tableArray addObject:message];
    
    //Insert Message in UI
        NSIndexPath *indexPath = [self.tableArray indexPathForMessage:message];
        [self.tableView beginUpdates];
        if ([self.tableArray numberOfMessagesInSection:indexPath.section] == 1)
        [self.tableView insertSections:[NSIndexSet indexSetWithIndex:indexPath.section]
                      withRowAnimation:UITableViewRowAnimationNone];
        [self.tableView insertRowsAtIndexPaths:@[indexPath]
                          withRowAnimation:UITableViewRowAnimationBottom];
        [self.tableView endUpdates];
        
        [self.tableView scrollToRowAtIndexPath:[self.tableArray indexPathForLastMessage]
                          atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    
        //Send message to server
        [self.gateway sendMessage:message];
}

-(void)inputbarDidPressLeftButton:(Inputbar *)inputbar
{
    [_inputbar resignFirstResponder];
    //请求用户开启Notification的权限
    if([[[NSUserDefaults standardUserDefaults] objectForKey:PropDeviceNotificationIsSetup] length]<=0) {
//        [(AppDelegate*)[[UIApplication sharedApplication] delegate] showPermitNoticePopupIsNeedCover:YES];
    }
}

-(void)inputbarDidChangeHeight:(CGFloat)new_height
{
    //Update DAKeyboardControl
    self.view.keyboardTriggerOffset = new_height;
    self.inputbarHeightConstraint.constant = new_height;
}

-(void)inputbarDidBecomeFirstResponder:(Inputbar *)inputbar
{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:PropGMOUserName] == NULL && !isOpened_) {
        isOpened_ = YES;
        [self showNamePrompt];
    }
}

#pragma mark - MessageGatewayDelegate

-(void)gatewayDidUpdateStatusForMessage:(Message *)message
{
    [_inputbar resignFirstResponder];
    NSIndexPath *indexPath = [self.tableArray indexPathForMessage:message];
    MessageCell *cell = (MessageCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    [cell updateMessageStatus];
    if (message.status == MessageStatusSent && [[[NSUserDefaults standardUserDefaults] objectForKey:PropGMOUserFirstChat] length]<=0) {
        [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:PropGMOUserFirstChat];
//        [(AppDelegate*)[[UIApplication sharedApplication] delegate] updateActionInfoToGoogleAnalytics:@"first-chat"];
    }
}
-(void)gatewayDidReceiveMessages:(NSArray *)array
{
    [self.tableArray addObjectsFromArray:array];
    [self.tableView reloadData];
}
-(void)gatewayDidNoMessagesWithIsSucceed: (BOOL)isSucceed{
    if (isSucceed == YES) {
//        pageIndex--;
    }else
    {
        [self showPregressHUDWithString:@"加载失败！"];
    }
    [self.tableView.mj_header endRefreshing];
}
-(void)gatewayNeedUserName
{
    NSLog(@"gatewayNeedUserName");
    [_inputbar resignFirstResponder];
    [self showNamePrompt];
}

-(void)setStatusBarAndNaviView
{
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    //显示顶部导航栏
    [self.navigationController setNavigationBarHidden:NO];
}

- (void) showNamePrompt
{
    if (_isDecision) {
//        AlertPickView *picker = [[AlertPickView alloc] initWithHeaderTitle:@"请先输入您的信息" cancelButtonTitle:@"取消" confirmButtonTitle:@"提交" textFieldPlaceholder:@"怎么称呼您？"];
//        picker.delegate = self;
//        picker.dataSource = self;
//        [picker show];
    }else {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"您的名字" message:@"怎么称呼您？" preferredStyle:UIAlertControllerStyleAlert];
        //取消按钮
        UIAlertAction *CancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {}];
        //警示按钮
        UIAlertAction *doneAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
            NSLog(@"%@",alertController.textFields.firstObject.text);
            NSString *name = alertController.textFields.firstObject.text;
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            //    hud.mode = MBProgressHUDModeIndeterminate;
            hud.label.text = @"正在上传信息！";
            [self.gateway putUserInfoWithName:name Decision:nil AndResultCallback:^(BOOL result) {
                [hud hideAnimated:YES];
                if (result) {
                    pageIndex=1;
                }else {
                    pageIndex=1;
                }
            }];

        }];
        //文本输入框
        [alertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
            //配置文本框的代码块
            textField.placeholder = @"您的名字";
        }];
        [alertController addAction:CancelAction];
        [alertController addAction:doneAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    
}

//#pragma mark AlertPickViewDelegate
//- (NSAttributedString *)AlertPickView:(AlertPickView *)pickerView
//                          titleForRow:(NSInteger)row{
//    NSAttributedString *str = [[NSAttributedString alloc] initWithString:NSLocalizedString(self.item[row],nil)];
//    return str;
//}
//- (NSInteger)numberOfRowsInDYAlertPickerView:(AlertPickView *)pickerView {
//    return self.item.count;
//}
//- (void)AlertPickView:(AlertPickView *)pickerView didConfirmWithItemAtRow:(NSInteger)row AndUserName:(NSString*)name{
//    NSLog(@"%@ didConfirm", self.item[row]);
//    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    //    hud.mode = MBProgressHUDModeIndeterminate;
//    hud.labelText = @"正在上传信息！";
//    [self.gateway putUserInfoWithName:name Decision:self.item[row] AndResultCallback:^(BOOL result) {
//        [hud hide:YES];
//        if (result) {
//            pageIndex=1;
//        }else {
//            pageIndex=1;
//        }
//    }];
//
//}
//
//- (void)DYAlertPickerViewDidClickCancelButton:(AlertPickView *)pickerView {
//    NSLog(@"Canceled");
//}
//
//
//- (BOOL)DYAlertPickerViewStateOfSwitchButton {
//    return YES;
//}

#pragma mark UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        NSString *name = [alertView textFieldAtIndex:0].text;
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        //    hud.mode = MBProgressHUDModeIndeterminate;
        hud.label.text = @"正在上传信息！";
        [self.gateway putUserInfoWithName:name Decision:nil AndResultCallback:^(BOOL result) {
            [hud hideAnimated:YES];
            if (result) {
                pageIndex=1;
            }else {
                pageIndex=1;
            }
        }];

    }
//    else {
//        NSLog(@"disable messageVontroller");
//    }
}

#pragma mark notification observer


- (void)showPregressHUDWithString:(NSString *)str {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeText;
    hud.label.text = str;//NSLocalizedString(@"TextCopied", nil);
    [hud hideAnimated:YES afterDelay:1.5];
}

#pragma mark notification observer
- (void)receivedNotificationMessage:(NSNotification *)notification
{
    [self showPregressHUDWithString:NSLocalizedString(@"newMessageContent", nil)];
    
    NSString *messageStr = notification.userInfo[@"message"];
    
    //    [self.gateway loadOldMessagesWithPageIndex:1 IsOnlyNew:@"true"];
    [self.tableView.mj_header beginRefreshing];
    Message *message = [[Message alloc] init];
    message.serverId = 0;
    message.text = messageStr;
    message.direction = @"out";
    message.isRead = YES;
    //message.chat_id = _chat.identifier;
    message.date = [NSDate date];
    [self.tableArray addObject:message];
    [self.tableView reloadData];
}

- (IBAction) showChurchVC {
//    [self performSegueWithIdentifier:@"showFindChurch" sender:nil];
    FindChurchVC* vc = [[FindChurchVC alloc] init];
    [[self navigationController] pushViewController:vc animated:YES];
}

@end
