//
//  FindChurchVC.m
//  PressBible
//
//  Created by WangYi on 8/30/16.
//  Copyright © 2016 daddygarden. All rights reserved.
//

#import "FindChurchVC.h"
#import "EditCell.h"
#import "AppConfig.h"
#import "MessageGateway.h"
#import "MBProgressHUD.h"

@interface FindChurchVC ()<EditCellDelegate,UITableViewDelegate,UITableViewDataSource>
{
    NSString *name_;
    NSString *mobile_;
    NSString *address_;
    NSIndexPath *selectedIndexPath_;
}

//@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) UITableView *tableView;
@property (nonatomic,strong)NSArray *items;

@end

@implementation FindChurchVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _items = @[@"姓名:",@"手机号码:",@"居住地址:"];
    
    // 创建UItableView，style选择Grouped或Plain，这里我们以Grouped为例
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height) style:UITableViewStyleGrouped];
    // 声明 tableView 的代理和数据源
    _tableView.delegate = self;
    _tableView.dataSource = self;
    // 添加到 view 上
    [self.view addSubview:_tableView];
    
    self.navigationItem.title = @"找教会";
    
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithTitle:@"提交" style:UIBarButtonItemStylePlain target:self action:@selector(submitInfo)];
    self.navigationItem.rightBarButtonItem = rightButton;
    [self.navigationController setNavigationBarHidden:NO];
}

-(void)closeVC {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    NSIndexPath *index = [NSIndexPath indexPathForRow:0 inSection:0];
    EditCell *cell = [_tableView cellForRowAtIndexPath:index];
    [cell.editTextField becomeFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) submitInfo {
    //返回键盘
    EditCell *beforeCell = [_tableView cellForRowAtIndexPath:selectedIndexPath_];
    [beforeCell.editTextField resignFirstResponder];
    
    if (name_.length <= 0) {
        EditCell *cell = [_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        cell.titleLabel.textColor = [UIColor redColor];
        [cell.editTextField becomeFirstResponder];
        return;
    }else if (mobile_.length <= 0) {
        EditCell *cell = [_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
        cell.titleLabel.textColor = [UIColor redColor];
        [cell.editTextField becomeFirstResponder];
        return;
    }else if (address_.length <= 0) {
        EditCell *cell = [_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
        cell.titleLabel.textColor = [UIColor redColor];
        [cell.editTextField becomeFirstResponder];
        return;
    }
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    __weak __typeof(self) weakSelf = self;
    [[MessageGateway sharedInstance] submitFindChurchInfoWithName:name_ Mobile:mobile_ Address:address_ AndResultCallback:^(BOOL result) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (result) {
            [weakSelf closeVC];
        }else {
            [weakSelf showPregressHUDWithString:@"提交失败啦，请稍后再试！"];
        }
    }];
    Message *message = [[Message alloc] init];
    message.serverId = @"-1";
    message.text = [NSString stringWithFormat:@"[%@]:姓名:%@;手机号码:%@;地址:%@",@"找教会",name_,mobile_,address_];
    message.direction = @"in";
    message.isRead = YES;
    message.date = [NSDate date];
    [[MessageGateway sharedInstance] sendMessage:message];
}

#pragma mark - Table view data source


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 100.f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 100)];
    view.backgroundColor = [UIColor clearColor];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15, 10, self.view.frame.size.width-30, 80)];
    label.font = [UIFont systemFontOfSize:15];
    label.textColor = [UIColor darkGrayColor];
    label.numberOfLines = 3;
    label.text = @"如果你需要找教会，我们可以为你推荐附近坚持圣经真理并且帮助你生命成长的教会。\n请留下你的联系方式:";
    [view addSubview:label];
    return view;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIndentifier = @"findChurchCell";
    EditCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIndentifier];
    if(nil == cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"EditCell" owner:nil options:nil].firstObject;
    }
    cell.indexPath = indexPath;
    cell.delegate = self;
    cell.titleLabel.text = [_items objectAtIndex:indexPath.row];
    return cell;
}

#pragma mark EditCellDelegate
-(void)editTextFieldDidBeginEditingWithIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"editTextFieldDidBeginEditingWithIndexPath");
    [_tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
    selectedIndexPath_ = indexPath;
}

-(void)editTextFieldDidEndEditing:(UITextField *)textField WithIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"editTextFieldDidEndEditing");
    if (indexPath.row == 0) {
        name_ = textField.text;
    }else if (indexPath.row == 1) {
        mobile_ = textField.text;
    }else if (indexPath.row == 2) {
        address_ = textField.text;
    }
}

- (void)showPregressHUDWithString:(NSString *)str {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeText;
    hud.labelText = str;//NSLocalizedString(@"TextCopied", nil);
    [hud hide:YES afterDelay:1.5];
}

@end
