//
//  ChatController.h
//  Whatsapp
//
//  Created by Rafael Castro on 6/16/15.
//  Copyright (c) 2015 HummingBird. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Chat.h"
#import "MessageGateway.h"

//
// This class control chat exchange message itself
// It creates the bubble UI
//
@interface MessageController : UIViewController
@property (strong, nonatomic) Chat *chat;
@property (strong, nonatomic) MessageGateway *gateway;
@property (assign, nonatomic) BOOL isDecision;
@end
