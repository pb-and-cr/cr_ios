//
//  EditCell.h
//  CReader
//
//  Created by WangYi on 2018/7/3.
//  Copyright © 2018年 WangYi. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol EditCellDelegate <NSObject>

-(void)editTextFieldDidBeginEditingWithIndexPath:(NSIndexPath *)indexPath;
-(void)editTextFieldDidEndEditing:(UITextField *)textField WithIndexPath:(NSIndexPath *)indexPath;

@end

@interface EditCell : UITableViewCell<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (weak, nonatomic) IBOutlet UITextField *editTextField;

@property (nonatomic, assign)id<EditCellDelegate> delegate;

@property (nonatomic, strong) NSIndexPath *indexPath;

@end
