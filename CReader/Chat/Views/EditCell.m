//
//  EditCell.m
//  CReader
//
//  Created by WangYi on 2018/7/3.
//  Copyright © 2018年 WangYi. All rights reserved.
//

#import "EditCell.h"

@implementation EditCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.editTextField.delegate = self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (self.delegate) {
        [self.delegate editTextFieldDidBeginEditingWithIndexPath:self.indexPath];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (self.delegate) {
        [self.delegate editTextFieldDidEndEditing:textField WithIndexPath:self.indexPath ];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (self.delegate) {
        [self.delegate editTextFieldDidEndEditing:textField WithIndexPath:self.indexPath];
    }
    [textField resignFirstResponder];
    return YES;
}

@end
