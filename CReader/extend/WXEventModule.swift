//
//  WXEventModule.swift
//  CReader
//
//  Created by Yoni on24/09/2016.
//  Copyright © 2016 com.taobao.weex. All rights reserved.
//

import Foundation
import Alamofire
import UIKit
import AVFoundation
import MediaPlayer



public extension WXEventModule {
    
    @objc public func openURL(_ url:String) {
        var newUrl:String = url
        if url.hasPrefix("//") {
            newUrl = String.init(format: "http://%@", url);
        }else if !url.hasPrefix("http") {
            //relative path
            newUrl = (URL.init(string: url, relativeTo: weexInstance.scriptURL)!.absoluteString)
        }
        
        if newUrl.contains("chat.js") {
            print("Yoni : hehe!")
            if(newUrl.contains("decision")){
                //用户决定信主
                AnalyticsTracker.shared.decision();
            }
            
        }else {
			print("qingsong---1111"+newUrl)
            let controller:WXViewController = WXViewController()
            controller.url = URL.init(string: newUrl)
            if newUrl.contains("backToRootView=true"){
                weexInstance.viewController.navigationController?.popToRootViewController(animated: true)
            }
            else if newUrl.contains("closeThisPage=true"){
                weexInstance.viewController.navigationController?.pushViewController(controller, animated:true)
                weexInstance.viewController.navigationController?.viewControllers.remove(at: (weexInstance.viewController.navigationController?.viewControllers.count)! - 2)
            }else{
                 weexInstance.viewController.navigationController?.pushViewController(controller, animated:true)
            }
        }
        
    }
    
	@objc func openApp(_ name:String){
        print("qingsongopenApp+\(name)")
		let findChurchVC: FindChurchVC = FindChurchVC()
		let messageVC: MessageController = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MessageController") as! MessageController
		
		let tabBarPageVC : TabbarViewController = TabbarViewController()
		//MessageController()
		if name == "chat" {
			weexInstance.viewController.navigationController?.pushViewController(messageVC, animated:true)
		}
		
		if name == "knowJesus" {
			weexInstance.viewController.navigationController?.pushViewController(findChurchVC, animated:true)
		}
		
		if name == "tabBarPage" {
			weexInstance.viewController.navigationController?.pushViewController(tabBarPageVC, animated:true)
		}
		
    }
    
    @objc public func gosell(_ productid:String) {
        print("Yoni : ",productid)
        let linkService = ALBBSDK.sharedInstance().getService(ALBBAppLinkService.self) as! ALBBAppLinkService
        linkService.jumpDetail(productid, params: [ALBBAppLinkParamPID : "mm_133031133_45940511_767888449"])
    }
    
//    @objc public func isNetworkAvailable()->Int {
//        print("isNetworkAvailable")
//        if ReachabilityManager.shared.isNetworkAvailable() {
//            return 1
//        }else {
//            return 0
//        }
//    }
	
	@objc public func gotoDonate (_ donateValue: Double){
		NotificationCenter.default.post(name: NotificationKey.openWeChatPayMentName, object: nil, userInfo:["donateValue":donateValue])
	}
	
    @objc public func closePage() {
        print("closePage")
    }
    
    @objc public func googleTrack(_ category:String, _ action:String, _ label:String, _ id:Int) {
        print("googleTrack: ",category,action,label,id)
        //firebase analytics
//        AnalyticsTracker.shared.sendToFIRAnalytics(category, action, label, id)
        
        //google analytics
//        AnalyticsTracker.shared.sendToGAIAnalytics(category, action, label, id)
        
    }
    
	@objc func openBook(_ id:Int, _ title:String, _ address:String, _ imgUrl:String, _ url:String) {
        print("qingsong:0-0-0-1")
		print("openBook: ","id ",id,"title ",title,"address ",address,"imgUrl ",imgUrl,"url ",url)
        
        /// 首先从address中获得代扩展名的文件名 ///，再把文件名给取得。
        let range = address.range(of: "/", options: .backwards, range: nil, locale: nil)
        let fileNameWithExtension = address.suffix(from: range!.upperBound)
        
        /// 查看沙盒中的文档文件夹是否有这个书籍文件
        let docDirs = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) as NSArray
        let docDir = docDirs[0] as! String
        
        /// 构建文件的最终路径，并判断文件是否存在于路径中
        let docDirUrl = NSURL(fileURLWithPath: docDir)
        let filePath = docDirUrl.appendingPathComponent(String(fileNameWithExtension))?.path
        let fileManage = FileManager.default

		do {
//			print("2-2-2-2-2-2")
			if fileManage.fileExists(atPath: filePath!) {
				//如果存在就打开
//				print("3-3-3-3-3-3-")
				NotificationCenter.default.post(name: NotificationKey.openBookNoticationName, object: nil, userInfo: ["filePath":filePath!,"id":id, "title":title, "imgUrl":imgUrl, "url":url])
			}else {
				//如果不存在就下载，并保存
//				print("4-4-4-4-4-4-4--")
				let destination = DownloadRequest.suggestedDownloadDestination()
				Alamofire.download(address, to: destination).validate().responseData { response in
				  //下载完成后就打开书籍
					NotificationCenter.default.post(name: NotificationKey.openBookNoticationName, object: nil, userInfo: ["filePath":filePath!,"id":id, "title":title, "imgUrl":imgUrl, "url":url])
				}
			}
			
		}catch {
			
		}
		
	

    }
}
