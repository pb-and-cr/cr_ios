//
//  WXImageLoaderDefaultImplement.swift
//  CReader
//
//  Created by Yoni on 19/01/2017.
//  Copyright © 2017 com.daddygarden. All rights reserved.
//

import Foundation
import UIKit
import WeexSDK
import Swift
import SDWebImage

class WXImageLoaderDefaultImplement:NSObject, WXImgLoaderProtocol {
    
   
//    func downloadImage(withURL url: String!, imageFrame: CGRect, userInfo options: [AnyHashable : Any]! = [:], completed completedBlock: ((UIImage?, Error?, Bool) -> Void)!) -> WXImageOperationProtocol! {
//
////        if url == nil {
////            print("yoni")
////        }
//
//        var temp:String
//        if (url.hasPrefix("//")) {
//            temp = "http:" + url;
//        } else {
//            temp = url;
//        }
//
//        let operation = SDWebImageManager.shared().downloadImage(with: URL.init(string: temp), options: SDWebImageOptions.retryFailed, progress: { (receivedSize:Int, expectedSize:Int) in
//
//        }) { (image:UIImage?, error:Error?, cacheType:SDImageCacheType, finished:Bool, imageURL:URL?) in
//            if (completedBlock != nil) {
//                completedBlock(image, error, finished)
//            }
//        }
//
//        return operation as? WXImageOperationProtocol
//    }
	
	func downloadImage(withURL url: String!, imageFrame: CGRect, userInfo options: [AnyHashable : Any]! = [:], completed completedBlock: ((UIImage?, Error?, Bool) -> Void)!) -> WXImageOperationProtocol! {
		print("weex加载图片:", url)
		guard let url = url else {
			print("weex传入的图片URL无效")
			return nil
		}
		let fullURL = url.hasPrefix("//")  ?  ("http:" + url)  :  url
		guard let validURL = URL(string: fullURL) else {
			print("weex图片URL无效：", fullURL)
			return nil
		}
		
		let downloadTask = SDWebImageManager.shared().downloadImage(with: validURL, options: .retryFailed, progress: nil) { (image, error, cacheType, finished, imageURL) in
			completedBlock?(image, error, finished)
		}
		return downloadTask as? WXImageOperationProtocol
	}

}
