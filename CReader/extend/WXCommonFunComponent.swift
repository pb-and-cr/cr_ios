//
//  WXCommonFunComponent.swift
//  CReader
//
//  Created by Yoni on2017/12/21.
//  Copyright © 2017年 com.taobao.weex. All rights reserved.
//

import Foundation


public extension WXCommonFunComponent{
    @objc public func ShareArticle(_ title:String, _ url:String, _ imgUrl:String, _ des:String) {
        print(title,url,imgUrl,des)
//        var objectsToShare: [Any] = [title]
//        if let imageURL = URL(string: imgUrl)
//        {
//            let data = try? Data(contentsOf: imageURL)
//            if let imageData = data {
//                objectsToShare.append(UIImage(data: imageData)!)
//            }
//        }
//
//        objectsToShare.append(URL(string: url)!)
//
//        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
//        self.weexInstance?.viewController.present(activityVC, animated: true, completion: nil)
		
		
		let params = NSMutableDictionary()
		
		params.ssdkSetupShareParams(byText: des, images: imgUrl, url: URL(string: url), title: title, type:.webPage)
				
		ShareSDK.showShareActionSheet(nil, customItems:nil, shareParams: params, sheetConfiguration: nil, onStateChanged: nil)
	}
}
