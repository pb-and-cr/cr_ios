/**
 * Created by Weex.
 * Copyright (c) 2016, Alibaba, Inc. All rights reserved.
 *
 * This source code is licensed under the Apache Licence 2.0.
 * For the full copyright and license information,please view the LICENSE file in the root directory of this source tree.
 */

#import "WXEventModule.h"

@implementation WXEventModule

@synthesize weexInstance;

WX_EXPORT_METHOD(@selector(openURL:))

WX_EXPORT_METHOD(@selector(openApp:))
	
WX_EXPORT_METHOD(@selector(isNetworkAvailable))
	
WX_EXPORT_METHOD(@selector(closePage))
	
WX_EXPORT_METHOD(@selector(googleTrack::::))
	
WX_EXPORT_METHOD(@selector(openBook:::::))
	
WX_EXPORT_METHOD(@selector(gosell:))
	
WX_EXPORT_METHOD(@selector(gotoDonate:))
@end

