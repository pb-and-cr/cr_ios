//
//  WXLoadingComponent.swift
//  SwiftWeexSample
//
//  Created by 青松 on 2018/8/8.
//  Copyright © 2018年 com.taobao.weex. All rights reserved.
//

import UIKit
import WeexSDK

class WXLoadingDotsComponent: WXComponent{
    
    
    private var dotOne = UIView()
    private var dotTwo = UIView()
    private var dotThree = UIView()
    
    
    override init(ref: String, type: String, styles: [AnyHashable : Any]?, attributes: [AnyHashable : Any]? = nil, events: [Any]?, weexInstance: WXSDKInstance) {
        super.init(ref: ref, type: type, styles: styles, attributes: attributes, events: events, weexInstance: weexInstance);
        //handle attributes
        //handle styles
        
    }
    
    //    override func loadView() -> UIView {
    //        return
    //    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dotOne.backgroundColor = #colorLiteral(red: 0.8823529412, green: 0.3647058824, blue: 0.3254901961, alpha: 1)
        dotOne.layer.masksToBounds = true
        dotOne.layer.cornerRadius = 5
        dotOne.frame = CGRect(x: 0, y: 0, width: 10, height: 10)
        dotTwo.backgroundColor = #colorLiteral(red: 0.8823529412, green: 0.3647058824, blue: 0.3254901961, alpha: 1)
        dotTwo.layer.masksToBounds = true
        dotTwo.layer.cornerRadius = 5
        dotTwo.frame = CGRect(x: 15, y: 0, width: 10, height: 10)
        dotThree.backgroundColor = #colorLiteral(red: 0.8823529412, green: 0.3647058824, blue: 0.3254901961, alpha: 1)
        dotThree.layer.masksToBounds = true
        dotThree.layer.cornerRadius = 5
        dotThree.frame = CGRect(x: 30, y: 0, width: 10, height: 10)
        view.addSubview(dotOne)
        view.addSubview(dotTwo)
        view.addSubview(dotThree)
        startAnimation()
    }
    
    func startAnimation() {
        dotOne.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
        dotTwo.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
        dotThree.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
        
        UIView.animate(withDuration: 0.6, delay: 0.0, options: [.repeat, .autoreverse], animations: {
            self.dotOne.transform = CGAffineTransform.identity
        }, completion: nil)
        
        UIView.animate(withDuration: 0.6, delay: 0.2, options: [.repeat, .autoreverse], animations: {
            self.dotTwo.transform = CGAffineTransform.identity
        }, completion: nil)
        
        UIView.animate(withDuration: 0.6, delay: 0.4, options: [.repeat, .autoreverse], animations: {
            self.dotThree.transform = CGAffineTransform.identity
        }, completion: nil)
    }
    
}

