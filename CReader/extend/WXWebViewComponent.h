//
//  WXWebViewComponent.h
//  CReader
//
//  Created by WangYi on 2018/6/12.
//  Copyright © 2018年 WangYi. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import <WeexSDK/WeexSDK.h>
#import <JavaScriptCore/JavaScriptCore.h>
#import <WeexSDK/WXComponent.h>

@protocol WebViewJSExport <JSExport>

- (int)isApp;
- (void)sendGosple;
- (void)clickYes;
@end

@interface WXWebViewComponent : WXComponent<UIWebViewDelegate,WebViewJSExport>

- (void)notifyWebview:(NSDictionary *) data;

- (void)reload;

- (void)goBack;

- (void)goForward;

@end
