//
//  TabbarViewController.swift
//  CReader
//
//  Created by 青松 on 2022/10/8.
//  Copyright © 2022 WangYi. All rights reserved.
//

import UIKit
import IoniconsKit

class TabbarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
		self.tabBar.barTintColor = UIColor.white
		self.tabBar.tintColor = #colorLiteral(red: 0.8823529412, green: 0.3647058822, blue: 0.3254901961, alpha: 1)
//		self.tabBar.appearance
		if #available(iOS 10.0, *) {
			self.tabBar.unselectedItemTintColor = #colorLiteral(red: 0.4, green: 0.4, blue: 0.4, alpha: 1)
		} else {
			// Fallback on earlier versions
		}
		
		//主页
//		let HomePage = HomePageViewController();
//		HomePage.url = URL(string: WeexBundleFolder + "index.js")
//		HomePage.tabBarItem = UITabBarItem(
//			title: "主页",
//			image: UIImage.ionicon(with: .iosHome, textColor: UIColor.orange, size: CGSize(width: 22, height: 22)),
//			selectedImage: UIImage.ionicon(with: .iosHome, textColor: UIColor.orange, size: CGSize(width: 22, height: 22))
//		)

		//专栏页
//		let FollowPage = HomePageViewController();
//		let FollowPage = WXViewController()
//		FollowPage.url = URL(string: WeexBundleFolder + "subscribe.js")
//		FollowPage.tabBarItem = UITabBarItem(
//			title: "专栏",
//			image: UIImage.ionicon(with: .helpBuoy, textColor: UIColor.orange, size: CGSize(width: 22, height: 22)),
//			selectedImage: UIImage.ionicon(with: .helpBuoy, textColor: UIColor.orange, size: CGSize(width: 22, height: 22))
//		)
		
		//消息页面
//		let MessagePage = WXViewController();
//		MessagePage.url = URL(string: WeexBundleFolder + "message.js")
//		MessagePage.tabBarItem = UITabBarItem(
//			title: "消息",
//			image: UIImage.ionicon(with: .iosChatboxes, textColor: UIColor.orange, size: CGSize(width: 22, height: 22)),
//			selectedImage: UIImage.ionicon(with: .iosChatboxes, textColor: UIColor.orange, size: CGSize(width: 22, height: 22))
//		)
		
		//书籍页面
		let BookPage = HomePageViewController();
		BookPage.url = URL(string: WeexBundleFolder + "libraryBook.js")
//		BookPage.url = URL(string: "http://10.65.0.27:8088/dist/libraryBook.js")
		BookPage.tabBarItem = UITabBarItem(
			title: "图书",
			image: UIImage.ionicon(with: .iosBook, textColor: UIColor.orange, size: CGSize(width: 22, height: 22)),
			selectedImage: UIImage.ionicon(with: .iosBook, textColor: UIColor.orange, size: CGSize(width: 22, height: 22))
		)
		
		//个人中心页面
		let MePage = HomePageViewController();
		MePage.url = URL(string: WeexBundleFolder + "me.js")
//		MePage.url = URL(string: "http://10.65.0.27:8088/dist/me.js")
		MePage.tabBarItem = UITabBarItem(
			title: "我",
			image: UIImage.ionicon(with: .iosPerson, textColor: UIColor.orange, size: CGSize(width: 22, height: 22)),
			selectedImage: UIImage.ionicon(with: .iosPerson, textColor: UIColor.orange, size: CGSize(width: 22, height: 22))
		)
		
//		self.addChildViewController(HomePage)
//		self.addChildViewController(FollowPage)
//		self.addChildViewController(MessagePage)
		self.addChildViewController(BookPage)
		self.addChildViewController(MePage)
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
