//
//  Constants.swift
//  CReader
//
//  Created by WangYi on 2018/5/25.
//

import Foundation

struct NotificationKey {
    static let openBookNoticationName = Notification.Name.init( "openBookReaderNotification")
	
	
//    static let openChatNoticationName = Notification.Name.init("openChatNotification")
	
	static let openWeChatPayMentName = Notification.Name.init("openWeChatPayMentNotification")
	
	static let openAlertThanks = Notification.Name.init("openAlertThanksNotification")
}

/// weex 的资源路径
let WeexBundleFolder = String(format: "file://%@/bundlejs/", Bundle.main.bundlePath)

//let WeexBundleFolder = String(format: "http://192.168.0.103:8088/dist/", Bundle.main.bundlePath)


let SETTINGS_APP_FIRST_LAUNCH = "app_first_launch"
