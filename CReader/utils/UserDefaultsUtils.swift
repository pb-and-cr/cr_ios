//
//  UserDefaultsUtils.swift
//  CReader
//
//  Created by WangYi on 2018/5/29.
//

import UIKit

final class UserDefaultsUtils: NSObject {
    //anyobject
    class func saveValue(value:AnyObject, key:String) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(value, forKey: key)
        userDefaults.synchronize()
    }
    class func valueWithKey(key:String) -> AnyObject {
        let userDefaults = UserDefaults.standard
        return userDefaults.object(forKey: key) as AnyObject
    }
    
    //bool
    class func saveBoolValue(value:Bool, key:String) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(value, forKey: key)
        userDefaults.synchronize()
    }
    class func boolValueWithKey(key:String) -> Bool {
        let userDefaults = UserDefaults.standard
        return userDefaults.bool(forKey: key)
    }
    
    //string
    class func saveStringValue(value:String, key:String) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(value, forKey: key)
        userDefaults.synchronize()
    }
    class func stringValueWithKey(key:String) -> String {
        let userDefaults = UserDefaults.standard
        return userDefaults.string(forKey: key)!
    }
    
    //int
    class func saveIntValue(value:Int, key:String) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(value, forKey: key)
        userDefaults.synchronize()
    }
    class func intValueWithKey(key:String) -> Int {
        let userDefaults = UserDefaults.standard
        return userDefaults.integer(forKey: key)
    }
    
    class func deleteValueWithKey(key:String) {
        let userDefaults = UserDefaults.standard
        userDefaults.removeObject(forKey: key)
        userDefaults.synchronize()
    }
    
    class func hasKey(key:String) -> Bool {
        let userDefaults = UserDefaults.standard
        let has = userDefaults.object(forKey: key) != nil
        return has
    }
}
