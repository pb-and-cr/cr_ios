//
//  AnalyticsTracker.swift
//  CReader
//
//  Created by WangYi on 2018/6/8.
//  Copyright © 2018年 WangYi. All rights reserved.
//

import UIKit
import FirebaseCore
import FirebaseAnalytics
import Alamofire

@objc class AnalyticsTracker:NSObject {
    static let GMO_API_KEY = "F4oFIT7gEA3h8RgiDArnGFosqFc4oIXW8SpIMJ5XQwcZFMdjOFdln43nDlvtNHR"
    let GMO_VISITOR_ID_KEY = "gmo.visitor.id"
    var sessionManager:SessionManager? = nil
    
    struct GMOProxyAPI {
        static let baseUrl = "https://xinxiwang.me"
        static let usersUrl = baseUrl+"/users"
        static let messagesUrl = baseUrl+"/messages"
        static let notificationsUrl = baseUrl+"/notifications"
        static let visitorUrl = baseUrl+"/visitor"
        static let eventsUrl = baseUrl+"/events"
    }
    
    @objc static let shared:AnalyticsTracker = AnalyticsTracker()
    
    private override init() {
        
    }
    
    func getGMOVisitorId() {
        if UserDefaultsUtils.hasKey(key: GMO_VISITOR_ID_KEY) == false {
            
            let configuration: URLSessionConfiguration = URLSessionConfiguration.default
            //        configuration.timeoutIntervalForRequest = 15
            //        configuration.timeoutIntervalForResource = 15
            // Set up certificates
            let pathToCert = Bundle.main.path(forResource: "xinxiwangCert", ofType: "cer")
            let localCertificate = NSData(contentsOfFile: pathToCert!)
            let certificates = [SecCertificateCreateWithData(nil, localCertificate!)!]
            
            // Configure the trust policy manager
            let serverTrustPolicy = ServerTrustPolicy.pinCertificates(
                certificates: certificates,
                validateCertificateChain: false,
                validateHost: true
            )
            let serverTrustPolicies = ["xinxiwang.me": serverTrustPolicy]
            let serverTrustPolicyManager = ServerTrustPolicyManager(policies: serverTrustPolicies)
            
            self.sessionManager = SessionManager(
                configuration: configuration,
                serverTrustPolicyManager: serverTrustPolicyManager
            )
            
            self.sessionManager?.request(GMOProxyAPI.visitorUrl, method: .post, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON(completionHandler: { (response) in
                switch response.result{
                case .success(let value):
                    let responseDict = value as! [String : Any]
                    if responseDict["success"]as! Int == 1 {
                        print(responseDict["id"] as! String)
                        UserDefaultsUtils.saveStringValue(value: responseDict["id"] as! String, key: self.GMO_VISITOR_ID_KEY)
                    }else {
                        
                    }
                    
                    
                case .failure(let error):
                    print(error)
                }
            })
        }
    }
    
    func sendToGMO(_ event:String) {
        if UserDefaultsUtils.hasKey(key: GMO_VISITOR_ID_KEY) == true {
            let configuration: URLSessionConfiguration = URLSessionConfiguration.default
            //configuration.timeoutIntervalForRequest = 15
            //configuration.timeoutIntervalForResource = 15
            // Set up certificates
            let pathToCert = Bundle.main.path(forResource: "xinxiwangCert", ofType: "cer")
            let localCertificate = NSData(contentsOfFile: pathToCert!)
            let certificates = [SecCertificateCreateWithData(nil, localCertificate!)!]
            
            // Configure the trust policy manager
            let serverTrustPolicy = ServerTrustPolicy.pinCertificates(
                certificates: certificates,
                validateCertificateChain: false,
                validateHost: true
            )
            let serverTrustPolicies = ["xinxiwang.me": serverTrustPolicy]
            let serverTrustPolicyManager = ServerTrustPolicyManager(policies: serverTrustPolicies)
            
            self.sessionManager = SessionManager(
                configuration: configuration,
                serverTrustPolicyManager: serverTrustPolicyManager
            )
            
            let timestamp = NSDate().timeIntervalSince1970
            let params = [
                "key":AnalyticsTracker.GMO_API_KEY,
                "appName":"Christian Reader",
                "visitorId":UserDefaultsUtils.stringValueWithKey(key: GMO_VISITOR_ID_KEY),
                "deviceType":"iOS",
                "eventType":event,
                "timestamp":String(timestamp),
                "language":"cn"
            ]
            
            self.sessionManager?.request(GMOProxyAPI.eventsUrl, method: .post, parameters: params, encoding: JSONEncoding.default, headers: nil).responseJSON(completionHandler: { (response) in
                switch response.result{
                case .success(let value):
                    print(value)
//                    let responseDict = value as! [String : Any]
//                    if responseDict["success"]as! Int == 1 {
//                        print(responseDict["id"] as! String)
//                        UserDefaultsUtils.saveStringValue(value: responseDict["id"] as! String, key: self.GMO_VISITOR_ID_KEY)
//                    }else {
//
//                    }
                case .failure(let error):
                    print(error)
                }
            })
        }
    }
    
    func sendToGAIAnalytics(_ category:String, _ action:String, _ label:String, _ id:Int) -> Bool {
        guard let gai = GAI.sharedInstance() else {
            assert(false, "Google Analytics not configured correctly")
            return true//Base on your function return type, it may be returning something else
        }
        let tracker = gai.tracker(withTrackingId: "UA-43690716-8")
        let event = GAIDictionaryBuilder.createEvent(withCategory: category, action: action, label: label, value: NSNumber(value: id))
        tracker?.send(event!.build() as! [NSObject: Any])
        return true
    }
    
    func sendToFIRAnalytics(_ category:String, _ action:String, _ label:String, _ id:Int) {
        var params:Dictionary = ["action":action]
        if label != "" {
            params["label"] = label
            params["id"] = String(describing: id)
        }
        Analytics.logEvent(category, parameters: params)
    }
    
    @objc func decision() {
        sendToGMO("decision")
    }
    
    func recordTextGospel() {
        sendToGMO("gospel")
        sendToGAIAnalytics("Christian Reader Gospel","Text","Text",0)
    }
    
    @objc func recordVideoGospel() {
        sendToGMO("gospel")
        sendToGAIAnalytics("Christian Reader Gospel","WebVideo","WebVideo",0)
    }
    
    
}
