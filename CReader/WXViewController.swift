//
//  WXViewController.swift
//  CReader
//
//  Created by Yoni on5/7/18.
//  Copyright © 2016 com.taobao.weex. All rights reserved.
//

import UIKit
import WeexSDK
import SDWebImage
import FolioReaderKit
import Alamofire
import SwiftyJSON
import AVFoundation
import MediaPlayer


class WXViewController: UIViewController,UIGestureRecognizerDelegate {
    var instance:WXSDKInstance?;
    var weexView = UIView()
    var weexHeight:CGFloat?;
    var top:CGFloat?;
    var url:URL?;
	
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self;
		render()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        /*
         *http://stackoverflow.com/questions/31365097/can-i-print-function-type-in-swift
         *
         */
        print(#function)
        
        if instance != nil {
            instance!.destroy()
        }
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil;
		
		NotificationCenter.default.removeObserver(self, name: NotificationKey.openBookNoticationName, object: nil)
		
		NotificationCenter.default.removeObserver(self, name: NotificationKey.openWeChatPayMentName, object: nil)
		
		NotificationCenter.default.removeObserver(self, name: NotificationKey.openAlertThanks, object: nil)
    }
    
    func render(){
        if instance != nil {
            instance!.destroy()
        }
        weak var weakSelf = self;
        instance = WXSDKInstance();
        instance!.viewController = weakSelf
        let width = self.view.frame.size.width
		
//        if UIDevice.current.isiPhoneX() {
//            instance!.frame = CGRect(x: 0, y: 145, width: width, height: self.view.frame.size.height-145)
//        }else {
            instance!.frame = CGRect(x: 0, y: UIApplication.shared.statusBarFrame.height, width: width, height: self.view.frame.size.height-UIApplication.shared.statusBarFrame.height)
//        }
		
        instance?.onCreate = {
            (view:UIView?)-> Void in
            weakSelf!.weexView.removeFromSuperview()
            weakSelf!.weexView = view!;
            weakSelf!.view.addSubview((weakSelf?.weexView)!)
            UIAccessibilityPostNotification(UIAccessibilityScreenChangedNotification, weakSelf!.weexView)
        }
        
        instance?.onFailed = {
            (error:Error?)-> Void in
            print("faild at error: %@", error!)
        }
        
        instance?.renderFinish = {
            (view:UIView?)-> Void in
            print("render finish")
        }
        instance?.updateFinish = {
            (view:UIView?)-> Void in
            print("update finish")
        }

        instance!.render(with: url, options: ["bundleUrl":url!.absoluteString], data: nil)
    }
	
	
	@objc func openBookReaderNotification(notification:Notification) {
		
		
		let commandCenter = MPRemoteCommandCenter.shared()
				  
		commandCenter.playCommand.addTarget { (_) -> MPRemoteCommandHandlerStatus in
			 if notification.userInfo != nil {
				 //            openBookReader(filePath: notification.userInfo!["filePath"] as! String, id: notification.userInfo!["id"] as! Int, title: notification.userInfo!["title"] as! String, imgUrl: notification.userInfo!["imgUrl"] as! String, url: notification.userInfo!["url"] as! String)
				let config = self.readerConfiguration()
				 let folioReader = FolioReader()
				 folioReader.presentReader(parentViewController: self, withEpubPath: notification.userInfo!["filePath"] as! String, andConfig: config)
			 }
					  
			 return .success
		}
		commandCenter.playCommand.isEnabled = true
	
	}
	
	
	@objc func openWeChatPayMentNotification(notification:Notification){
		let donateValue = notification.userInfo!
		getWeChatPrePayInfo(donateValue["donateValue"] as! Double)

	}
	
	func getWeChatPrePayInfo (_ donateValue: Double){
		Alamofire.request("http://christian_reading.daddygarden.com/api/Pay/getAppPayParams",method: .post, parameters: ["total":donateValue])
			.responseJSON { response in
				if let jsons = response.result.value {
					print("JSON: \(jsons)"+"qingsongdonate")					
					let responseInfo = JSON(jsons)
//					print(responseInfo["data"]["appid"]);
					let req = PayReq()
//					req.openID = responseInfo["data"]["partnerid"].string!
					req.partnerId = responseInfo["data"]["partnerid"].string!
					req.prepayId = responseInfo["data"]["prepayid"].string!
					req.package = responseInfo["data"]["package"].string!
					req.nonceStr = responseInfo["data"]["noncestr"].string!
					req.timeStamp = UInt32(responseInfo["data"]["timestamp"].int32!)
					req.sign = responseInfo["data"]["sign"].string!
					
					WXApi.send(req)
				}
			}
	}
	
	
	
    private func readerConfiguration() -> FolioReaderConfig {
        let config = FolioReaderConfig()
        
        //        config.localizedShareWebLink: URL? = nil
        //        config.quoteCustomLogoImage =
        
        config.shouldHideNavigationOnTap = false
        config.scrollDirection = .horizontal
        config.displayTitle = true
        config.localizedHighlightMenu = "高亮"
        config.localizedDefineMenu = "查询"
        config.localizedPlayMenu = "播放"
        config.localizedPauseMenu = "暂停"
        config.localizedFontMenuNight = "夜间模式"
        config.localizedPlayerMenuStyle = "样式"
        config.localizedFontMenuDay = "白昼模式"
        config.localizedLayoutHorizontal = "水平翻页"
        config.localizedLayoutVertical = "垂直翻页"
        config.localizedReaderOnePageLeft = "还剩1页"
        config.localizedReaderManyPagesLeft = "剩余页数"
        config.localizedReaderManyMinutes = "剩余分钟"
        config.localizedReaderOneMinute = "还剩分钟"
        config.localizedReaderLessThanOneMinute = "少于1分钟"
        config.localizedShareChapterSubject = "选自章节"
        config.localizedShareHighlightSubject = "笔记来自于"
        config.localizedShareAllExcerptsFrom = "节选自"
        config.localizedShareBy = "来自"
        config.localizedCancel = "取消"
        config.localizedShare = "分享"
        config.localizedChooseExisting = "选择已存"
        config.localizedTakePhoto = "照相"
        config.localizedShareImageQuote = "分享引用的图片"
        config.localizedShareTextQuote = "分享引用的文字"
        config.localizedHighlightsTitle = "高亮"
        config.localizedContentsTitle = "目录"
        // Custom sharing quote background
        //        config.quoteCustomBackgrounds = []
        //        if let image = UIImage(named: "demo-bg") {
        //            let customImageQuote = QuoteImage(withImage: image, alpha: 0.6, backgroundColor: UIColor.black)
        //            config.quoteCustomBackgrounds.append(customImageQuote)
        //        }
        
        config.tintColor = UIColor(red:0.98, green:0.02, blue:0.19, alpha:1.0)
        
        let textColor = UIColor(red:0.86, green:0.73, blue:0.70, alpha:1.0)
        let customColor = UIColor(red:0.98, green:0.02, blue:0.19, alpha:1.0)
        let customQuote = QuoteImage(withColor: customColor, alpha: 1.0, textColor: textColor)
        config.quoteCustomBackgrounds.append(customQuote)
        
        return config
    }

}

//extension UIDevice {
//    /// 是否是iPhone X
//    ///
//    /// - Returns: iPhone X
//    public func isiPhoneX() -> Bool {
//        if UIScreen.main.bounds.height == 812 {
//            return true
//        }
//        return false
//    }
//}

