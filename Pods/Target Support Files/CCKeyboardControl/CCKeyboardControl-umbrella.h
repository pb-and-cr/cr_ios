#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "CCKeyboardControl.h"

FOUNDATION_EXPORT double CCKeyboardControlVersionNumber;
FOUNDATION_EXPORT const unsigned char CCKeyboardControlVersionString[];

