// { "framework": "Vue" }

/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 161);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.paramsToString = paramsToString;
exports.jump = jump;
exports.back = back;
exports.getWithParameter = getWithParameter;
exports.jumpSubPage = jumpSubPage;

var _util = __webpack_require__(39);

var _util2 = _interopRequireDefault(_util);

var _prompt = __webpack_require__(2);

var _prompt2 = _interopRequireDefault(_prompt);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function getNavigator() {
    var nav = weex.requireModule('navigator');
    return nav;
};

function paramsToString(obj) {
    var param = "";
    for (var name in obj) {
        param += "&" + name + "=" + encodeURI(obj[name]);
    }
    return param.substring(1);
};

function getUrl(url, jsFile) {
    var bundleUrl = url;
    var host = '';
    var path = '';
    var nativeBase = '';
    var isWebAssets = bundleUrl.indexOf('http://') >= 0;
    var isAndroidAssets = bundleUrl.indexOf('file://assets/') >= 0;
    var isiOSAssets = bundleUrl.indexOf('file:///') >= 0 && bundleUrl.indexOf('CReader.app') > 0;
    if (isAndroidAssets) {
        nativeBase = 'file://assets/';
    } else if (isiOSAssets) {
        // file:///var/mobile/Containers/Bundle/Application/{id}/WeexDemo.app/
        // file:///Users/{user}/Library/Developer/CoreSimulator/Devices/{id}/data/Containers/Bundle/Application/{id}/WeexDemo.app/
        nativeBase = bundleUrl.substring(0, bundleUrl.lastIndexOf('/') + 1);
    } else {
        var matches = /\/\/([^\/]+?)\//.exec(bundleUrl);
        var matchFirstPath = /\/\/[^\/]+\/([^\/]+)\//.exec(bundleUrl);
        if (matches && matches.length >= 2) {
            host = matches[1];
        }
        if (matchFirstPath && matchFirstPath.length >= 2) {
            path = matchFirstPath[1];
        }
        nativeBase = 'http://' + host + '/';
    }
    var h5Base = './index.html?page=';
    // in Native
    var base = nativeBase;
    if (typeof navigator !== 'undefined' && (navigator.appCodeName === 'Mozilla' || navigator.product === 'Gecko')) {
        // check if in weexpack project
        if (path === 'web' || path === 'dist') {
            base = h5Base + '/dist/';
        } else {
            base = h5Base + '';
        }
    } else {
        base = nativeBase + (!!path ? path + '/' : '');
    }

    var newUrl = base + jsFile;
    return newUrl;
}
// 跳转到新的url地址
function jump(self, url) {
    if (WXEnvironment.platform == 'Web') {
        window.location.href = "/web/" + url + ".html";
    } else {

        var path = self.$getConfig().bundleUrl;

        path = _util2.default.setWXBundleUrl(path, url + ".js");
        // prompt.alert(path);
        getNavigator().push({
            url: url,
            animated: "true"
        });
    }
}

function back(self) {
    if (WXEnvironment.platform == 'Web') {
        window.history.go(-1);
    } else {
        getNavigator().pop({
            animated: "true"
        });
    }
}

function getWithParameter(self, url, parameter) {
    if (WXEnvironment.platform == 'Web') {
        window.location.href = "/web/" + url + ".html?phantom_limb=true&" + paramsToString(parameter);
    } else {
        var path = self.$getConfig().bundleUrl;
        path = getUrl(path, url + ".js?" + paramsToString(parameter));
        var event = weex.requireModule('event');
        event.openURL(path);
    }
}

function jumpSubPage(self, url, parameter) {
    if (WXEnvironment.platform == 'Web') {
        window.location.href = "/web/" + url + ".html?phantom_limb=true&" + paramsToString(parameter);
    } else {
        var event = weex.requireModule('event');
        var path = self.$getConfig().bundleUrl;
        console.log(path + "qingsongtest0000000000");
        var h = path.includes('?');
        console.log(h);
        if (h) {
            var questionIndex = path.indexOf("?");
            path = path.slice(0, questionIndex);
            path = getUrl(path, url + ".js?isSubPage&" + paramsToString(parameter));
            event.openURL(path);
        } else {
            path = getUrl(path, url + ".js?isSubPage&" + paramsToString(parameter));
            event.openURL(path);
        }
    }
}

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.getImgPath = getImgPath;
exports.getIconFontPath = getIconFontPath;
exports.logVideoPlayEvent = logVideoPlayEvent;
exports.checkNetStatusOnOrOff = checkNetStatusOnOrOff;
exports.getTabBarHeight = getTabBarHeight;

var _prompt = __webpack_require__(2);

var _prompt2 = _interopRequireDefault(_prompt);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// 获取图片在三端上不同的路径
// e.g. 图片文件名是 test.jpg, 转换得到的图片地址为
// - H5      : http: //localhost:1337/src/images/test.jpg
// - Android : local:///test
// - iOS     : ../images/test.jpg
function getImgPath(img_name) {
    var bundleUrl = weex.config.bundleUrl;
    var platform = weex.config.env.platform;
    var img_path = '';
    if (platform == 'Web' || bundleUrl.indexOf("http") >= 0) {
        img_path = '/assets/images/' + img_name;
    } else if (platform == 'android') {
        // android 不需要后缀
        img_name = img_name.substr(0, img_name.lastIndexOf('.'));
        img_path = 'local:///' + img_name;
    } else {
        //ios
        img_path = 'local:///bundlejs/images/' + img_name;
    }
    return img_path;
};

function getIconFontPath() {
    var bundleUrl = weex.config.bundleUrl;
    var platform = weex.config.env.platform;
    var iconFont_path = '';
    // console.log("platform: "+platform);
    // console.log("bundleUrl: "+bundleUrl);
    if (platform == 'android') {
        // android 不需要后缀
        // console.log("platform: android -- ");
        iconFont_path = 'local:///font/';
    } else if (platform == 'iOS') {
        //ios 
        // iconFont_path =bundleUrl.substring(0, bundleUrl.lastIndexOf('/') + 1)+`font/`
        iconFont_path = 'local:///bundlejs/font/';
    } else {
        iconFont_path = '/assets/font/';
    }
    // console.log("iconFont_path: "+iconFont_path);
    return iconFont_path;
}

// 发送信息到 google analytics
function logVideoPlayEvent(videoName, videoId, album) {
    if (WXEnvironment.platform != 'Web') {
        var event = weex.requireModule('event');
        if (typeof event.logVideoPlayEvent != "undefined") {
            event.logVideoPlayEvent(videoName, videoId, album);
        }
    }
};

function checkNetStatusOnOrOff() {
    if (WXEnvironment.platform != 'Web') {
        try {
            var event = weex.requireModule('event');
            if (typeof event.isNetworkAvailable != "undefined") {
                return event.isNetworkAvailable();
            }
        } catch (e) {
            return 1;
        }
    } else {
        return 1;
    }
};

function getTabBarHeight() {

    try {
        var event = weex.requireModule('event');
        // console.log("qingsongtabbar"+event.getTabBarHeightForDiffPhone);
        return event.getTabBarHeightForDiffPhone();
    } catch (e) {
        return 98;
    }
};

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var prompt = {
    getModel: function getModel() {
        return weex.requireModule('modal');
    },
    toast: function toast(info) {
        this.getModel().toast({ message: info, duration: 0.3 });
    },
    alert: function alert(info) {
        this.getModel().alert({ message: info, duration: 0.3 });
    }
};

exports.default = prompt;

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var environment = {
  language: "zh" //改变这里的值。来切换中英文
};

var langFiles = {
  en: __webpack_require__(44).lang,
  zh: __webpack_require__(46).lang,
  zh_tw: __webpack_require__(45).lang
};

var already = false;

var global = {

  init: function init() {
    if (already == true) {
      return;
    }
    console.log(" global init ");
    var platform = weex.config.env.platform;
    var lang = "zh";
    try {
      var deviceInfo = weex.requireModule('deviceInfo');
      if (typeof deviceInfo != "undefined" && typeof deviceInfo.GetLanguage == "function") {
        lang = deviceInfo.GetLanguage().toLocaleLowerCase();
      }
    } catch (e) {
      console.log(" lang " + e);
    }

    if (platform.toLocaleLowerCase() == "ios") {
      // # ios #
      // 中文简体：zh-Hans
      // 英文：en
      // 中文繁体：zh-Hant
      // 印地语：hi
      // 西班牙语：es
      // 墨西哥西班牙语：es-MX
      // 拉丁美洲西班牙语：es-419
      if (lang.indexOf("zh") >= 0) {
        if (lang.indexOf("hans") >= 0) {
          environment.language = "zh";
        } else {
          environment.language = "zh_tw";
        }
      } else if (lang.indexOf("en") >= 0) {
        environment.language = "en";
      } else if (lang.indexOf("es") >= 0) {
        environment.language = "en";
      } else {
        environment.language = "zh";
      }
    } else {
      if (lang.indexOf("zh_cn") >= 0 || lang.indexOf("zh_cn") >= 0) {
        environment.language = "zh";
      } else if (lang.indexOf("zh_") >= 0) {
        environment.language = "zh_tw";
      } else if (lang.indexOf("cn") >= 0 || lang.indexOf("cn") >= 0) {
        environment.language = "zh";
      } else if (lang.indexOf("en") >= 0) {
        environment.language = "en";
      } else {
        environment.language = "zh";
      }
    }
  },
  lang: function lang(key) {
    return "";
  },
  display: function display(key) {
    if (already == false) {
      this.init();
    }
    console.log(" already " + already);
    if (environment.language == "zh") {
      return langFiles.zh[key] || "not find";
    } else if (environment.language == "en") {
      return langFiles.en[key] || "not find";
    } else if (environment.language == "zh-tw") {
      return langFiles.zh_tw[key] || "not find";
    }
    return key;
  },
  getLanguage: function getLanguage() {
    if (already == false) {
      this.init();
    }
    return environment.language;
  }
};

exports.default = global;
//
// setTimeout(function(){
//     exports.lang = function(key) {
//         if (environment.language == "zh") {
//             return langFiles.zh[key] || "not find";
//         }
//         else if (environment.language == "en") {
//             return langFiles.en[key] || "not find";
//         }
//         else if (environment.language == "zh-tw") {
//             return langFiles.zh_tw[key] || "not find";
//         }
//         return key;
//     }
// },100);

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.getBookShelf = getBookShelf;
exports.saveBookShelf = saveBookShelf;
exports.openBook = openBook;
exports.getBookListByService = getBookListByService;
exports.getArticleListInfo = getArticleListInfo;
exports.getArticleListByBrowse = getArticleListByBrowse;
exports.getSubscribeInfo = getSubscribeInfo;
exports.queryAllAlbumLists = queryAllAlbumLists;
exports.getAlbumInfoById = getAlbumInfoById;
exports.getBooks = getBooks;
exports.getBookProfile = getBookProfile;
exports.userAddBookToShelf = userAddBookToShelf;
exports.userRemoveBookToShelf = userRemoveBookToShelf;
exports.serchBookName = serchBookName;
exports.searchResult = searchResult;
exports.userAddAticleToLove = userAddAticleToLove;
exports.userRemoveAticleFromLove = userRemoveAticleFromLove;
exports.isArticleInLove = isArticleInLove;
exports.userGetLovedArticle = userGetLovedArticle;
exports.getArticleInfoById = getArticleInfoById;
exports.getArticleOtherInfo = getArticleOtherInfo;
exports.shareArticleToFriends = shareArticleToFriends;
exports.getSuggestBookLists = getSuggestBookLists;
exports.getBookListsInfo = getBookListsInfo;
exports.getPersonalData = getPersonalData;
exports.getFollowColumnList = getFollowColumnList;
exports.getNotsubscribedColumnInfo = getNotsubscribedColumnInfo;
exports.userFollow = userFollow;
exports.userRemoveColumn = userRemoveColumn;
exports.getColumnInfo = getColumnInfo;
exports.getBookList = getBookList;
exports.getBookListByTag = getBookListByTag;

var _fetch = __webpack_require__(6);

var _user = __webpack_require__(5);

var _storage = __webpack_require__(9);

var _global = __webpack_require__(3);

var _global2 = _interopRequireDefault(_global);

var _syncAblumInfo = __webpack_require__(8);

var _syncAblumInfo2 = _interopRequireDefault(_syncAblumInfo);

var _prompt = __webpack_require__(2);

var _prompt2 = _interopRequireDefault(_prompt);

var _navigator = __webpack_require__(0);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// import googleTrack from '../02.bus/googleTrack';

var BOOK_SHELF = 'BookShelf';

//用户获取书架信息
function getBookShelf() {
    //登陆成功后返回token，和user_id
    //let openId = getOpenId();
    //let user_id = getUserId();
    return (0, _storage.storageGetItem)(BOOK_SHELF).then(function (res) {
        // console.log(res.data);
        if (res.result == "failed") {
            return [];
        } else {
            var data = JSON.parse(res.data);
            var len = data.length;
            for (var i = 0; i < len; i++) {
                data[i]['progress'] = { "visible": false, "value": 0 };
            }
            console.log(data);
            return data;
        }
    });
}

//用户获取书架信息
function saveBookShelf(bookList) {
    //登陆成功后返回token，和user_id
    return (0, _storage.storageSetItem)(BOOK_SHELF, JSON.stringify(bookList));
}

function openBook(bookInfo) {}

//从服务器端同步数据到本地
function getBookListByService(bookList, pageIndex) {
    return (0, _user.getUserId)().then(function (userId) {
        return (0, _fetch.get)('/api/Books/getBooksOnShelf', { 'user_id': userId, "page_index": pageIndex });
    });
}

function getArticleListInfo(pageIndex) {
    return (0, _user.getOpenId)().then(function (openId) {
        return (0, _fetch.get)('/api/Article/v1/index', { 'openId': openId, 'pageIndex': pageIndex });
    });
}

function getArticleListByBrowse(pageIndex) {
    return (0, _user.getOpenId)().then(function (openId) {
        return (0, _fetch.get)('/api/Article/v1/getArticleListByBrowse', { 'openId': openId, 'pageIndex': pageIndex });
    });
}
function getSubscribeInfo() {
    return (0, _user.getOpenId)().then(function (openId) {
        return (0, _fetch.get)('/api/follow/v1/index', { 'openId': openId });
    });
    // return get('/api/follow/v1/index', { 'openId': openId}, fun);
}
function queryAllAlbumLists() {
    return (0, _user.getOpenId)().then(function (openId) {
        return (0, _fetch.get)('/api/follow/v1/queryAllAlbumLists', { 'openId': openId });
    });
}
function getAlbumInfoById(id, pageIndex) {
    return (0, _user.getOpenId)().then(function (openId) {
        return (0, _fetch.get)('/api/Article/v1/album', { 'openId': openId, 'pageIndex': pageIndex, 'id': id });
    });
    // return get('/api/Article/v1/album', { 'openId': openId, 'id': id,'pageIndex':pageIndex}, fun);
}
//获取图书信息
function getBooks(id, pageIndex) {
    return (0, _user.getOpenId)().then(function (openId) {
        return (0, _fetch.get)('/api/Books/getBooksTags', { 'openId': openId, 'id': id, 'pageIndex': pageIndex });
    });
    // return get('/api/Books/getBooksTags',{'openId': openId,'id': id,'pageIndex':pageIndex},fun);
}
//获取书籍信息
function getBookProfile(id) {
    return (0, _user.getOpenId)().then(function (openId) {
        return (0, _fetch.get)('/api/Books/getBookProfile', { 'openId': openId, 'id': id });
    });
}
//用户添加书籍到书架
function userAddBookToShelf(bookId) {
    //登陆成功后返回token，和user_id
    //let openId = getOpenId();
    //let user_id = getUserId();
    // let user_id = 7245;
    //type = 1 表示书籍
    return (0, _user.getUserId)().then(function (userId) {
        return (0, _fetch.get)('/api/Books/addBookToShelf', { 'article_book_id': bookId, 'user_id': userId, 'type': 1 });
    });
    // return get('/api/Books/addBookToShelf', { 'book_id': bookId, 'user_id': user_id}, fun);
}
//用户从书架移出书籍
function userRemoveBookToShelf(bookId) {
    //登陆成功后返回token，和user_id
    //let openId = getOpenId();
    //let user_id = getUserId();
    // let user_token = 7245;
    // return get('/api/Books/removeBookFromShelf', { 'book_id': bookId, 'user_token': user_token}, fun);
    return (0, _user.getUserId)().then(function (userId) {
        console.log(" userId -  " + userId);
        if (userId <= 0) {
            return { data: { status: 1 } };
        } else {
            return (0, _fetch.get)('/api/Books/removeBookFromShelf', { 'article_book_id': bookId, 'user_id': userId, 'type': 1 }).then(function (res) {
                if (userId <= 0) {
                    return { data: { status: 1 } };
                } else {
                    return res;
                }
            });
        }
    });
}
//用户搜索图书
function serchBookName(name, pageIndex) {
    return (0, _user.getUserId)().then(function (userId) {
        return (0, _fetch.post)('/api/Books/searchBook', { 'title': name, 'pageIndex': pageIndex });
    });
}

//用户搜素文章，图书，专栏
function searchResult(value, type, pageIndex) {
    //type = 1,查询书;2表示查询文章;3，表示查询专辑
    return (0, _user.getUserId)().then(function (userId) {
        return (0, _fetch.post)('/api/follow/searchResult', { 'title': value, 'pageIndex': pageIndex, 'type': type, 'user_id': userId });
    });
}
//用户添加文章到我的收藏
function userAddAticleToLove(articleId, loveCount) {
    //type = 2 表示文章
    return (0, _user.getUserId)().then(function (userId) {
        if (userId == -1) {
            return -1;
        } else {
            return (0, _fetch.get)('/api/Article/addArticleToLove', { 'article_book_id': articleId, 'love_count': loveCount, 'user_id': userId, 'type': 2 });
        }
    });
}
//用户移出文章到我的收藏
function userRemoveAticleFromLove(articleId, loveCount) {
    return (0, _user.getUserId)().then(function (userId) {
        return (0, _fetch.get)('/api/Article/removeArticleFromLove', { 'article_book_id': articleId, 'love_count': loveCount, 'user_id': userId });
    });
}
function isArticleInLove(articleId) {
    return (0, _user.getUserId)().then(function (userId) {
        return (0, _fetch.get)('/api/Article/isArticleInLove', { 'article_book_id': articleId, 'user_id': userId });
    });
}
//用户获取收藏的文章信息
function userGetLovedArticle(pageIndex) {
    //type = 2 表示文章
    return (0, _user.getUserId)().then(function (userId) {
        return (0, _fetch.get)('/api/Article/articleInLove', { 'user_id': userId, 'pageIndex': pageIndex, 'type': 2 });
    });
}
//用户阅读文章，获取文章的相关信息
function getArticleInfoById(id) {
    return (0, _user.getUserId)().then(function (userId) {
        return (0, _fetch.get)('/api/Article/articleInfoById', { 'id': id, 'user_id': userId });
    });
}

//用户阅读文章，获取文章的相关信息
function getArticleOtherInfo(id, album_id) {
    return (0, _user.getUserId)().then(function (userId) {
        return (0, _fetch.get)('/api/Article/getArticleOtherInfo', { 'id': id, 'user_id': userId, 'album_id': album_id });
    });
}

//用户分享文章
function shareArticleToFriends(id) {
    return (0, _user.getUserId)().then(function (userId) {
        return (0, _fetch.get)('/api/Article/shareArticles', { 'id': id });
    });
}

//获取书单
function getSuggestBookLists() {
    // let openId = getOpenId();
    return (0, _user.getOpenId)().then(function (openId) {
        return (0, _fetch.get)('/api/Books/getSuggestBookLists', { 'openId': openId });
    });
    // return get('/api/Books/getSuggestBookLists',{'openId': openId},fun);
}
//获取书单下的详细书本信息
function getBookListsInfo(bookList_id) {
    // let openId = getOpenId();
    return (0, _user.getOpenId)().then(function (openId) {
        return (0, _fetch.get)('/api/Books/getBookListsInfo', { 'openId': openId, 'bookList_id': bookList_id });
    });
    // return get('/api/Books/getBookListsInfo',{'openId': openId,'bookList_id':bookList_id},fun);
}
//用户获取个人信息
function getPersonalData() {
    // let openId = getOpenId();
    // let userId = getUserId();
    return (0, _user.getOpenId)().then(function (openId) {
        return (0, _fetch.get)('/api/ChristianReaderUser/getPersonalData', { 'openId': openId }).then(function (res) {
            if (res.data.status == 1) {
                (0, _user.setUserInfo)(res.data.data);
            } else {
                (0, _user.setUserInfo)(res.data.data);
            }
            return res;
        });
    });
    // return getPromise('/api/ChristianReaderUser/getPersonalData',{'openId': openId});
    // return get('/api/ChristianReaderUser/getPersonalData',{'openId': openId},fun);
}

//获得用户已经订阅的专栏信息：
function getFollowColumnList() {
    return (0, _user.getOpenId)().then(function (openId) {
        return (0, _fetch.get)('/api/follow/v1/followColumnInfo', { 'openId': openId });
    });
    // return get('/api/follow/v1/followColumnInfo', { 'openId': openId}, fun);
}
//获得用户未订阅的专栏信息：
function getNotsubscribedColumnInfo() {
    return (0, _user.getOpenId)().then(function (openId) {
        return (0, _fetch.get)('/api/follow/v1/notsubscribedList', { 'openId': openId });
    });
    // return get('/api/follow/v1/notsubscribedList', { 'openId': openId}, fun);
}
//用户订阅某个专栏
function userFollow(columnId) {
    return (0, _user.getOpenId)().then(function (openId) {
        return (0, _fetch.get)('/api/follow/v1/userFollow', { 'columnId': columnId, 'openId': openId });
    });
    // syncAblumInfo.follow(columnId);
    // return get('/api/follow/v1/userFollow', { 'columnId': columnId, 'openId': openId}, fun);
}
//用户取消某个专栏的订阅：
function userRemoveColumn(columnId) {
    return (0, _user.getOpenId)().then(function (openId) {
        return (0, _fetch.get)('/api/follow/v1/userRemoveColumn', { 'columnId': columnId, 'openId': openId });
    });
    // syncAblumInfo.cancel(columnId);
    // return get('/api/follow/v1/userRemoveColumn', { 'columnId': columnId, 'openId': openId}, fun);
}
//根据专栏id获得专栏详细信息
function getColumnInfo(columnId) {
    return (0, _user.getOpenId)().then(function (openId) {
        return (0, _fetch.get)('/api/follow/v1/getColumnInfo', { 'columnId': columnId, 'openId': openId });
    });
    // return get('/api/follow/v1/getColumnInfo', { 'columnId': columnId, 'openId': openId , 'language':global.getLanguage()}, fun);
}
function getBookList() {
    return (0, _user.getOpenId)().then(function (openId) {
        return (0, _fetch.get)('/api/shop/getBooksInShop', { 'openId': openId });
    });
    // return get('/api/shop/getBooksInShop', { 'columnId': columnId, 'openId': openId , 'language':global.getLanguage()}, fun);
}
function getBookListByTag(tagid) {
    return (0, _user.getOpenId)().then(function (openId) {
        return (0, _fetch.get)('/api/shop/getBooksInShopByTag', { 'openId': openId, 'tag_id': tagid });
    });
    // return get('/api/shop/getBooksInShop', { 'columnId': columnId, 'openId': openId , 'language':global.getLanguage()}, fun);
}

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.getOpenId = getOpenId;
exports.getUserId = getUserId;
exports.getUserInfo = getUserInfo;
exports.exit = exit;
exports.setUserInfo = setUserInfo;
exports.login = login;
exports.registerByEmail = registerByEmail;
exports.registerByPhone = registerByPhone;
exports.getPswBackByEmail = getPswBackByEmail;
exports.getPswBackByPhone = getPswBackByPhone;
exports.sendPhoneCode = sendPhoneCode;
exports.updatePsw = updatePsw;
exports.updateProfile = updateProfile;
exports.userFeedback = userFeedback;

var _fetch = __webpack_require__(6);

var _storage = __webpack_require__(9);

var _global = __webpack_require__(3);

var _global2 = _interopRequireDefault(_global);

var _syncAblumInfo = __webpack_require__(8);

var _syncAblumInfo2 = _interopRequireDefault(_syncAblumInfo);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var syncUser = new BroadcastChannel('christianReaderUser');
var USER_KEY = 'userKey'; //用户存储的KeyValue

var syncUserInfo = {
    receive: function receive(fun) {
        syncUser.onmessage = function (event) {
            // console.log(event.data) // Assemble!
            fun();
        };
    },
    post: function post() {
        var message = {
            status: 'update'
        };
        syncUser.postMessage(message);
    },
    close: function close(fun) {
        syncUser.close();
    }
};

exports.default = syncUserInfo;
function getOpenId() {
    return (0, _storage.storageGetItem)(USER_KEY).then(function (res) {
        if (res.result == "failed") {
            return "";
        } else {
            console.log("openId: " + res.data);
            return JSON.parse(res.data).openId;
            // return "8A471FD74AF017EE0680381611D442CD";
        }
        // return "8A471FD74AF017EE0680381611D442CD";
    });
    // return "8A471FD74AF017EE0680381611D442CD";
}

function getUserId() {
    return (0, _storage.storageGetItem)(USER_KEY).then(function (res) {
        if (res.result == "failed") {
            return -1;
        } else {
            console.log("openId: " + res.data);
            return JSON.parse(res.data).id;
        }
    });
}

function getUserInfo() {
    return (0, _storage.storageGetItem)(USER_KEY).then(function (res) {

        if (res.result == "failed") {
            var _ref;

            return _ref = {
                id: -1,
                openId: '',
                head_icon: (0, _fetch.getHost)() + '/static/default/2.jpg',
                nickName: '主的好孩子,没有任何信息',
                profile: '点击这里登录或注册'
            }, _defineProperty(_ref, 'openId', ''), _defineProperty(_ref, 'bookCounts', 0), _defineProperty(_ref, 'loveCounts', 0), _defineProperty(_ref, 'followCounts', 0), _ref;
        } else {
            return JSON.parse(res.data);
        }
    });
}

//更新资料
function exit() {
    (0, _storage.storageRemoveItem)(USER_KEY);
}

function setUserInfo(value) {
    return (0, _storage.storageSetItem)(USER_KEY, JSON.stringify(value));
}

//登陆
function login(userName, password) {
    return (0, _fetch.post)('/api/ChristianReaderUser/login', { 'userName': userName, 'password': password }).then(function (res) {
        console.log(" -- login -- ");
        console.log(res);
        if (res.data.status == 1) {
            setUserInfo(res.data.data);
        }
        return res;
    });
    // return post('/api/ChristianReaderUser/login', { 'userName': userName, 'password': password}, fun);
}
//用户邮箱注册
function registerByEmail(nickName, email, password) {
    return (0, _fetch.post)('/api/ChristianReaderUser/registerByEmail', { 'nickName': nickName, 'email': email, 'password': password });
    // return post('/api/ChristianReaderUser/registerByEmail', { 'nickName': nickName ,'email': email,'password': password}, fun);
}
//用户手机注册
function registerByPhone(nickName, mobile, verifyCode, password) {
    return (0, _fetch.post)('/api/ChristianReaderUser/registerByPhone', { 'nickName': nickName, 'mobile': mobile, 'verifyCode': verifyCode, 'password': password });
    // return post('/api/ChristianReaderUser/registerByPhone', { 'nickName': nickName ,'mobile': mobile,'verifyCode': verifyCode,'password': password}, fun);
}
//找回密码
function getPswBackByEmail(email) {
    return (0, _fetch.post)('/api/ChristianReaderUser/getPasswordBack', { 'email': email });
    // return post('/api/ChristianReaderUser/getPasswordBack', { 'email': email}, fun);
}
function getPswBackByPhone(mobile, verifyCode) {
    return (0, _fetch.post)('/api/ChristianReaderUser/getPasswordBackByPhone', { 'mobile': mobile, 'verifyCode': verifyCode });
    // return post('/api/ChristianReaderUser/getPasswordBackByPhone', { 'mobile': mobile, 'verifyCode': verifyCode,}, fun);
}
//发送手机验证码
function sendPhoneCode(mobile) {
    return (0, _fetch.post)('/api/ChristianReaderUser/getSms', { 'mobile': mobile });
}
//修改密码
function updatePsw(mobile, password) {
    return (0, _fetch.post)('/api/ChristianReaderUser/updatePswByPhone', { 'mobile': mobile, 'password': password });
}
//更新资料
function updateProfile(name, nickName, des) {
    return getOpenId().then(function (openId) {
        return (0, _fetch.post)('/api/ChristianReaderUser/updatePersonalProfile', { 'name': name, 'nickName': nickName, 'brief_personal_des': des, 'user_token': openId }).then(function (res) {
            if (res.data.status == 1) {
                getUserInfo().then(function (res) {
                    res.nickName = nickName;
                    res.profile = des;
                    setUserInfo(res);
                });
            }
            return res;
        });
    });
}
//用户反馈
function userFeedback(contactWays, content) {
    return (0, _fetch.post)('/api/ChristianReaderUser/feedback', { 'ContactInformation': contactWays, 'Content': content, 'AppInfo': '基督徒阅读' });
}

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.getHost = getHost;
exports.fetch = fetch;
exports.get = get;
exports.post = post;
// const host = "http://file.biblemooc.net/";
// const host = "http://172.20.10.4:1099";
var host = "http://47.244.180.248:1099";
// const host = "http://192.168.2.127:12000";
// const host = "http://read.biblemooc.net";

function getHost() {
    return host;
}

function getStream() {
    var stream = weex.requireModule('stream');
    return stream;
}

function paramsToString(obj) {
    var param = "";
    for (var name in obj) {
        param += "&" + name + "=" + obj[name];
    }
    return param.substring(1);
}

function fetch(path, method, param, successFun) {
    path = host + path;
    getStream().fetch({
        method: method,
        url: path,
        type: 'json',
        body: paramsToString(param)
    }, successFun);
    console.log(path);
}

// export function  get(path,param,fun) {
//     console.log(host+path+"?"+paramsToString(param));
//     return getStream().fetch({
//         method: 'GET',
//         type: 'json',
//         url: host+path+"?"+paramsToString(param),
//     }, fun)
// }

function get(path, param) {
    console.log(host + path + "?" + paramsToString(param));
    return new Promise(function (resolve, reject) {
        getStream().fetch({
            method: 'GET',
            url: host + path + "?" + paramsToString(param),
            type: 'json'
        }, function (ret) {
            resolve(ret);
        });
    });
}
//
// export function post(path,param,successFun)
// {
//     path = host+path;
//     console.log(path);
//     console.log(paramsToString(param));
//     getStream().fetch({
//             method: 'POST',
//             url: path,
//             type: 'json',
//             headers:{'Content-Type':'application/x-www-form-urlencoded'},
//             body: paramsToString(param)
//         }
//         ,successFun);
// }

function post(path, param) {
    path = host + path;
    console.log(path);
    console.log(paramsToString(param));
    return new Promise(function (resolve, reject) {
        getStream().fetch({
            method: 'POST',
            url: path,
            type: 'json',
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            body: paramsToString(param)
        }, function (ret) {
            resolve(ret);
        });
    });
}

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _common = __webpack_require__(1);

exports.default = {
    tabItems: [{
        index: 0,
        title: "主页",
        titleColor: '#666666',
        selectedColor: '#E15D53',
        fontSize: '25px',
        iconWdith: '48px',
        iconHeight: '48px',
        iconFont: 'ion-android-home',
        image: (0, _common.getImgPath)("home.png"),
        selectedImage: (0, _common.getImgPath)('homeselected.png'),
        ref: 'home'
    }, {
        index: 1,
        title: "专栏",
        titleColor: '#666666',
        selectedColor: '#E15D53',
        fontSize: '25px',
        iconWdith: '48px',
        iconHeight: '48px',
        iconFont: 'ion-help-buoy',
        image: (0, _common.getImgPath)("library.png"),
        selectedImage: (0, _common.getImgPath)("libraryselected.png"),
        ref: 'album'
    },
    //   {
    //       index:2,  
    //       title:"消息",   
    //       titleColor:'#666666',  
    //       selectedColor: '#E15D53',
    //       fontSize: '25px',
    //       iconWdith: '48px',
    //       iconHeight: '48px',
    //       iconFont:'ion-chatboxes',
    //       image:getImgPath("subscribe.png"),  
    //       selectedImage:getImgPath("subscribeselected.png"),  
    //       ref:'chat',
    //   },
    {
        index: 2,
        title: "图书",
        titleColor: '#666666',
        selectedColor: '#E15D53',
        fontSize: '25px',
        iconWdith: '48px',
        iconHeight: '48px',
        iconFont: 'ion-ios-book',
        image: (0, _common.getImgPath)("chat.png"),
        selectedImage: (0, _common.getImgPath)("chatselected.png"),
        ref: 'books'
    }, {
        index: 3,
        title: "我",
        titleColor: '#666666',
        selectedColor: '#E15D53',
        fontSize: '25px',
        iconWdith: '48px',
        iconHeight: '48px',
        iconFont: 'ion-ios-person',
        image: (0, _common.getImgPath)("me1.png"),
        selectedImage: (0, _common.getImgPath)("meselected.png"),
        ref: 'me'
    }],
    bookstoremessage: { message: "来自合作伙伴的精选书籍推荐给您" },
    bottommessage: { message: "没有更多了!" }

    // 正常模式的tab title配置
    // tabTitles: [
    //   {
    //     title: '主页',
    //     icon: getImgPath("home.png"),
    //     activeIcon: getImgPath('homeselected.png'),
    //   },
    //   {
    //     title: '图书馆',
    //     icon: getImgPath("library.png"),
    //     activeIcon: getImgPath("libraryselected.png")
    //   },
    //   {
    //     title: '订阅',
    //     icon: getImgPath("subscribe.png"),
    //     activeIcon: getImgPath("subscribeselected.png")
    //   },
    //   {
    //     title: '答疑',
    //     icon: getImgPath("chat.png"),
    //     activeIcon: getImgPath("chatselected.png")
    //   },
    //   {
    //     title: '我',
    //     icon: getImgPath("me1.png"),
    //     activeIcon: getImgPath("meselected.png")
    //   }
    // ],
    // tabStyles: {
    //   bgColor: '#FFFFFF',
    //   titleColor: '#666666',
    //   activeTitleColor: '#E15D53',
    //   activeBgColor: '#ffffff',
    //   isActiveTitleBold: true,
    //   iconWidth: 60,
    //   iconHeight: 60,
    //   width: 160,
    //   height: 120,
    //   BottomColor: '#FFC900',
    //   BottomWidth: 0,
    //   BottomHeight: 0,
    //   activeBottomColor: 'green',
    //   activeBottomWidth: 160,
    //   activeBottomHeight: 6,
    //   fontSize: 24,
    //   textPaddingLeft: 10,
    //   textPaddingRight: 10
    // },
};

/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var sync = new BroadcastChannel('christian reader');

var syncAblumInfo = {
  receive: function receive(fun) {
    sync.onmessage = fun;
  },
  follow: function follow(ablumnId) {
    var message = {
      status: 'add',
      id: ablumnId
    };
    sync.postMessage(message);
  },
  cancel: function cancel(ablumnId) {
    var message = {
      status: 'cancel',
      id: ablumnId
    };
    sync.postMessage(message);
  },
  getData: function getData() {
    var message = {
      status: 'getData'
    };
    sync.postMessage(message);
  },
  close: function close(ablumnId, fun) {
    sync.close();
  }
};

exports.default = syncAblumInfo;

/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.storageSetItem = storageSetItem;
exports.storageGetItem = storageGetItem;
exports.storageRemoveItem = storageRemoveItem;

var storage = weex.requireModule('storage');

function storageSetItem(key, value) {
    // console.log(' -- storageSetItem -- ');
    // console.log(value);
    return new Promise(function (resolve, reject) {
        storage.setItem(key, value, function (event) {
            // console.log(event);
            resolve(event);
        });
    });
};

function storageGetItem(key) {
    return new Promise(function (resolve, reject) {
        storage.getItem(key, function (event) {
            // console.log(" storage event ");
            // console.log(event);
            resolve(event);
        });
    });
}

function storageRemoveItem(key) {
    return new Promise(function (resolve, reject) {
        storage.removeItem(key, function (event) {
            // console.log(event);
            resolve(event);
        });
    });
}

/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/**
 * Created by vincent on 2017/8/27.
 * 使用杜威的十进制来进行数据分析和处理
 */

// let googleTrack = {

//     USER_BEHAVIOR:'userBehavior',

//     USER_BEHAVIOR_HOME:'home',                      // 主页
//     USER_BEHAVIOR_ALBUM:'album',
//     USER_BEHAVIOR_ARTICLE:'albumArticleDetail',                    // 文章点击

//     USER_BEHAVIOR_LIBRARY:'library',                    // 图书馆
//     USER_BEHAVIOR_LIBRARY_BOOKSHELF:'libraryBookshelf',        // 图书馆-书架
//     USER_BEHAVIOR_LIBRARY_CATEGORY:'libraryCategory',          // 图书馆-分类
//     USER_BEHAVIOR_LIBRARY_BOOK_INTRODUCTION:'libraryBookIntroduction',          // 图书馆-书籍介绍
//     USER_BEHAVIOR_LIBRARY_BOOK_INTRODUCTION_ADD:'libraryBookAddToBookshelf',          // 图书馆-分类
//     USER_BEHAVIOR_LIBRARY_BOOKLIST:'libraryBookList',         // 图书馆-书单
//     USER_BEHAVIOR_LIBRARY_BOOKSHOP:'store',              // 图书馆-书店
//     USER_BEHAVIOR_LIBRARY_BOOKSHOP_MORE:'storeCategory',      // 图书馆-书店
//     USER_BEHAVIOR_LIBRARY_BOOKSHOP_DETAIL:'storeBookDetail',    // 图书馆-书店-书的详情介绍
//     USER_BEHAVIOR_LIBRARY_BOOKSHOP_BUY:'storeClickBuy',    // 图书馆-书店-去购买
//     USER_BEHAVIOR_LIBRARY_SEARCH:'librarySearch',           // 图书馆-搜索

//     USER_BEHAVIOR_FOLLOW:'follow',                                    // 订阅
//     USER_BEHAVIOR_CHAT:'chat',                                        // 答疑
//     USER_BEHAVIOR_ME:'user',                                            // 我
//     USER_BEHAVIOR_USER:'userLoginOrReg',                                  // 用户注册和登录
//     USER_BEHAVIOR_USER_REGBYMAIL:'userRegByMail',                       // 用户邮件注册
//     USER_BEHAVIOR_USER_REGBYPHONE:'userRegByPhone' ,                     // 用户手机注册

//     USER_BEHAVIOR_USER_OTHER:'user',                                  // 其他信息
//     USER_BEHAVIOR_USER_OTHER_SUBSCRIPTION:'userOpenSubscription',         // 订阅
//     USER_BEHAVIOR_USER_OTHER_FEEDBACK:'userClickFeedback',         // 其他信息
//     USER_BEHAVIOR_USER_OTHER_ARTICLE:'userOpenCollectionArtilce',           // 收藏
//     USER_BEHAVIOR_USER_OTHER_FRIDEND:'userShareToFriend',            // 分享给朋友

//     //用户点击事件跟踪
//     userAction(action,label,id) {
//         let event =  weex.requireModule('event');       
//         if(typeof(event.googleTrack)!='undefined'){
//             event.googleTrack(this.USER_BEHAVIOR,action,label,id);
//         }
//         console.log(action+" -- "+label+" -- "+id);
//     },
//     userHome() {
//         this.userAction(this.USER_BEHAVIOR_HOME,"",0);
//     },
//     userLibrary() {
//         this.userAction(this.USER_BEHAVIOR_LIBRARY_BOOKSHELF,"",0);
//     },
//     //订阅
//     userFollow() {
//         this.userAction(this.USER_BEHAVIOR_FOLLOW,"",0);
//     },
//     //用户点击专栏
//     userOpenAlbum(albumName,albumId) {
//         this.userAction(this.USER_BEHAVIOR_ALBUM,albumName,albumId);
//     },
//     //用户点击文章次数
//     userOpenArticle(title,id) {
//         this.userAction(this.USER_BEHAVIOR_ARTICLE,title,id);
//     },
//     //点击阅读书
//     userLibraryOpenBook(title) { 
//         this.userAction(this.USER_BEHAVIOR_LIBRARY_BOOKSHELF,title,0);
//     },
//     //点击书籍分类
//     userLibraryOpenCategory(categoryName) { 
//         this.userAction(this.USER_BEHAVIOR_LIBRARY_CATEGORY,categoryName,0);
//     },
//     //点击书籍分类
//     userOpenBookIntroduction(bookName,bookId) { 
//         this.userAction(this.USER_BEHAVIOR_LIBRARY_BOOK_INTRODUCTION,bookName,bookId);
//     },
//     //点击书籍分类
//     userOpenBookIntroductionAdd(bookName,bookId) { 
//         this.userAction(this.USER_BEHAVIOR_LIBRARY_BOOK_INTRODUCTION_ADD,bookName,bookId);
//     },
//     //点击书单
//     userLibraryOpenBookList() { 
//         this.userAction(this.USER_BEHAVIOR_LIBRARY_BOOKLIST,'',0);
//     },
//     //点击书单
//     userLibraryOpenBookListByName(name) { 
//         this.userAction(this.USER_BEHAVIOR_LIBRARY_BOOKLIST,name,0);
//     },
//     //点击书店导航
//     userLibraryOpenBookshop() { 
//         this.userAction(this.USER_BEHAVIOR_LIBRARY_BOOKSHOP,"",0);
//     },
//      //点击书店中的更多
//      userLibraryOpenBookshopMore(categoryName) { 
//         this.userAction(this.USER_BEHAVIOR_LIBRARY_BOOKSHOP_MORE,categoryName,0);
//     },
//     //点击书店中某本书
//     userLibraryOpenBookshopDetails(bookName) { 
//         this.userAction(this.USER_BEHAVIOR_LIBRARY_BOOKSHOP_DETAIL,bookName,0);
//     },
//     //点击书店中去购买
//     userLibraryOpenBookshopBuy(bookName) { 
//         this.userAction(this.USER_BEHAVIOR_LIBRARY_BOOKSHOP_BUY,bookName,0);
//     },
//     //点击搜索
//     userSearch(keywords) {
//         this.userAction(this.USER_BEHAVIOR_LIBRARY_SEARCH,keywords,0);
//     },
//     //点击答疑
//     userChat() {
//         this.userAction(this.USER_BEHAVIOR_CHAT,"",0);
//     },
//     //点击Me
//     userMe() {
//         this.userAction(this.USER_BEHAVIOR_ME,"",0);
//     },
//     //用户点击登录或注册按钮
//     userLoginOrReg() {
//         this.userAction(this.USER_BEHAVIOR_USER,"",0);
//     },
//     //用户用邮件注册
//     userRegByMail() {
//         this.userAction(this.USER_BEHAVIOR_USER,this.USER_BEHAVIOR_USER_REGBYMAIL,0);
//     },
//     //用户用手机注册
//     userRegByPhone() {
//         this.userAction(this.USER_BEHAVIOR_USER,this.USER_BEHAVIOR_USER_REGBYPHONE,0);
//     },
//     //订阅
//     userOtherSubscription() {
//         this.userAction(this.USER_BEHAVIOR_USER_OTHER,this.USER_BEHAVIOR_USER_OTHER_SUBSCRIPTION,0);
//     },
//     //收藏
//     userOtherArticle() {
//         this.userAction(this.USER_BEHAVIOR_USER_OTHER,this.USER_BEHAVIOR_USER_OTHER_ARTICLE,0);
//     },
//     //其他
//     userOtherFeedback() {
//         this.userAction(this.USER_BEHAVIOR_USER_OTHER,this.USER_BEHAVIOR_USER_OTHER_FEEDBACK,0);
//     },
//     userOtherShareToFriend() {
//         this.userAction(this.USER_BEHAVIOR_USER_OTHER,this.USER_BEHAVIOR_USER_OTHER_FRIDEND,0);
//     },
// };

// export default googleTrack;


/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _typeof2 = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//import UrlParser from 'url-parse';

var Utils = {
  //UrlParser: UrlParser,
  _typeof: function _typeof(obj) {
    return Object.prototype.toString.call(obj).slice(8, -1).toLowerCase();
  },
  isPlainObject: function isPlainObject(obj) {
    return Utils._typeof(obj) === 'object';
  },
  isString: function isString(obj) {
    return typeof obj === 'string';
  },
  isNonEmptyArray: function isNonEmptyArray() {
    var obj = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];

    return obj && obj.length > 0 && Array.isArray(obj) && typeof obj !== 'undefined';
  },
  isObject: function isObject(item) {
    return item && (typeof item === 'undefined' ? 'undefined' : _typeof2(item)) === 'object' && !Array.isArray(item);
  },
  isEmptyObject: function isEmptyObject(obj) {
    return Object.keys(obj).length === 0 && obj.constructor === Object;
  },
  decodeIconFont: function decodeIconFont(text) {
    // 正则匹配 图标和文字混排 eg: 我去上学校&#xe600;,天天不&#xe600;迟到
    var regExp = /&#x[a-z]\d{3,4};?/;
    if (regExp.test(text)) {
      return text.replace(new RegExp(regExp, 'g'), function (iconText) {
        var replace = iconText.replace(/&#x/, '0x').replace(/;$/, '');
        return String.fromCharCode(replace);
      });
    } else {
      return text;
    }
  },
  mergeDeep: function mergeDeep(target) {
    for (var _len = arguments.length, sources = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
      sources[_key - 1] = arguments[_key];
    }

    if (!sources.length) return target;
    var source = sources.shift();
    if (Utils.isObject(target) && Utils.isObject(source)) {
      for (var key in source) {
        if (Utils.isObject(source[key])) {
          if (!target[key]) {
            Object.assign(target, _defineProperty({}, key, {}));
          }
          Utils.mergeDeep(target[key], source[key]);
        } else {
          Object.assign(target, _defineProperty({}, key, source[key]));
        }
      }
    }
    return Utils.mergeDeep.apply(Utils, [target].concat(sources));
  },
  appendProtocol: function appendProtocol(url) {
    if (/^\/\//.test(url)) {
      var bundleUrl = weex.config.bundleUrl;

      return 'http' + (/^https:/.test(bundleUrl) ? 's' : '') + ':' + url;
    }
    return url;
  },
  encodeURLParams: function encodeURLParams(url) {
    var parsedUrl = new UrlParser(url, true);
    return parsedUrl.toString();
  },
  goToH5Page: function goToH5Page(jumpUrl) {
    var animated = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
    var callback = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;

    var Navigator = weex.requireModule('navigator');
    var jumpUrlObj = new Utils.UrlParser(jumpUrl, true);
    var url = Utils.appendProtocol(jumpUrlObj.toString());
    Navigator.push({
      url: Utils.encodeURLParams(url),
      animated: animated.toString()
    }, callback);
  },

  env: {
    isTaobao: function isTaobao() {
      var appName = weex.config.env.appName;

      return (/(tb|taobao|淘宝)/i.test(appName)
      );
    },
    isTrip: function isTrip() {
      var appName = weex.config.env.appName;

      return appName === 'LX';
    },
    isBoat: function isBoat() {
      var appName = weex.config.env.appName;

      return appName === 'Boat' || appName === 'BoatPlayground';
    },
    isWeb: function isWeb() {
      var platform = weex.config.env.platform;

      return (typeof window === 'undefined' ? 'undefined' : _typeof2(window)) === 'object' && platform.toLowerCase() === 'web';
    },
    isIOS: function isIOS() {
      var platform = weex.config.env.platform;

      return platform.toLowerCase() === 'ios';
    },

    /**
     * 是否为 iPhone X
     * @returns {boolean}
     */
    isIPhoneX: function isIPhoneX() {
      var deviceHeight = weex.config.env.deviceHeight;

      if (Utils.env.isWeb()) {
        return (typeof window === 'undefined' ? 'undefined' : _typeof2(window)) !== undefined && window.screen && window.screen.width && window.screen.height && parseInt(window.screen.width, 10) === 375 && parseInt(window.screen.height, 10) === 812;
      }
      return Utils.env.isIOS() && deviceHeight === 2436;
    },
    isAndroid: function isAndroid() {
      var platform = weex.config.env.platform;

      return platform.toLowerCase() === 'android';
    },
    isAlipay: function isAlipay() {
      var appName = weex.config.env.appName;

      return appName === 'AP';
    },
    isTmall: function isTmall() {
      var appName = weex.config.env.appName;

      return (/(tm|tmall|天猫)/i.test(appName)
      );
    },
    isAliWeex: function isAliWeex() {
      return Utils.env.isTmall() || Utils.env.isTrip() || Utils.env.isTaobao();
    },
    supportsEB: function supportsEB() {
      var weexVersion = weex.config.env.weexVersion || '0';
      var isHighWeex = Utils.compareVersion(weexVersion, '0.10.1.4') && (Utils.env.isIOS() || Utils.env.isAndroid());
      var expressionBinding = weex.requireModule('expressionBinding');
      return expressionBinding && expressionBinding.enableBinding && isHighWeex;
    },


    /**
     * 判断Android容器是否支持是否支持expressionBinding(处理方式很不一致)
     * @returns {boolean}
     */
    supportsEBForAndroid: function supportsEBForAndroid() {
      return Utils.env.isAndroid() && Utils.env.supportsEB();
    },


    /**
     * 判断IOS容器是否支持是否支持expressionBinding
     * @returns {boolean}
     */
    supportsEBForIos: function supportsEBForIos() {
      return Utils.env.isIOS() && Utils.env.supportsEB();
    },


    /**
     * 获取weex屏幕真实的设置高度，需要减去导航栏高度
     * @returns {Number}
     */
    getPageHeight: function getPageHeight() {
      var env = weex.config.env;

      var navHeight = Utils.env.isWeb() ? 0 : Utils.env.isIPhoneX() ? 176 : 132;
      return env.deviceHeight / env.deviceWidth * 750 - navHeight;
    }
  },

  /**
   * 版本号比较
   * @memberOf Utils
   * @param currVer {string}
   * @param promoteVer {string}
   * @returns {boolean}
   * @example
   *
   * const { Utils } = require('@ali/wx-bridge');
   * const { compareVersion } = Utils;
   * console.log(compareVersion('0.1.100', '0.1.11')); // 'true'
   */
  compareVersion: function compareVersion() {
    var currVer = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '0.0.0';
    var promoteVer = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '0.0.0';

    if (currVer === promoteVer) return true;
    var currVerArr = currVer.split('.');
    var promoteVerArr = promoteVer.split('.');
    var len = Math.max(currVerArr.length, promoteVerArr.length);
    for (var i = 0; i < len; i++) {
      var proVal = ~~promoteVerArr[i];
      var curVal = ~~currVerArr[i];
      if (proVal < curVal) {
        return true;
      } else if (proVal > curVal) {
        return false;
      }
    }
    return false;
  },

  /**
   * 分割数组
   * @param arr 被分割数组
   * @param size 分割数组的长度
   * @returns {Array}
   */
  arrayChunk: function arrayChunk() {
    var arr = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
    var size = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 4;

    var groups = [];
    if (arr && arr.length > 0) {
      groups = arr.map(function (e, i) {
        return i % size === 0 ? arr.slice(i, i + size) : null;
      }).filter(function (e) {
        return e;
      });
    }
    return groups;
  },
  truncateString: function truncateString(str, len) {
    var hasDot = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;

    var newLength = 0;
    var newStr = '';
    var singleChar = '';
    var chineseRegex = /[^\x00-\xff]/g;
    var strLength = str.replace(chineseRegex, '**').length;
    for (var i = 0; i < strLength; i++) {
      singleChar = str.charAt(i).toString();
      if (singleChar.match(chineseRegex) !== null) {
        newLength += 2;
      } else {
        newLength++;
      }
      if (newLength > len) {
        break;
      }
      newStr += singleChar;
    }

    if (hasDot && strLength > len) {
      newStr += '...';
    }
    return newStr;
  }
};

exports.default = Utils;

/***/ }),
/* 12 */
/***/ (function(module, exports) {

var g;

// This works in non-strict mode
g = (function() {
	return this;
})();

try {
	// This works if eval is allowed (see CSP)
	g = g || Function("return this")() || (1,eval)("this");
} catch(e) {
	// This works if the window reference is available
	if(typeof window === "object")
		g = window;
}

// g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}

module.exports = g;


/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(75)
)

/* script */
__vue_exports__ = __webpack_require__(47)

/* template */
var __vue_template__ = __webpack_require__(101)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "/Users/james/Desktop/creader/cread/wx-creader/src/03.components/chat.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-7f4b3501"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__


/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(92)
)

/* script */
__vue_exports__ = __webpack_require__(48)

/* template */
var __vue_template__ = __webpack_require__(118)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "/Users/james/Desktop/creader/cread/wx-creader/src/03.components/commonFun.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-1e38cfbb"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__


/***/ }),
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(88)
)

/* script */
__vue_exports__ = __webpack_require__(49)

/* template */
var __vue_template__ = __webpack_require__(114)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "/Users/james/Desktop/creader/cread/wx-creader/src/03.components/crIconFont.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-54d45360"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__


/***/ }),
/* 16 */
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(77)
)

/* script */
__vue_exports__ = __webpack_require__(50)

/* template */
var __vue_template__ = __webpack_require__(103)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "/Users/james/Desktop/creader/cread/wx-creader/src/03.components/defaultImage.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-6e3bb343"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__


/***/ }),
/* 17 */
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(76)
)

/* script */
__vue_exports__ = __webpack_require__(51)

/* template */
var __vue_template__ = __webpack_require__(102)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "/Users/james/Desktop/creader/cread/wx-creader/src/03.components/defaultLoading.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-7d951ea4"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__


/***/ }),
/* 18 */
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(93)
)

/* script */
__vue_exports__ = __webpack_require__(52)

/* template */
var __vue_template__ = __webpack_require__(119)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "/Users/james/Desktop/creader/cread/wx-creader/src/03.components/followAlbum.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-da97c6b6"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__


/***/ }),
/* 19 */
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(82)
)

/* script */
__vue_exports__ = __webpack_require__(53)

/* template */
var __vue_template__ = __webpack_require__(108)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "/Users/james/Desktop/creader/cread/wx-creader/src/03.components/libraryBook.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-5f1f2eab"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__


/***/ }),
/* 20 */
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(96)
)

/* script */
__vue_exports__ = __webpack_require__(54)

/* template */
var __vue_template__ = __webpack_require__(122)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "/Users/james/Desktop/creader/cread/wx-creader/src/03.components/libraryBookLists.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-a271094c"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__


/***/ }),
/* 21 */
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(91)
)

/* script */
__vue_exports__ = __webpack_require__(55)

/* template */
var __vue_template__ = __webpack_require__(117)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "/Users/james/Desktop/creader/cread/wx-creader/src/03.components/libraryBookShelf.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-61d0236f"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__


/***/ }),
/* 22 */
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(79)
)

/* script */
__vue_exports__ = __webpack_require__(56)

/* template */
var __vue_template__ = __webpack_require__(105)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "/Users/james/Desktop/creader/cread/wx-creader/src/03.components/libraryCategory.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-232fb940"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__


/***/ }),
/* 23 */
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(97)
)

/* script */
__vue_exports__ = __webpack_require__(57)

/* template */
var __vue_template__ = __webpack_require__(123)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "/Users/james/Desktop/creader/cread/wx-creader/src/03.components/libraryNav.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-1bedd751"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__


/***/ }),
/* 24 */
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(73)
)

/* script */
__vue_exports__ = __webpack_require__(58)

/* template */
var __vue_template__ = __webpack_require__(99)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "/Users/james/Desktop/creader/cread/wx-creader/src/03.components/libraryStore.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-73902a8f"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__


/***/ }),
/* 25 */
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(98)
)

/* script */
__vue_exports__ = __webpack_require__(59)

/* template */
var __vue_template__ = __webpack_require__(124)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "/Users/james/Desktop/creader/cread/wx-creader/src/03.components/loadingCircle.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-0ad11c5a"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__


/***/ }),
/* 26 */
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(95)
)

/* script */
__vue_exports__ = __webpack_require__(60)

/* template */
var __vue_template__ = __webpack_require__(121)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "/Users/james/Desktop/creader/cread/wx-creader/src/03.components/netError.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-0655a214"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__


/***/ }),
/* 27 */
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(86)
)

/* script */
__vue_exports__ = __webpack_require__(61)

/* template */
var __vue_template__ = __webpack_require__(112)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "/Users/james/Desktop/creader/cread/wx-creader/src/03.components/networkErrorDisplay.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-00a24362"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__


/***/ }),
/* 28 */
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(89)
)

/* script */
__vue_exports__ = __webpack_require__(62)

/* template */
var __vue_template__ = __webpack_require__(115)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "/Users/james/Desktop/creader/cread/wx-creader/src/03.components/progress.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-2fa3fe54"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__


/***/ }),
/* 29 */
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(87)
)

/* script */
__vue_exports__ = __webpack_require__(63)

/* template */
var __vue_template__ = __webpack_require__(113)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "/Users/james/Desktop/creader/cread/wx-creader/src/03.components/searchBar.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-2c31d9f2"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__


/***/ }),
/* 30 */
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(94)
)

/* script */
__vue_exports__ = __webpack_require__(64)

/* template */
var __vue_template__ = __webpack_require__(120)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "/Users/james/Desktop/creader/cread/wx-creader/src/03.components/submitting.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-0884de22"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__


/***/ }),
/* 31 */
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(81)
)

/* script */
__vue_exports__ = __webpack_require__(65)

/* template */
var __vue_template__ = __webpack_require__(107)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "/Users/james/Desktop/creader/cread/wx-creader/src/03.components/topNavigationWidget.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-e614bc58"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__


/***/ }),
/* 32 */
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(90)
)

/* script */
__vue_exports__ = __webpack_require__(66)

/* template */
var __vue_template__ = __webpack_require__(116)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "/Users/james/Desktop/creader/cread/wx-creader/src/03.components/topNavigationWidgetWithBack.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-de01bb7e"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__


/***/ }),
/* 33 */
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(83)
)

/* script */
__vue_exports__ = __webpack_require__(67)

/* template */
var __vue_template__ = __webpack_require__(109)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "/Users/james/Desktop/creader/cread/wx-creader/src/03.components/topbar.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-567c65f2"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__


/***/ }),
/* 34 */
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(74)
)

/* script */
__vue_exports__ = __webpack_require__(68)

/* template */
var __vue_template__ = __webpack_require__(100)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "/Users/james/Desktop/creader/cread/wx-creader/src/03.components/videoPlay.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-51b638f6"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__


/***/ }),
/* 35 */
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(84)
)

/* script */
__vue_exports__ = __webpack_require__(69)

/* template */
var __vue_template__ = __webpack_require__(110)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "/Users/james/Desktop/creader/cread/wx-creader/src/03.components/weexDialogue.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-6d88d5bc"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__


/***/ }),
/* 36 */
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(78)
)

/* script */
__vue_exports__ = __webpack_require__(70)

/* template */
var __vue_template__ = __webpack_require__(104)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "/Users/james/Desktop/creader/cread/wx-creader/src/03.components/weexOverlay.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-35e83bf6"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__


/***/ }),
/* 37 */
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(85)
)

/* script */
__vue_exports__ = __webpack_require__(71)

/* template */
var __vue_template__ = __webpack_require__(111)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "/Users/james/Desktop/creader/cread/wx-creader/src/03.components/weexWebView.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-1a78c982"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__


/***/ }),
/* 38 */
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(80)
)

/* script */
__vue_exports__ = __webpack_require__(72)

/* template */
var __vue_template__ = __webpack_require__(106)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "/Users/james/Desktop/creader/cread/wx-creader/src/03.components/weextabbar.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-7f686df0"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__


/***/ }),
/* 39 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
/**
 * Created by zwwill on 2017/8/27.
 */

var utilFunc = {
    initIconFont: function initIconFont() {
        var domModule = weex.requireModule('dom');
        domModule.addRule('fontFace', {
            'fontFamily': "iconfont",
            'src': "url('http://at.alicdn.com/t/font_404010_jgmnakd1zizr529.ttf')"
        });
    },
    setWXBundleUrl: function setWXBundleUrl(url, jsFile) {
        var bundleUrl = url;
        var host = '';
        var path = '';
        var nativeBase = void 0;
        var isAndroidAssets = bundleUrl.indexOf('your_current_IP') >= 0 || bundleUrl.indexOf('file://assets/') >= 0;
        var isiOSAssets = bundleUrl.indexOf('file:///') >= 0 && bundleUrl.indexOf('WeexDemo.app') > 0;
        if (isAndroidAssets) {
            nativeBase = 'file://assets/dist';
        } else if (isiOSAssets) {
            // file:///var/mobile/Containers/Bundle/Application/{id}/WeexDemo.app/
            // file:///Users/{user}/Library/Developer/CoreSimulator/Devices/{id}/data/Containers/Bundle/Application/{id}/WeexDemo.app/
            nativeBase = bundleUrl.substring(0, bundleUrl.lastIndexOf('/') + 1);
        } else {
            var matches = /\/\/([^\/]+?)\//.exec(bundleUrl);
            var matchFirstPath = /\/\/[^\/]+\/([^\/]+)\//.exec(bundleUrl);
            if (matches && matches.length >= 2) {
                host = matches[1];
            }
            if (matchFirstPath && matchFirstPath.length >= 2) {
                path = matchFirstPath[1];
            }
            nativeBase = 'http://' + host + '/';
        }
        var h5Base = './index.html?page=';
        // in Native
        var base = nativeBase;
        if (typeof navigator !== 'undefined' && (navigator.appCodeName === 'Mozilla' || navigator.product === 'Gecko')) {
            // check if in weexpack project
            if (path === 'web' || path === 'dist') {
                base = h5Base + '/dist/';
            } else {
                base = h5Base + '';
            }
        } else {
            base = nativeBase + (!!path ? path + '/' : '');
        }

        var newUrl = base + jsFile;
        return newUrl;
    },
    setWebBundleUrl: function setWebBundleUrl(url, jsFile) {
        var bundleUrl = url;
        var host = '';
        var path = '';
        var nativeBase = void 0;
        var isAndroidAssets = bundleUrl.indexOf('your_current_IP') >= 0 || bundleUrl.indexOf('file://assets/') >= 0;
        var isiOSAssets = bundleUrl.indexOf('file:///') >= 0 && bundleUrl.indexOf('WeexDemo.app') > 0;
        if (isAndroidAssets) {
            nativeBase = 'file://assets/dist';
        } else if (isiOSAssets) {
            // file:///var/mobile/Containers/Bundle/Application/{id}/WeexDemo.app/
            // file:///Users/{user}/Library/Developer/CoreSimulator/Devices/{id}/data/Containers/Bundle/Application/{id}/WeexDemo.app/
            nativeBase = bundleUrl.substring(0, bundleUrl.lastIndexOf('/') + 1);
        } else {
            var matches = /\/\/([^\/]+?)\//.exec(bundleUrl);
            var matchFirstPath = /\/\/[^\/]+\/([^\/]+)\//.exec(bundleUrl);
            if (matches && matches.length >= 2) {
                host = matches[1];
            }
            if (matchFirstPath && matchFirstPath.length >= 2) {
                path = matchFirstPath[1];
            }
            nativeBase = 'http://' + host + '/';
        }
        var h5Base = './index.html?page=';
        // in Native
        var base = nativeBase;
        if (typeof navigator !== 'undefined' && (navigator.appCodeName === 'Mozilla' || navigator.product === 'Gecko')) {
            // check if in weexpack project
            if (path === 'web' || path === 'dist') {
                base = h5Base + '/web/';
            } else {
                base = h5Base + '';
            }
        } else {
            base = nativeBase + (!!path ? path + '/' : '');
        }

        var newUrl = base + jsFile;
        return newUrl;
    },
    getUrlSearch: function getUrlSearch(url, name) {
        // debugger;
        var address,
            val = "";
        if (WXEnvironment.platform == 'Web') {
            address = window.location.href;
        } else {
            address = url;
        }

        var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
        var r = address.slice(address.indexOf('?') + 1).match(reg);

        if (r != null) {
            try {
                val = decodeURIComponent(r[2]);
            } catch (_e) {}
        }
        return val;
    }
};

exports.default = utilFunc;

/***/ }),
/* 40 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var sync = new BroadcastChannel('book');

var syncBookShelf = {
  receive: function receive(fun) {
    sync.onmessage = fun;
  },
  addToShelf: function addToShelf(bookId) {
    var message = {
      status: 'add',
      bookId: bookId
    };
    sync.postMessage(message);
  },
  cancel: function cancel() {
    var message = {
      status: 'cancel'
    };
    sync.postMessage(message);
  },
  getData: function getData() {
    var message = {
      status: 'getData'
    };
    sync.postMessage(message);
  },
  close: function close(ablumnId, fun) {
    sync.close();
  }
};

exports.default = syncBookShelf;

/***/ }),
/* 41 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
/**
 * Created by zwwill on 2017/8/27.
 */

var EVENT_DOWNLOAD = 'downLoadBook';

var downloadEvent = {
    addDownLoadBookEventListener: function addDownLoadBookEventListener(fun) {
        var event = weex.requireModule('globalEvent');
        event.addEventListener(EVENT_DOWNLOAD, function (value) {
            console.log("get downLoadBook");
            fun(value);
        });
    },
    removeDownLoadBookEventListener: function removeDownLoadBookEventListener() {
        var event = weex.requireModule('globalEvent');
        console.log("remove downLoadBook");
        event.removeEventListener(EVENT_DOWNLOAD);
    }
};

exports.default = downloadEvent;

/***/ }),
/* 42 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.getBookList = getBookList;
exports.getBookListByTag = getBookListByTag;
exports.getBookProfile = getBookProfile;

var _fetch = __webpack_require__(6);

var _user = __webpack_require__(5);

var _storage = __webpack_require__(9);

var _global = __webpack_require__(3);

var _global2 = _interopRequireDefault(_global);

var _syncAblumInfo = __webpack_require__(8);

var _syncAblumInfo2 = _interopRequireDefault(_syncAblumInfo);

var _prompt = __webpack_require__(2);

var _prompt2 = _interopRequireDefault(_prompt);

var _navigator = __webpack_require__(0);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// import googleTrack from '../02.bus/googleTrack';

//获得商店新
function getBookList() {
    return (0, _user.getOpenId)().then(function (openId) {
        return (0, _fetch.get)('/api/shop/getBooksInShop', { 'openId': openId });
    });
    // return get('/api/shop/getBooksInShop', { 'columnId': columnId, 'openId': openId , 'language':global.getLanguage()}, fun);
}

//根据书籍的标签获得信息
function getBookListByTag(tagid) {
    return (0, _user.getOpenId)().then(function (openId) {
        return (0, _fetch.get)('/api/shop/getBooksInShopByTag', { 'openId': openId, 'tag_id': tagid });
    });
    // return get('/api/shop/getBooksInShop', { 'columnId': columnId, 'openId': openId , 'language':global.getLanguage()}, fun);
}

//获取书籍信息
function getBookProfile(id) {
    return (0, _user.getOpenId)().then(function (openId) {
        return (0, _fetch.get)('/api/shop/getBookProfile', { 'openId': openId, 'id': id });
    });
}

/***/ }),
/* 43 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
/**
 * Created by zwwill on 2017/8/27.
 */

var setting = {
    getPlatform: function getPlatform() {
        return weex.config.env.platform.toLowerCase();
    },
    isWeb: function isWeb() {
        return this.getPlatform() == "web";
    },
    isAndroid: function isAndroid() {
        return this.getPlatform() == "android";
    },
    isIOS: function isIOS() {
        return this.getPlatform() == "ios";
    }
};

exports.default = setting;

/***/ }),
/* 44 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var lang = {
    "loading": "Loading",
    "networkError": "Unable to get network data, please connect to network and try again",
    "refresh": "Refresh",
    "columnInfoItro": "Album Introduction",
    "suggestion": "Recommended Album",
    "alreadyFollow": "Aready Follow",
    "videoplay": "Video Playing",
    "follow": "follow",
    "videofrom": "VideoFrom",
    "videoList": "Recommended Videos",
    "recommendation": "Excellent Column Recommendation",
    "recommendedvideos": "Excellent Video Recommendation"

};
exports.lang = lang;

/***/ }),
/* 45 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var lang = {
					"loading": "加載中",
					"networkError": "無法獲取網絡數據 請連接網絡後重試",
					"refresh": "刷新",
					"columnInfoItro": "專輯介紹",
					"suggestion": "專欄推薦",
					"alreadyFollow": "已訂閱專欄",
					"videoplay": "視頻播放",
					"follow": "訂閱",
					"videofrom": "視頻來源",
					"videoList": "本專欄視頻推薦",
					"recommendation": "優秀專欄推薦",
					"recommendedvideos": "優秀視頻推薦"
};
exports.lang = lang;

/***/ }),
/* 46 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var lang = {
    "loading": "加载中",
    "networkError": "无法获取网络数据 请连接网络后重试",
    "refresh": "刷新",
    "columnInfoItro": "专辑介绍",
    "suggestion": "专栏推荐",
    "alreadyFollow": "已订阅专栏",
    "videoplay": "视频播放",
    "follow": "订阅",
    "videofrom": "视频来源",
    "videoList": "本专栏视频推荐",
    "recommendation": "优秀专栏推荐",
    "recommendedvideos": "优秀视频推荐"
};
exports.lang = lang;

/***/ }),
/* 47 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _common = __webpack_require__(1);

var _global = __webpack_require__(3);

var _global2 = _interopRequireDefault(_global);

var _Utils = __webpack_require__(11);

var _Utils2 = _interopRequireDefault(_Utils);

var _weextabbarConfig = __webpack_require__(7);

var _weextabbarConfig2 = _interopRequireDefault(_weextabbarConfig);

var _prompt = __webpack_require__(2);

var _prompt2 = _interopRequireDefault(_prompt);

var _navigator = __webpack_require__(0);

var _creader = __webpack_require__(4);

var _user = __webpack_require__(5);

var _user2 = _interopRequireDefault(_user);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
    data: function data() {
        return {};
    },

    created: function created() {
        var deviceHeight = WXEnvironment.deviceHeight || unknown;
        var deviceWidth = WXEnvironment.deviceWidth || unknown;
        this.height = 750 / deviceWidth * deviceHeight - 96;
    },

    methods: {
        tabBarCurrentTabSelected: function tabBarCurrentTabSelected(e) {
            var page = e.page;
            console.log(page);
            (0, _navigator.jumpSubPage)(this, page, { closeThisPage: true });
        },
        goToPersonalProfile: function goToPersonalProfile() {
            if (this.personInfo.id > 0) {
                (0, _navigator.jumpSubPage)(this, 'userPersonalProfile');
            } else {
                (0, _navigator.jumpSubPage)(this, 'userLogin');
            }
        },
        goToMySubscribe: function goToMySubscribe() {
            (0, _navigator.jumpSubPage)(this, 'mySubscribe');
        },
        goToMyLists: function goToMyLists() {
            (0, _navigator.jumpSubPage)(this, 'myArticles');
        },
        goToFeedback: function goToFeedback() {
            (0, _navigator.jumpSubPage)(this, 'UserFeedback');
        },
        refresh: function refresh() {
            var self = this;
            _global2.default.init();
            self.netStatus = 0;
            self.getData();
        },
        getData: function getData() {
            var _this = this;

            var self = this;
            (0, _user.getUserInfo)().then(function (res) {
                console.log(" --- getUserInfo --- ");
                self.personInfo = res;
                //                        console.log(self.personInfo.nickName);
                self.netStatus = 2;
                (0, _creader.getPersonalData)().then(function (res) {
                    var self = _this;
                    console.log(res);
                    if (res.status != -1 && typeof res.data != "undefined") {
                        self.netStatus = 2;
                        self.personInfo = res.data.data;
                        console.log(self.personInfo);
                    } else {
                        self.netStatus = 1;
                        self.errorType = 'neterror';
                    }
                });
            });
        }
    }

};

/***/ }),
/* 48 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
//
//
//
//

exports.default = {};

/***/ }),
/* 49 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _common = __webpack_require__(1);

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

var iconItems = __webpack_require__(125);

var fontFamily = "ionfont";
var domModule = weex.requireModule("dom");
module.exports = {
    data: function data() {
        return {
            iconValue: ''
        };
    },
    beforeCreate: function beforeCreate() {
        // let url = weex.config.bundleUrl;
        // if(url.indexOf('?')>0){
        //     url = url.substring(0,url.indexOf('?'));
        // }
        // url = url.split('/').slice(0, -1).join('/');
        // console.log(url+"qingsongtesticonfont0000000");
        var partUrl = (0, _common.getIconFontPath)();
        var url = partUrl + "ionicons.ttf";
        // console.log(url+"qingsongtesticonfont1111111");
        domModule.addRule('fontFace', {
            'fontFamily': fontFamily,
            'src': "url('" + url + "')"
        });
    },
    created: function created() {
        this.iconValue = iconItems[this.name];
    },

    props: {
        name: {
            type: String,
            defalut: ''
        },
        color: {
            type: String,
            default: 'white'
        },
        size: {
            type: [Number, String],
            default: '70px'
        },
        activeColor: {
            type: String
        }
    },
    computed: {
        getFontName: function getFontName() {
            var icon = iconItems[this.name];
            console.log(icon + "qingsongtesticonfont");
            return this.decode(icon || '');
        },
        getStyle: function getStyle() {
            var style = {
                'color': this.color,
                'font-size': this.size,
                'font-family': fontFamily
            };
            if (this.activeColor) {
                style["color:active"] = this.activeColor;
            }
            return style;
        }
    },
    methods: {
        _click: function _click(e) {
            this.$emit("click", e);
        },
        decode: function decode(fontCode) {
            if (/^&#x/.test(fontCode)) {
                return String.fromCharCode(fontCode.replace(/^&#x/, '0x').replace(/;$/, ''));
            } else {
                return String.fromCharCode('0x' + fontCode);
            }
        }
    }
};

/***/ }),
/* 50 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
    props: {
        backgroup: { type: String },
        source: { type: String }

    },
    data: function data() {
        return {
            loadFinished: false
        };
    },
    created: function created() {
        console.log(' --- ');
    },

    methods: {
        onImageLoad: function onImageLoad(event) {
            if (event.success) {
                // Do something to hanlde success
                this.loadFinished = true;
            }
        }
    }
};

/***/ }),
/* 51 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
//
//
//
//
//
//
//
//
//


exports.default = {
    name: 'defaultLoading',
    data: function data() {
        return {
            msg: 'hello vue'
        };
    }
};

/***/ }),
/* 52 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _common = __webpack_require__(1);

var _navigator = __webpack_require__(0);

var _global = __webpack_require__(3);

var _global2 = _interopRequireDefault(_global);

var _user = __webpack_require__(5);

var _prompt = __webpack_require__(2);

var _prompt2 = _interopRequireDefault(_prompt);

var _creader = __webpack_require__(4);

var _syncAblumInfo = __webpack_require__(8);

var _syncAblumInfo2 = _interopRequireDefault(_syncAblumInfo);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
    data: function data() {
        return {
            global: _global2.default,
            getImgPath: _common.getImgPath,
            app: 'ion-android-apps',
            search: 'ion-search'
        };
    },

    computed: {
        Id: function Id() {
            var self = this;
            return self.albums.length;
        }
    },
    props: {
        albums: {
            type: Array,
            // default: function () {
            //           return []
            // },
            required: true
        },
        name: {
            type: String,
            default: ''
        },
        height: {
            type: String,
            default: '100px'
        },
        textStyle: String,
        textColor: {
            type: String,
            default: 'white'
        },
        bgcolor: {
            type: String,
            default: '#E15D53'
        }
    },
    created: function created() {
        var self = this;
        // syncAblumInfo.receive(res=>{
        //     if(res.data.status == 'getData'){
        //         self.getData();
        //     }else{
        //         for(let i = 0;i<self.albums.length;i++){
        //             if(res.data.id == self.albums[i].id && res.data.status == 'add'){
        //                 self.albums[i].status = 1;
        //                 self.albums[i].follow_count = self.albums[i].follow_count + 1;
        //             }
        //             if(res.data.id == self.albums[i].id && res.data.status == 'cancel'){
        //                 self.albums[i].status = 0;
        //                 self.albums[i].follow_count = self.albums[i].follow_count - 1;
        //             }
        //         }  
        //     }                                      
        // })
    },
    mounted: function mounted() {},

    methods: {
        goBack: function goBack() {
            (0, _navigator.back)(this, "");
        },
        toggleSubscribe: function toggleSubscribe(albumId, index) {
            var _this = this;

            var self = this;
            (0, _user.getOpenId)().then(function (openId) {
                if (openId) {
                    var _loop = function _loop(i) {
                        if (i == index) {
                            if (self.albums[i].status == 0) {
                                (0, _creader.userFollow)(albumId).then(function (res) {
                                    _prompt2.default.toast("订阅成功");
                                    if (res.data.status == 1) {
                                        self.albums[i].status = 1;
                                        self.albums[i].follow_count = self.albums[i].follow_count + 1;
                                        _syncAblumInfo2.default.follow(albumId);
                                    }
                                });
                            } else {
                                (0, _creader.userRemoveColumn)(albumId).then(function (res) {
                                    _prompt2.default.toast("取消成功");
                                    if (res.data.status == 1) {
                                        self.albums[i].status = 0;
                                        self.albums[i].follow_count = self.albums[i].follow_count - 1;
                                        _syncAblumInfo2.default.cancel(albumId);
                                    }
                                });
                            }
                        }
                    };

                    for (var i = 0; i < self.albums.length; i++) {
                        _loop(i);
                    }
                } else {
                    _prompt2.default.toast("请登录");
                    setTimeout(function () {
                        (0, _navigator.jumpSubPage)(_this, 'userLogin');
                    }, 2000);
                }
            });
        },
        jumpToAuthorColumn: function jumpToAuthorColumn(albumId, status) {
            (0, _navigator.jumpSubPage)(this, 'articleAlbumInfo', { id: albumId, status: status });
        }
    }
};

/***/ }),
/* 53 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _common = __webpack_require__(1);

var _navigator = __webpack_require__(0);

var _global = __webpack_require__(3);

var _global2 = _interopRequireDefault(_global);

var _user = __webpack_require__(5);

var _prompt = __webpack_require__(2);

var _prompt2 = _interopRequireDefault(_prompt);

var _creader = __webpack_require__(4);

var _storage = __webpack_require__(9);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; } //
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
  data: function data() {
    var _ref;

    return _ref = {
      global: _global2.default,
      getImgPath: _common.getImgPath,
      albumLists: [],
      tabItems: [{ title: "书店", index: 0 }, { title: "分类", index: 1 }, { title: "书单", index: 2 }, { title: "书架", index: 3 }],
      left: 0,
      artcileResult: [],
      albumResult: [],
      bookResult: [],
      currentTab: -1,
      pageIndex: 1,
      errorType: '',
      errorMsg: '',
      netStatus: -1
    }, _defineProperty(_ref, 'errorType', ''), _defineProperty(_ref, 'height', ''), _ref;
  },

  computed: {
    Id: function Id() {
      var self = this;
      return self.albums.length;
    }
  },
  props: {},
  mounted: function mounted() {},
  created: function created() {},

  methods: {
    goBack: function goBack() {
      (0, _navigator.back)(this, "");
    },
    jumpToBookShelf: function jumpToBookShelf() {
      (0, _navigator.jumpSubPage)(this, "libraryBookShelf");
    },
    jumpToBookSearch: function jumpToBookSearch() {
      (0, _navigator.jumpSubPage)(this, "librarySearch");
    },
    loadData: function loadData() {
      console.log("--------------loadData ----------------");
      if (this.currentTab < 0) {
        if ((0, _common.checkNetStatusOnOrOff)() == 1) {
          this.currentTab = 1;
        } else {
          this.currentTab = 3;
        }
      }
      if (this.currentTab == 0) {
        this.left = 0;
      } else if (this.currentTab == 1) {
        this.left = 187.5;
      } else if (this.currentTab == 2) {
        this.left = 375;
      } else {
        this.left = 562.5;
      }
    },
    changeTab: function changeTab(index) {
      this.currentTab = index;
      if (this.currentTab == 0) {
        this.left = 0;
      } else if (this.currentTab == 1) {
        this.left = 187.5;
      } else if (this.currentTab == 2) {
        this.left = 375;
      } else {
        this.left = 562.5;
      }
    },
    wxcSearchbarInputOnFocus: function wxcSearchbarInputOnFocus() {
      // modal.toast({ 'message': 'onfocus', 'duration': 1 });
    },
    wxcSearchbarInputOnBlur: function wxcSearchbarInputOnBlur() {
      // modal.toast({ 'message': 'onbulr', 'duration': 1 });
    },
    wxcSearchbarCloseClicked: function wxcSearchbarCloseClicked() {
      // modal.toast({ 'message': 'close.click', 'duration': 1 });
    },
    wxcSearchbarInputOnInput: function wxcSearchbarInputOnInput(e) {
      // this.value = e.value;
    },
    wxcSearchbarCancelClicked: function wxcSearchbarCancelClicked(e) {
      var self = this;
      self.netStatus = 0;
      self.artcileResult = [];
      self.albumResult = [];
      self.bookResult = [];
      var type = 0;
      if (this.currentTab == 0) {
        type = 2;
        (0, _creader.searchResult)(e.value, type, this.pageIndex).then(function (res) {
          console.log(res);
          if (res.status != -1 && typeof res.data != "undefined") {
            self.netStatus = 2;
            if (res.data.data.length > 0) {
              self.artcileResult = res.data.data;
            } else {
              self.netStatus = 1;
              self.errorType = 'noResult';
              self.errorMsg = '查无结果';
            }
          } else {
            self.netStatus = 1;
            self.errorType = 'noResult';
            self.errorMsg = '请检查网络';
          }
        });
      } else if (this.currentTab == 1) {
        type = 3;
        (0, _creader.searchResult)(e.value, type, this.pageIndex).then(function (res) {

          console.log(res);
          if (res.status != -1 && typeof res.data != "undefined") {
            self.netStatus = 2;
            if (res.data.data.length > 0) {
              self.albumResult = res.data.data;
            } else {
              self.netStatus = 1;
              self.errorType = 'noResult';
              self.errorMsg = '查无结果';
            }
          } else {
            self.netStatus = 1;
            self.errorType = 'noResult';
            self.errorMsg = '请检查网络';
          }
        });
      } else {
        type = 1;
        (0, _creader.searchResult)(e.value, type, this.pageIndex).then(function (res) {
          console.log(res);
          self.netStatus = 2;
          // if(res.status != -1 && typeof(res.data)!="undefined") {
          //       self.netStatus = 2;
          //       self.albumResult = res.data.data;
          //   }else{
          //       self.netStatus = 1;
          //       self.errorType = 'noResult';
          // }
        });
      }
    }
  }
};

/***/ }),
/* 54 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _common = __webpack_require__(1);

var _global = __webpack_require__(3);

var _global2 = _interopRequireDefault(_global);

var _Utils = __webpack_require__(11);

var _Utils2 = _interopRequireDefault(_Utils);

var _weextabbarConfig = __webpack_require__(7);

var _weextabbarConfig2 = _interopRequireDefault(_weextabbarConfig);

var _prompt = __webpack_require__(2);

var _prompt2 = _interopRequireDefault(_prompt);

var _navigator = __webpack_require__(0);

var _creader = __webpack_require__(4);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            currentPosition: 2,
            tabTitles: _weextabbarConfig2.default.tabTitles,
            tabStyles: _weextabbarConfig2.default.tabStyles,
            getImgPath: _common.getImgPath,
            bookLists: [],
            storagediv: [],
            networkStatus: 1,
            global: _global2.default,
            errorType: '',
            errorMsg: '',
            netStatus: 0,
            height: ''
        };
    },

    created: function created() {
        this.getData();
    },
    methods: {
        getData: function getData() {
            var self = this;
            (0, _creader.getSuggestBookLists)().then(function (res) {
                console.log(" -- getSuggestBookLists -- ");
                console.log(res);
                if (res.status != -1 && typeof res.data != "undefined" && res.data != null) {
                    self.netStatus = 2;
                    self.bookLists = res.data.data;
                } else {
                    self.netStatus = 1;
                    self.errorType = 'neterror';
                }
            });
        },
        refresh: function refresh() {
            var self = this;
            _global2.default.init();
            self.netStatus = 0;
            self.getData();
        },
        onchange: function onchange(event) {
            console.log('changed:', event.index);
        },
        bannerJump: function bannerJump(event) {
            console.log("11");
        },

        // libraryNavCurrentTabSelected(e){
        //     const page = e.page;
        //     if(e.page=='libraryBookShelf')
        //     {
        //         back();
        //     }
        //     else if(e.page=='libraryCategory'){
        //         jumpSubPage(this,page,{closeThisPage:true});
        //     }
        //     else if(e.page=='librarySearch'){
        //         jumpSubPage(this,page,{closeThisPage:true});
        //     }else{
        //         jumpSubPage(this,page,{closeThisPage:true})
        //     }

        // },
        goToBookList: function goToBookList(id) {
            (0, _navigator.jumpSubPage)(this, 'libraryBookListDetail', { id: id });
        }
    }

}; //
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/***/ }),
/* 55 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(global) {

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _common = __webpack_require__(1);

var _navigator = __webpack_require__(0);

var _fetch = __webpack_require__(6);

var _weextabbarConfig = __webpack_require__(7);

var _weextabbarConfig2 = _interopRequireDefault(_weextabbarConfig);

var _creader = __webpack_require__(4);

var _prompt = __webpack_require__(2);

var _prompt2 = _interopRequireDefault(_prompt);

var _globalEvent = __webpack_require__(41);

var _globalEvent2 = _interopRequireDefault(_globalEvent);

var _syncBookShelf = __webpack_require__(40);

var _syncBookShelf2 = _interopRequireDefault(_syncBookShelf);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// import googleTrack from '../02.bus/googleTrack';

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
  props: {
    tabbarItemSelected: Number
  },
  data: function data() {
    return {
      currentPage: 1,
      currentPosition: 0,
      tabTitles: _weextabbarConfig2.default.tabTitles,
      tabStyles: _weextabbarConfig2.default.tabStyles,
      getImgPath: _common.getImgPath,
      errorType: "",
      errorMsg: "",
      global: global,
      netStatus: 0,
      isShelf: false,
      columnWidth: "auto",
      height: "",
      title: "!",
      content: "您确定要将本书移出书架吗？",
      confirmText: "确定",
      cancelText: "取消",
      show: false,
      single: false,
      showNoPrompt: false,
      isChecked: false,
      currentBookId: -1,
      currentIndex: -1,
      bookTotal: 0,
      bookIndex: 1,
      books: [],
      isSync: false,
      downLoadIndex: -1,
      downLoadStatus: 0,
      imgDownLoad: false
    };
  },
  created: function created() {
    var self = this;
    self.getData();
    console.log("qingsong1111111");

    _syncBookShelf2.default.receive(function (res) {
      if (res.data.status == "add") {
        console.log("qingsong22222222");
        (0, _creader.getBookShelf)().then(function (data) {
          self.books = [];
          self.books = data;
          self.netStatus = 2;
        });
      }
      if (res.data.status == "getData") {
        console.log("qingsong33333333");
        self.syncData();
      }
    });
    _globalEvent2.default.addDownLoadBookEventListener(function (res) {
      if (res.status == 1) {
        if (self.books[self.downLoadIndex]["progress"].visible == false) {
          self.books[self.downLoadIndex]["progress"].visible = true;
        }
        self.books[self.downLoadIndex]["progress"].value = res.progress;
      } else if (res.status == 2) {
        self.books[self.downLoadIndex]["progress"] = {
          visible: false,
          value: 0
        };
        self.downLoadStatus = 0;
        self.downLoadIndex = 0;
      } else if (res.status == 3) {
        _prompt2.default.toast("下载异常，请稍后再试！");
        self.books[self.downLoadIndex]["progress"] = {
          visible: false,
          value: 0
        };
        self.downLoadStatus = 0;
        self.downLoadIndex = 0;
      } else if (res.status == 4) {
        _prompt2.default.toast("网络无法连接，请联网后再操作. ");
        self.books[self.downLoadIndex]["progress"] = {
          visible: false,
          value: 0
        };
        self.downLoadStatus = 0;
        self.downLoadIndex = 0;
      }
      this.$set(self.books, self.downLoadIndex, self.books[self.downLoadIndex]);
      console.log(res);
    });
  },

  methods: {
    onchange: function onchange(event) {
      console.log("changed:", event.index);
    },
    bannerJump: function bannerJump(event) {
      console.log("11");
    },
    loadData: function loadData() {
      console.log("--------------loadData ----------------");
      var self = this;
      self.getData();
    },
    jumpToBookProfile: function jumpToBookProfile(id, title, address, imgUrl) {
      var _this = this;

      (0, _creader.getBookShelf)().then(function (data) {
        var h = -1;
        for (var _i = 0; _i < data.length; _i++) {
          if (data[_i].id == id) {
            // data = data.splice(i,1);
            h = _i;
          }
        }
        var arr = data.splice(h, 1);
        data.unshift(arr[0]);
        (0, _creader.saveBookShelf)(data);
        if (_this.downLoadStatus == 1) {
          _prompt2.default.toast("一次只能下载一本书籍，请稍后再试.");
          return;
        } else {
          _this.downLoadStatus == 1;
          var len = _this.books.length;
          for (var i = 0; i < len; i++) {
            if (_this.books[i].id == id) {
              _this.downLoadIndex = i;
              break;
            }
          }
          // googleTrack.userLibraryOpenBook(title,id);
          var event = weex.requireModule("event");
          event.openBook(id, title, address, imgUrl, (0, _fetch.getHost)() + "/display/index/displayBook?id=" + id);
        }
      });
    },
    openPopUp: function openPopUp(id, index) {
      var self = this;
      self.currentBookId = id;
      self.currentIndex = index;
      // prompt.toast(self.currentBookId+'-----'+self.currentIndex);
      self.show = true;
    },
    dialogCancelBtnClick: function dialogCancelBtnClick() {
      this.show = false;
      // prompt.toast("我点了取消");
    },
    dialogConfirmBtnClick: function dialogConfirmBtnClick() {
      var _this2 = this;

      var self = this;
      self.show = false;
      // prompt.toast(self.currentBookId);
      (0, _creader.userRemoveBookToShelf)(self.currentBookId).then(function (res) {
        console.log(res);
        if (res.data.status == 1) {
          self.books.splice(self.currentIndex, 1);
          (0, _creader.saveBookShelf)(_this2.books);
        } else {
          _prompt2.default.toast("移除失败，请稍后再试");
        }
      });
    },
    refresh: function refresh() {
      var self = this;
      global.init();
      self.netStatus = 0;
      self.getData();
    },
    isExist: function isExist(item) {
      var len = this.books.length;
      for (var i = 0; i < len; i++) {
        if (this.books[i].id == item.id) {
          return true;
          break;
        }
      }
      return false;
    },
    syncData: function syncData() {
      var _this3 = this;

      //采用循环调用，把所有的数据同步完成
      (0, _creader.getBookListByService)(this.books, this.bookIndex).then(function (res) {
        var res = res.data;
        // console.log(" -- this.bookIndex  --");
        // console.log(this.bookIndex);
        console.log("qingsong");
        console.log(res);
        if (res.status == 1) {
          _this3.bookTotal = res.data.total;
          var tlist = [];
          res.data.list.forEach(function (item) {
            //从服务器拿到数据后遍历，是否有新添加的书籍
            if (_this3.isExist(item) == false) {
              item["progress"] = { visible: false, value: 0 };
              item["img_url"] = res.data.host + item["img_url"];
              item["address"] = res.data.host + item["address"];
              tlist.push(item);
            }
          });
          if (tlist.length > 0) {
            _this3.books = _this3.books.concat(tlist);
          }
          if (_this3.bookIndex * 50 >= _this3.bookTotal) {
            //                                console.log(" ----- this.books ---- ");
            //                                console.log(this.books);
            (0, _creader.saveBookShelf)(_this3.books);
            if (_this3.books.length == 0) {
              _this3.netStatus = 1;
              _this3.errorType = "noResult";
              _this3.errorMsg = "您的书架里还没有添加任何书籍..";
            } else {
              _this3.netStatus = 2;
              _this3.imgDownLoad = true;
            }
          } else {
            _this3.bookIndex = _this3.bookIndex + 1;
            _this3.syncData();
          }
        }
      });
    },
    getData: function getData() {
      var self = this;
      if (self.isSync == false) {
        self.isSync = true;
        (0, _creader.getBookShelf)().then(function (data) {
          self.books = data;
          if (self.books.length > 0) {
            self.netStatus = 2;
          }
          if (data.length == 0) {
            setTimeout(self.syncData, 400);
            self.netStatus = 1;
            self.errorType = "noResult";
            self.errorMsg = "您的书架里还没有添加任何书籍..";
          } else {
            setTimeout(self.syncData, 3000);
          }
        });
      }
    },
    tabBarCurrentTabSelected: function tabBarCurrentTabSelected(e) {
      var page = e.page;
      console.log(page);
      (0, _navigator.jumpSubPage)(this, page, { closeThisPage: true });
    }
  }
};
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(12)))

/***/ }),
/* 56 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(global) {

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _common = __webpack_require__(1);

var _navigator = __webpack_require__(0);

var _creader = __webpack_require__(4);

var _prompt = __webpack_require__(2);

var _prompt2 = _interopRequireDefault(_prompt);

var _googleTrack = __webpack_require__(10);

var _googleTrack2 = _interopRequireDefault(_googleTrack);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

var domModule = weex.requireModule("dom");
exports.default = {
    data: function data() {
        return {
            getImgPath: _common.getImgPath,
            currentPosition: 1,
            tags: [],
            books: [],
            storageList: [],
            global: global,
            selected: false,
            errorType: '',
            errorMsg: '',
            Id: 0,
            imgDownLoad: false,
            netStatus: 0,
            pageIndex: 1,
            currentTagId: 143,
            leftHeight: '',
            deviceHeight: '',
            height: ''
        };
    },

    created: function created() {
        this.getData(143, 1);
        // googleTrack.userLibraryOpenCategory("");
    },
    methods: {
        jumpToBookProfile: function jumpToBookProfile(id) {
            (0, _navigator.jumpSubPage)(this, 'libraryBookIntroduce', { id: id });
        },
        getMoreInfo: function getMoreInfo() {
            var self = this;
            self.pageIndex = self.pageIndex + 1;
            self.getData(self.currentTagId, self.pageIndex);
        },
        refresh: function refresh() {
            var self = this;
            self.netStatus = 0;
            self.getData(self.currentId, self.pageIndex);
        },
        getData: function getData(id, pageIndex) {
            var self = this;
            (0, _creader.getBooks)(id, pageIndex).then(function (res) {
                console.log("qingsong123");
                console.log(res);
                if (res.status != -1 && typeof res.data != "undefined" && res.data != null) {
                    self.netStatus = 2;
                    self.tags = res.data.data.tags;
                    self.books = self.books.concat(res.data.data.books);
                    self.imgDownLoad = true;
                } else {
                    self.netStatus = 1;
                    self.errorType = 'neterror';
                }
            });
        },
        getBooksByCategory: function getBooksByCategory(index, id) {
            var self = this;
            self.books = [], self.pageIndex = 1;
            for (var i = 0; i < self.tags.length; i++) {
                if (i == index) {
                    self.Id = i;
                    self.getData(id, 1);
                    self.currentTagId = self.tags[i].id;
                    // googleTrack.userLibraryOpenCategory(self.tags[i].name);
                    return;
                }
            }
        }
    }

};
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(12)))

/***/ }),
/* 57 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _Utils = __webpack_require__(11);

var _Utils2 = _interopRequireDefault(_Utils);

var _navigator = __webpack_require__(0);

var _common = __webpack_require__(1);

var _setting = __webpack_require__(43);

var _setting2 = _interopRequireDefault(_setting);

var _googleTrack = __webpack_require__(10);

var _googleTrack2 = _interopRequireDefault(_googleTrack);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

var dom = weex.requireModule('dom');
var animation = weex.requireModule('animation');
exports.default = {
  props: {
    currentPage: {
      type: Number
    },
    currentPosition: {
      type: Number
    },
    tabTitles: {
      type: Array,
      default: function _default() {
        return [];
      }
    },
    isShelf: {
      type: Boolean,
      default: true
    },
    tabStyles: {
      type: Object,
      default: function _default() {
        return {
          bgColor: '#FFFFFF',
          titleColor: '#515151',
          activeTitleColor: '#E15D53',
          activeBgColor: '#ffffff',
          isActiveTitleBold: true,
          iconWidth: 60,
          iconHeight: 60,
          width: 160,
          height: 120,
          borderBottomColor: '#FFC900',
          borderBottomWidth: 0,
          borderBottomHeight: 0,
          activeBorderBottomColor: '#E15D53',
          activeBorderBottomWidth: 6,
          activeBorderBottomHeight: 6,
          fontSize: 31,
          textPaddingLeft: 10,
          textPaddingRight: 10
        };
      }
    },
    titleType: {
      type: String,
      default: 'icon'
    },
    isTabView: {
      type: Boolean,
      default: true
    },
    duration: {
      type: [Number, String],
      default: 300
    },
    timingFunction: {
      type: String,
      default: 'cubic-bezier(0.25, 0.46, 0.45, 0.94)'
    }
  },
  data: function data() {
    return {
      topNav: ['书架', '分类', '书单', '书店'],
      getImgPath: _common.getImgPath

    };
  },
  created: function created() {
    var titleType = this.titleType,
        tabStyles = this.tabStyles;
  },

  methods: {
    setPage: function setPage(index) {
      console.log(index + " -- " + this.currentPosition);
      if (index == this.currentPosition) {
        return;
      }
      var page = '';
      switch (index) {
        case 0:
          page = 'libraryBookShelf';
          break;
        case 1:
          page = 'libraryCategory';
          break;
        case 2:
          page = 'libraryBookLists';
          break;
        case 3:
          page = 'storeEntry';
          break;
        case 4:
          page = 'librarySearch';
          break;
      }
      var parm = {};
      if (page == 'libraryBookShelf') {
        this.back();
      } else {
        if (this.currentPosition == 0) {
          (0, _navigator.jumpSubPage)(this, page);
        } else {
          (0, _navigator.jumpSubPage)(this, page, { closeThisPage: true });
        }
      }
    },
    back: function back() {
      if (_setting2.default.isWeb() || _setting2.default.isAndroid()) {
        (0, _navigator.back)(this, "");
      } else {
        (0, _navigator.jumpSubPage)(this, 'libraryBookShelf', { backToRootView: true, closeThisPage: true });
      }
    }
  }

};

/***/ }),
/* 58 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(global) {

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _weextabbarConfig = __webpack_require__(7);

var _weextabbarConfig2 = _interopRequireDefault(_weextabbarConfig);

var _common = __webpack_require__(1);

var _navigator = __webpack_require__(0);

var _store = __webpack_require__(42);

var _prompt = __webpack_require__(2);

var _prompt2 = _interopRequireDefault(_prompt);

var _googleTrack = __webpack_require__(10);

var _googleTrack2 = _interopRequireDefault(_googleTrack);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

var domModule = weex.requireModule("dom");
exports.default = {
  data: function data() {
    return {
      getImgPath: _common.getImgPath,
      currentPosition: 3,
      tags: [],
      books: [],
      message: _weextabbarConfig2.default.bookstoremessage.message,
      global: global,
      errorType: "",
      errorMsg: "",
      netStatus: 0,
      deviceHeight: "",
      height: ""
    };
  },

  created: function created() {
    var deviceHeight = WXEnvironment.deviceHeight || unknown;
    var deviceWidth = WXEnvironment.deviceWidth || unknown;
    this.height = 750 / deviceWidth * deviceHeight;
    this.getData();
    //   googleTrack.userLibraryOpenBookshop();
  },
  methods: {
    jumpToBookDetail: function jumpToBookDetail(id) {
      (0, _navigator.jumpSubPage)(this, "storeBookDetail", { id: id, from: "shop" });
    },
    jumpToCategoryDetail: function jumpToCategoryDetail(id, category) {
      (0, _navigator.jumpSubPage)(this, "storeCategory", { id: id, category: category });
    },
    refresh: function refresh() {
      var self = this;
      self.netStatus = 0;
      self.getData();
    },
    getData: function getData() {
      var self = this;
      (0, _store.getBookList)().then(function (res) {
        console.log("qingsong999");
        console.log(res);
        if (res.status != -1 && typeof res.data != "undefined" && res.data != null) {
          self.netStatus = 2;
          self.tags = res.data.data.tags;
          self.books = self.books.concat(res.data.data.books);
        } else {
          self.netStatus = 1;
          self.errorType = "neterror";
        }
      });
    }
  }
};
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(12)))

/***/ }),
/* 59 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _common = __webpack_require__(1);

//
//
//
//
//
//

var animation = weex.requireModule('animation');
var modal = weex.requireModule('modal');
exports.default = {
    data: function data() {
        return {
            current_rotate: 0,
            loading: false,
            height: '',
            getImgPath: _common.getImgPath
        };
    },
    created: function created() {
        // let deviceHeight = WXEnvironment.deviceHeight;
        // let deviceWidth = WXEnvironment.deviceWidth;
        // this.height = (deviceHeight*750/deviceWidth-208)/2;
    },


    methods: {
        rotate: function rotate(el, x) {
            var self = this;
            this.current_rotate += 360;
            animation.transition(el, {
                styles: {
                    opacity: x == 1 ? 0.5 : 1,
                    transform: 'scale(' + (x == 1 ? 0.6 : 1) + ')' + 'rotate(' + self.current_rotate + 'deg)',
                    // transform: 'rotate(' + self.current_rotate + 'deg)',
                    transformOrigin: 'center center'
                },
                duration: 600,
                timingFunction: 'linear',
                delay: 0
            }, function () {
                this.loading && this.rotate(el, -1 * x);
            }.bind(this));
        }
    }
};

/***/ }),
/* 60 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _global = __webpack_require__(3);

var _global2 = _interopRequireDefault(_global);

var _navigator = __webpack_require__(0);

var _common = __webpack_require__(1);

var _prompt = __webpack_require__(2);

var _prompt2 = _interopRequireDefault(_prompt);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
    data: function data() {
        return {
            global: _global2.default,
            getImgPath: _common.getImgPath,
            noResult: 'noResult',
            neterror: 'neterror'
        };
    },

    props: {
        networkError: String,
        errorType: {
            type: String
        },
        errorMsg: String,
        refresh: String
    },
    methods: {
        Refresh: function Refresh() {
            this.$emit('toNetError');
        }
    },

    created: function created() {
        _global2.default.init();
    }
};

/***/ }),
/* 61 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _global = __webpack_require__(3);

var _global2 = _interopRequireDefault(_global);

var _navigator = __webpack_require__(0);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
    data: function data() {
        return {
            global: _global2.default
        };
    },

    props: {
        networkError: String,
        refresh: String
    },
    methods: {
        Refresh: function Refresh() {
            console.log("1111");
            this.$emit('childMethod');
        }
    },

    created: function created() {
        _global2.default.init();
    }
};

/***/ }),
/* 62 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
    props: {
        barColor: {
            type: String,
            default: '#9B7B56'
        },
        barWidth: {
            type: Number,
            default: 600
        },
        barHeight: {
            type: Number,
            default: 8
        },
        value: {
            type: Number,
            default: 0
        }
    },
    computed: {
        runWayStyle: function runWayStyle() {
            var barWidth = this.barWidth,
                barHeight = this.barHeight;

            return {
                width: barWidth + 'px',
                height: barHeight + 'px'
            };
        },
        progressStyle: function progressStyle() {
            var value = this.value,
                barWidth = this.barWidth,
                barHeight = this.barHeight,
                barColor = this.barColor;

            var newValue = value < 0 ? 0 : value > 100 ? 100 : value;
            return {
                backgroundColor: barColor,
                height: barHeight + 'px',
                width: newValue / 100 * barWidth + 'px'
            };
        }
    }
};

/***/ }),
/* 63 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _common = __webpack_require__(1);

var _prompt = __webpack_require__(2);

var _prompt2 = _interopRequireDefault(_prompt);

var _navigator = __webpack_require__(0);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
  props: {
    disabled: {
      type: Boolean,
      default: false
    },
    alwaysShowCancel: {
      type: Boolean,
      default: true
    },
    inputType: {
      type: String,
      default: 'text'
    },
    returnKeyType: {
      type: String,
      default: 'default'
    },
    mod: {
      type: String,
      default: 'default'
    },
    autofocus: {
      type: Boolean,
      default: false
    },
    theme: {
      type: String,
      default: 'gray'
    },
    barStyle: {
      type: Object,
      default: function _default() {
        return {};
      }
    },
    defaultValue: {
      type: String,
      default: ''
    },
    placeholder: {
      type: String,
      default: '搜索'
    },
    cancelLabel: {
      type: String,
      default: '搜索'
    },
    depName: {
      type: String,
      default: '杭州'
    }
  },
  computed: {
    needShowCancel: function needShowCancel() {
      return this.alwaysShowCancel || this.showCancel;
    },
    buttonStyle: function buttonStyle() {
      var barStyle = this.barStyle;

      if (barStyle.backgroundColor) {
        return { backgroundColor: barStyle.backgroundColor };
      }
      return {};
    }
  },
  data: function data() {
    return {
      getImgPath: _common.getImgPath,
      showCancel: false,
      showClose: false,
      value: ''

    };
  },
  created: function created() {
    this.defaultValue && (this.value = this.defaultValue);
    if (this.disabled) {
      this.showCancel = false;
      this.showClose = false;
    }
  },

  methods: {
    onBlur: function onBlur() {
      var self = this;
      setTimeout(function () {
        self.showCancel = false;
        self.detectShowClose();
        self.$emit('wxcSearchbarInputOnBlur', { value: self.value });
      }, 10);
    },
    goBack: function goBack() {
      (0, _navigator.back)(this, '');
    },
    autoBlur: function autoBlur() {
      this.$refs['search-input'].blur();
    },
    onFocus: function onFocus() {
      if (this.isDisabled) {
        return;
      }
      this.showCancel = true;
      this.detectShowClose();
      this.$emit('wxcSearchbarInputOnFocus', { value: this.value });
    },
    closeClicked: function closeClicked() {
      this.value = '';
      // this.showCancel && (this.showCancel = false);
      this.showClose && (this.showClose = false);
      this.$emit('wxcSearchbarCloseClicked', { value: this.value });
      this.$emit('wxcSearchbarInputOnInput', { value: this.value });
    },
    onInput: function onInput(e) {
      this.value = e.value;
      this.showCancel = true;
      this.detectShowClose();
      this.$emit('wxcSearchbarInputOnInput', { value: this.value });
    },
    onSubmit: function onSubmit(e) {
      this.onBlur();
      this.value = e.value;
      this.showCancel = true;
      this.detectShowClose();
      this.$emit('wxcSearchbarInputReturned', { value: this.value });
    },
    searchClicked: function searchClicked() {
      // this.showCancel && (this.showCancel = false);
      // this.showClose && (this.showClose = false);
      if (this.value) {
        this.$emit('wxcSearchbarCancelClicked', { value: this.value });
      } else {
        _prompt2.default.toast("请输入关键词");
      }
    },
    detectShowClose: function detectShowClose() {
      this.showClose = this.value.length > 0 && this.showCancel;
    },
    depClicked: function depClicked() {
      this.$emit('wxcSearchbarDepChooseClicked', {});
    },
    inputDisabledClicked: function inputDisabledClicked() {
      this.$emit('wxcSearchbarInputDisabledClicked', {});
    },
    setValue: function setValue(value) {
      this.value = value;
    }
  }
}; //
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

// import { INPUT_ICON, ARROW_ICON, CLOSE_ICON } from './type';

/***/ }),
/* 64 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _global = __webpack_require__(3);

var _global2 = _interopRequireDefault(_global);

var _navigator = __webpack_require__(0);

var _common = __webpack_require__(1);

var _prompt = __webpack_require__(2);

var _prompt2 = _interopRequireDefault(_prompt);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
    data: function data() {
        return {
            global: _global2.default,
            getImgPath: _common.getImgPath,
            loadinging: true,
            height: ''
        };
    },

    props: {
        networkError: String,
        refresh: String
    },
    methods: {
        Refresh: function Refresh() {
            console.log("1111");
            this.$emit('childMethod');
        }
    },

    created: function created() {
        var deviceHeight = WXEnvironment.deviceHeight;
        var deviceWidth = WXEnvironment.deviceWidth;
        this.height = (deviceHeight * 750 / deviceWidth - 108) / 2;
        _global2.default.init();
    }
};

/***/ }),
/* 65 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _common = __webpack_require__(1);

var _navigator = __webpack_require__(0);

var _global = __webpack_require__(3);

var _global2 = _interopRequireDefault(_global);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//
//
//
//
//
//
//
//
//
//
//

exports.default = {
  data: function data() {
    return {
      global: _global2.default,
      getImgPath: _common.getImgPath,
      app: 'ion-android-apps',
      search: 'ion-search'
    };
  },

  props: {
    secondaryPage: {
      type: Boolean,
      default: false
    },
    name: {
      type: String,
      default: ''
    },
    height: {
      type: String,
      default: '100px'
    },
    textStyle: String,
    textColor: {
      type: String,
      default: 'white'
    },
    bgcolor: {
      type: String,
      default: '#E15D53'
    }
  },
  methods: {
    goBack: function goBack() {
      console.log("qingsong1");
      (0, _navigator.back)(this, "");
    },
    jumpToSearch: function jumpToSearch() {
      (0, _navigator.jumpSubPage)(this, "searchPage");
    },
    jumpToMySubscribe: function jumpToMySubscribe() {
      console.log("qingsongceshi1111111111111");
      (0, _navigator.jumpSubPage)(this, "mySubscribe");
    }
  }
};

/***/ }),
/* 66 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _common = __webpack_require__(1);

var _navigator = __webpack_require__(0);

var _global = __webpack_require__(3);

var _global2 = _interopRequireDefault(_global);

var _fetch = __webpack_require__(6);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
  data: function data() {
    return {
      global: _global2.default,
      getImgPath: _common.getImgPath,
      url: (0, _fetch.getHost)() + "/display/index/displayAlbumInfo?id="
    };
  },

  props: {
    secondaryPage: {
      type: Boolean,
      default: false
    },
    willHide: {
      type: Boolean,
      default: false
    },
    name: {
      type: String,
      default: ''
    },
    height: {
      type: String,
      default: '100px'
    },
    textStyle: String,
    textColor: {
      type: String,
      default: 'white'
    },
    albumInfo: {
      type: Object
    },
    bgcolor: {
      type: String,
      default: '#E15D53'
    },
    shareColor: {
      type: String,
      default: '#ffffff'
    }
  },
  methods: {
    goBack: function goBack() {
      (0, _navigator.back)(this, "");
    },
    jumpToMySubscribe: function jumpToMySubscribe() {
      (0, _navigator.jumpSubPage)(this, "mySubscribe");
    },
    share: function share(albumInfo) {
      console.log(albumInfo);
      var url = "";
      this.$refs.commonFun.ShareArticle(albumInfo.author, this.url + albumInfo.id, albumInfo.img_url, albumInfo.remark);
    }
  }
}; //
//
//
//
//
//
//
//
//
//
//

/***/ }),
/* 67 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _common = __webpack_require__(1);

var _navigator = __webpack_require__(0);

var _global = __webpack_require__(3);

var _global2 = _interopRequireDefault(_global);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
  name: 'topbar',
  data: function data() {
    return {
      global: _global2.default,
      getImgPath: _common.getImgPath
    };
  },

  props: {
    topbarname: String,
    textStyle: String,
    bgcolor: {
      type: String,
      default: '#E15D53'
    }
  },
  methods: {
    goBack: function goBack() {
      (0, _navigator.back)(this, "");
    }
  }
}; //
//
//
//
//
//
//
//
//
//
//
//

/***/ }),
/* 68 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
//
//
//
//
//
//
//
//
//


exports.default = {
    props: {
        src: String,
        title: String,
        isLandscape: Boolean
    }
};

/***/ }),
/* 69 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
  props: {
    show: {
      type: Boolean,
      default: false
    },
    single: {
      type: Boolean,
      default: false
    },
    title: {
      type: String,
      default: ''
    },
    content: {
      type: String,
      default: ''
    },
    top: {
      type: Number,
      default: 400
    },
    cancelText: {
      type: String,
      default: '取消'
    },
    confirmText: {
      type: String,
      default: '确定'
    },
    mainBtnColor: {
      type: String,
      default: '#E15D53'
    },
    secondBtnColor: {
      type: String,
      default: '#666666'
    },
    showNoPrompt: {
      type: Boolean,
      default: false
    },
    noPromptText: {
      type: String,
      default: '不再提示'
    },
    isChecked: {
      type: Boolean,
      default: false
    }
  },
  data: function data() {
    return {
      pageHeight: 1334
    };
  },
  created: function created() {
    var _weex$config$env = weex.config.env,
        deviceHeight = _weex$config$env.deviceHeight,
        deviceWidth = _weex$config$env.deviceWidth;

    this.pageHeight = deviceHeight / deviceWidth * 750;
  },

  methods: {
    secondaryClicked: function secondaryClicked() {
      this.$emit('wxcDialogCancelBtnClicked', {
        type: 'cancel'
      });
    },
    primaryClicked: function primaryClicked(e) {
      this.$emit('wxcDialogConfirmBtnClicked', {
        type: 'confirm'
      });
    }
  }
};

/***/ }),
/* 70 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

var animation = weex.requireModule('animation');
exports.default = {
  props: {
    show: {
      type: Boolean,
      default: true
    },
    hasAnimation: {
      type: Boolean,
      default: true
    },
    duration: {
      type: [Number, String],
      default: 300
    },
    timingFunction: {
      type: Array,
      default: function _default() {
        return ['ease-in', 'ease-out'];
      }
    },
    opacity: {
      type: [Number, String],
      default: 0.6
    },
    canAutoClose: {
      type: Boolean,
      default: true
    }
  },
  computed: {
    overlayStyle: function overlayStyle() {
      return {
        opacity: this.hasAnimation ? 0 : 1,
        backgroundColor: 'rgba(0, 0, 0,' + this.opacity + ')'
      };
    },
    shouldShow: function shouldShow() {
      var _this = this;

      var show = this.show,
          hasAnimation = this.hasAnimation;

      hasAnimation && setTimeout(function () {
        _this.appearOverlay(show);
      }, 50);
      return show;
    }
  },
  methods: {
    overlayClicked: function overlayClicked(e) {
      this.canAutoClose ? this.appearOverlay(false) : this.$emit('wxcOverlayBodyClicked', {});
    },
    appearOverlay: function appearOverlay(bool) {
      var _this2 = this;

      var duration = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : this.duration;
      var hasAnimation = this.hasAnimation,
          timingFunction = this.timingFunction,
          canAutoClose = this.canAutoClose;

      var needEmit = !bool && canAutoClose;
      needEmit && this.$emit('wxcOverlayBodyClicking', {});
      var overlayEl = this.$refs['wxc-overlay'];
      if (hasAnimation && overlayEl) {
        animation.transition(overlayEl, {
          styles: {
            opacity: bool ? 1 : 0
          },
          duration: duration,
          timingFunction: timingFunction[bool ? 0 : 1],
          delay: 0
        }, function () {
          needEmit && _this2.$emit('wxcOverlayBodyClicked', {});
        });
      } else {
        needEmit && this.$emit('wxcOverlayBodyClicked', {});
      }
    }
  }
};

/***/ }),
/* 71 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
//
//
//
//
//


exports.default = {
    props: {
        src: String,
        web: String
    }
};

/***/ }),
/* 72 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _navigator = __webpack_require__(0);

exports.default = {
    // mixins: [mixins],
    props: {
        tabItems: {
            type: Array,
            default: function _default() {
                return [];
            },
            required: true
        },

        styles: {
            type: Object,
            default: function _default() {
                return {};
            },
            required: false
        },
        height: {
            type: String,
            default: '108px'
        },
        network: {
            default: 1
        }
    },

    data: function data() {
        return {
            selectedTab: 0,
            translateX: 'translateX(0px)',
            deviceWidth: 750,
            titleStyle: {}
        };
    },
    created: function created() {
        var self = this;
        self.totalWidth = self.deviceWidth * self.tabItems.length;
    },
    mounted: function mounted() {
        var self = this;
        if (self.network == 1) {
            self.selectedTab = 0;
        } else if (self.network == 3) {
            self.selectedTab = 1;
            self.$emit('wxChange', self.selectedTab);
        } else {
            self.selectedTab = 3;
            self.$emit('wxChange', self.selectedTab);
        }
        // self.totalWidth = self.deviceWidth * self.tabItems.length;
        self.setTranslateX();
    },


    methods: {
        changeTab: function changeTab(item, index) {
            this.selectedTab = index;
            this.setTranslateX();
            this.$emit('wxChange', index);
        },
        setTranslateX: function setTranslateX() {
            var x = this.selectedTab * 750; //this.deviceWidth;
            this.translateX = 'translateX(-' + x + 'px)';
        },
        getStyles: function getStyles() {
            var baseStyle = {
                'bottom': 0,
                'height': this.height
            };
            return Object.assign({}, baseStyle, this.styles);
        },
        getIconStyle: function getIconStyle(item) {
            return {
                width: item.iconWdith || '48px',
                height: item.iconHeight || '48px'
            };
        },
        getTitleStyle: function getTitleStyle(item) {
            return {
                'font-size': item.fontSize || '28px',
                'color': this.selectedTab === item.index ? item.selectedColor : item.titleColor
            };
        }
    }
}; //
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/***/ }),
/* 73 */
/***/ (function(module, exports) {

module.exports = {
  "body": {
    "backgroundColor": "#ffffff",
    "width": "750",
    "position": "absolute",
    "top": "200",
    "left": 0,
    "right": 0,
    "bottom": 0
  },
  "promptInformation": {
    "width": "750",
    "position": "absolute",
    "top": "0",
    "left": 0,
    "right": 0,
    "bottom": 0,
    "alignItems": "center",
    "justifyContent": "space-around"
  },
  "top": {
    "backgroundColor": "#ffffff"
  },
  "messagebox": {
    "top": "0",
    "left": 0,
    "right": 0,
    "height": "60",
    "backgroundColor": "#ffffff",
    "alignItems": "center",
    "justifyContent": "center"
  },
  "mes": {
    "fontSize": "30",
    "color": "#959595"
  },
  "category_box": {
    "backgroundColor": "#ffffff",
    "height": "430",
    "width": "750"
  },
  "category_box_top": {
    "flexDirection": "row",
    "justifyContent": "space-between",
    "alignItems": "center",
    "paddingLeft": "30",
    "paddingRight": "0",
    "height": "80",
    "width": "750",
    "borderBottomColor": "#e8e8e8",
    "borderBottomWidth": "2"
  },
  "category_title": {
    "fontSize": "30",
    "fontWeight": "bold",
    "color": "#515151"
  },
  "more": {
    "justifyContent": "flex-end",
    "alignItems": "center",
    "flexDirection": "row",
    "height": "80",
    "width": "180"
  },
  "books": {
    "width": "750",
    "height": "350",
    "flexDirection": "row",
    "justifyContent": "flex-start",
    "alignItems": "center",
    "position": "relative",
    "paddingLeft": "40"
  },
  "book": {
    "width": "200",
    "height": "307",
    "marginTop": "20"
  },
  "bookimage": {
    "width": "160",
    "height": "220",
    "borderRadius": "5",
    "position": "relative",
    "top": "0"
  },
  "booktitle": {
    "lines": 2,
    "fontSize": "26",
    "color": "#5b5b5b",
    "paddingTop": "10",
    "paddingBottom": "10",
    "marginLeft": "10",
    "marginRight": "10",
    "textAlign": "center",
    "width": "160"
  },
  "bottom-white": {
    "width": "750",
    "height": "180",
    "backgroundColor": "#ffffff"
  }
}

/***/ }),
/* 74 */
/***/ (function(module, exports) {

module.exports = {
  "video": {
    "width": "750",
    "height": "480"
  }
}

/***/ }),
/* 75 */
/***/ (function(module, exports) {

module.exports = {
  "body": {
    "width": "750"
  }
}

/***/ }),
/* 76 */
/***/ (function(module, exports) {

module.exports = {}

/***/ }),
/* 77 */
/***/ (function(module, exports) {

module.exports = {}

/***/ }),
/* 78 */
/***/ (function(module, exports) {

module.exports = {
  "wxc-overlay": {
    "width": "750",
    "position": "fixed",
    "left": 0,
    "top": 0,
    "bottom": 0,
    "right": 0
  }
}

/***/ }),
/* 79 */
/***/ (function(module, exports) {

module.exports = {
  "waterfall": {
    "position": "absolute",
    "top": "0",
    "left": "200",
    "right": 0,
    "bottom": 0,
    "paddingBottom": "180"
  },
  "book": {
    "width": "200",
    "height": "350",
    "marginLeft": "37",
    "marginTop": "50",
    "marginBottom": "20",
    "justifyContent": "flex-start",
    "alignItems": "center"
  },
  "body": {
    "width": "750",
    "position": "absolute",
    "top": "200",
    "left": 0,
    "right": 0,
    "bottom": "0"
  },
  "type": {
    "fontSize": "38",
    "color": "#636363",
    "fontWeight": "bold"
  },
  "search": {
    "fontFamily": "iconfont"
  },
  "content_category": {
    "position": "absolute",
    "top": "0",
    "left": "0",
    "right": 0,
    "bottom": 0,
    "flexDirection": "row",
    "flexWrap": "nowrap",
    "justifyContent": "flex-start"
  },
  "left": {
    "width": "200",
    "borderRightColor": "#e9e9e9",
    "borderRightWidth": "1",
    "borderRightStyle": "solid",
    "flexDirection": "column",
    "marginTop": "20"
  },
  "namecount": {
    "height": "122",
    "flexDirection": "column",
    "alignItems": "center",
    "justifyContent": "center"
  },
  "namecount-selected": {
    "height": "122",
    "flexDirection": "column",
    "alignItems": "center",
    "justifyContent": "center",
    "borderLeftColor": "#E15D53",
    "borderLeftWidth": "5",
    "borderLeftStyle": "solid"
  },
  "category": {
    "textAlign": "center",
    "height": "50",
    "lineHeight": "50",
    "fontSize": "33",
    "fontWeight": "bold",
    "color": "#5b5b5b"
  },
  "category-selected": {
    "height": "50",
    "lineHeight": "50",
    "textAlign": "center",
    "fontSize": "33",
    "fontWeight": "bold",
    "color": "#E15D53"
  },
  "count": {
    "height": "50",
    "fontSize": "26",
    "lineHeight": "50",
    "textAlign": "center",
    "color": "#aeaeae"
  },
  "count-selected": {
    "height": "50",
    "fontSize": "26",
    "lineHeight": "50",
    "color": "#E15D53",
    "textAlign": "center"
  },
  "right-books": {
    "flexDirection": "row",
    "flexWrap": "wrap",
    "justifyContent": "flex-start",
    "alignItems": "flex-start",
    "position": "absolute",
    "top": 0,
    "left": "200",
    "right": 0
  },
  "bookname": {
    "fontSize": "26",
    "color": "#505050",
    "marginTop": "26",
    "marginBottom": "15",
    "marginLeft": "12",
    "marginRight": "12",
    "lines": 2,
    "textAlign": "center",
    "width": "200"
  },
  "bottom-white": {
    "width": "750",
    "height": "100"
  },
  "promptInformation": {
    "width": "750",
    "position": "absolute",
    "top": "0",
    "left": 0,
    "right": 0,
    "bottom": 0,
    "alignItems": "center",
    "justifyContent": "space-around"
  }
}

/***/ }),
/* 80 */
/***/ (function(module, exports) {

module.exports = {
  "wx-tabbar": {
    "width": "750"
  },
  "tab-component": {
    "flexDirection": "row"
  },
  "tabbar": {
    "width": "750",
    "position": "fixed",
    "left": 0,
    "right": 0,
    "bottom": 0,
    "zIndex": 1000,
    "flexDirection": "row",
    "justifyContent": "space-around",
    "alignItems": "center",
    "borderTopWidth": "1",
    "borderTopStyle": "solid",
    "borderTopColor": "#D8D8D8",
    "backgroundColor": "#ffffff"
  },
  "tabbar-item": {
    "flex": 1,
    "flexDirection": "column",
    "alignItems": "center",
    "justifyContent": "center"
  },
  "icon": {
    "width": "48",
    "height": "48"
  },
  "wx-text": {
    "fontSize": "25",
    "paddingTop": "2",
    "textAlign": "center",
    "color": "#646464"
  }
}

/***/ }),
/* 81 */
/***/ (function(module, exports) {

module.exports = {
  "content": {
    "flexDirection": "row",
    "flexFlow": "nowrap",
    "justifyContent": "space-between",
    "alignItems": "center",
    "paddingLeft": "30",
    "paddingRight": "30"
  },
  "title": {
    "fontSize": "34",
    "fontFamily": "Roboto"
  }
}

/***/ }),
/* 82 */
/***/ (function(module, exports) {

module.exports = {
  "wrapper": {
    "width": "750",
    "position": "absolute",
    "top": "0",
    "left": 0,
    "right": 0,
    "bottom": 0
  },
  "top": {
    "width": "750",
    "height": "100",
    "backgroundColor": "#E15D53",
    "flexDirection": "row",
    "justifyContent": "space-between",
    "alignItems": "center",
    "paddingRight": "30",
    "paddingLeft": "30"
  },
  "tab-bar": {
    "flexDirection": "row",
    "justifyContent": "space-around",
    "alignItems": "center",
    "width": "750",
    "height": "100",
    "backgroundColor": "#ffffff"
  },
  "tab-item": {
    "flex": 1,
    "alignSelf": "stretch",
    "lineHeight": "100",
    "fontSize": "29",
    "fontWeight": "bold",
    "textAlign": "center",
    "fontFamily": "Roboto"
  },
  "tab-bar-border": {
    "width": "187.5",
    "height": "100",
    "position": "absolute",
    "top": "100",
    "left": 0,
    "borderBottomWidth": "4",
    "borderBottomStyle": "solid",
    "borderBottomColor": "#E15D53",
    "transitionProperty": "left",
    "transitionDuration": 200,
    "transitionTimingFunction": "ease-in-out"
  },
  "@TRANSITION": {
    "tab-bar-border": {
      "property": "left",
      "duration": 200,
      "timingFunction": "ease-in-out"
    }
  },
  "article-mid": {
    "flexDirection": "row",
    "flexWrap": "nowrap",
    "justifyContent": "space-between",
    "alignItems": "center",
    "paddingLeft": "30",
    "paddingRight": "30",
    "paddingTop": "25",
    "paddingBottom": "25",
    "backgroundColor": "#ffffff",
    "marginBottom": "19"
  },
  "article-content": {
    "flexDirection": "column",
    "flexWrap": "nowrap",
    "justifyContent": "space-between",
    "alignItems": "flex-start",
    "width": "400",
    "height": "194",
    "fontFamily": "Roboto"
  },
  "title": {
    "color": "#101010",
    "fontSize": "31",
    "fontFamily": "\"Helvetica Neue\",sans-serif",
    "fontWeight": "bold",
    "paddingBottom": "15",
    "paddingRight": "30"
  },
  "article_read_recommend": {
    "color": "#B6B6B6",
    "fontSize": "21"
  }
}

/***/ }),
/* 83 */
/***/ (function(module, exports) {

module.exports = {
  "back": {
    "width": "100",
    "height": "100",
    "justifyContent": "center",
    "alignItems": "center"
  },
  "topbar": {
    "position": "fixed",
    "top": 0,
    "width": "750",
    "height": "100",
    "flexDirection": "row",
    "justifyContent": "space-between",
    "alignItems": "center"
  },
  "right-part": {
    "width": "100",
    "height": "100"
  },
  "backButton": {
    "width": "100",
    "height": "100",
    "textAlign": "center",
    "lineHeight": "100",
    "fontSize": "40",
    "position": "absolute",
    "left": "0"
  }
}

/***/ }),
/* 84 */
/***/ (function(module, exports) {

module.exports = {
  "container": {
    "position": "fixed",
    "width": "750",
    "zIndex": 99999
  },
  "dialog-box": {
    "position": "fixed",
    "left": "96",
    "width": "558",
    "backgroundColor": "#FFFFFF"
  },
  "dialog-content": {
    "paddingTop": "36",
    "paddingBottom": "36",
    "paddingLeft": "36",
    "paddingRight": "36"
  },
  "content-title": {
    "color": "#333333",
    "fontSize": "36",
    "textAlign": "center",
    "marginBottom": "24"
  },
  "content-subtext": {
    "color": "#666666",
    "fontSize": "26",
    "lineHeight": "36",
    "textAlign": "center"
  },
  "dialog-footer": {
    "flexDirection": "row",
    "alignItems": "center",
    "borderTopColor": "#F3F3F3",
    "borderTopWidth": "1"
  },
  "footer-btn": {
    "flexDirection": "row",
    "alignItems": "center",
    "justifyContent": "center",
    "flex": 1,
    "height": "90"
  },
  "cancel": {
    "borderRightColor": "#F3F3F3",
    "borderRightWidth": "1"
  },
  "btn-text": {
    "fontSize": "36",
    "color": "#666666"
  },
  "no-prompt": {
    "width": "486",
    "alignItems": "center",
    "justifyContent": "center",
    "flexDirection": "row",
    "marginTop": "24"
  },
  "no-prompt-icon": {
    "width": "24",
    "height": "24",
    "marginRight": "12"
  },
  "no-prompt-text": {
    "fontSize": "24",
    "color": "#A5A5A5"
  }
}

/***/ }),
/* 85 */
/***/ (function(module, exports) {

module.exports = {}

/***/ }),
/* 86 */
/***/ (function(module, exports) {

module.exports = {
  "netDivError": {
    "width": "750",
    "display": "flex",
    "flexFlow": "column",
    "alignItems": "center",
    "justifyContent": "center",
    "height": "300"
  },
  "netTextError": {
    "paddingTop": "10",
    "paddingBottom": "10",
    "fontSize": "35"
  },
  "button": {
    "width": "280",
    "height": "80",
    "borderWidth": "1",
    "borderColor": "#000000",
    "borderStyle": "solid",
    "textAlign": "center",
    "lineHeight": "80",
    "fontSize": "35",
    "borderRadius": "10",
    "transform": "translateY(50px)"
  }
}

/***/ }),
/* 87 */
/***/ (function(module, exports) {

module.exports = {
  "wxc-search-bar": {
    "paddingLeft": "30",
    "paddingRight": "30",
    "backgroundColor": "#ffffff",
    "width": "750",
    "height": "100",
    "flexDirection": "row",
    "justifyContent": "space-between",
    "alignItems": "center"
  },
  "wxc-search-bar-yellow": {
    "backgroundColor": "#ffc900"
  },
  "search-bar-input": {
    "paddingTop": 0,
    "paddingBottom": 0,
    "paddingRight": "40",
    "paddingLeft": "60",
    "fontSize": "26",
    "height": "80",
    "lineHeight": "80",
    "backgroundColor": "#ffffff",
    "borderRadius": "6"
  },
  "search-bar-input-yellow": {
    "backgroundColor": "#fff6d6"
  },
  "search-bar-close": {
    "position": "absolute",
    "width": "30",
    "height": "30",
    "right": "120",
    "top": "36"
  },
  "search-bar-button": {
    "marginRight": 0,
    "color": "#333333"
  },
  "search-bar-button-yellow": {
    "backgroundColor": "#FFC900"
  },
  "input-has-dep": {
    "paddingLeft": "240",
    "width": "710"
  },
  "bar-dep": {
    "width": "170",
    "paddingRight": "12",
    "paddingLeft": "12",
    "height": "42",
    "alignItems": "center",
    "flexDirection": "row",
    "position": "absolute",
    "left": "24",
    "top": "22",
    "borderRightStyle": "solid",
    "borderRightWidth": "1",
    "borderRightColor": "#C7C7C7"
  },
  "bar-dep-yellow": {
    "borderRightColor": "#C7C7C7"
  },
  "dep-text": {
    "flex": 1,
    "textAlign": "center",
    "fontSize": "26",
    "color": "#666666",
    "marginRight": "6",
    "lines": 1,
    "textOverflow": "ellipsis"
  },
  "dep-arrow": {
    "width": "24",
    "height": "24"
  },
  "icon-has-dep": {
    "left": "214"
  },
  "disabled-input": {
    "width": "750",
    "height": "64",
    "position": "absolute",
    "left": 0,
    "backgroundColor": "rgba(0,0,0,0)"
  },
  "has-dep-disabled": {
    "width": "550",
    "left": "200"
  }
}

/***/ }),
/* 88 */
/***/ (function(module, exports) {

module.exports = {
  "icon-block": {
    "justifyContent": "flex-start"
  },
  "icon": {
    "textAlign": "center"
  }
}

/***/ }),
/* 89 */
/***/ (function(module, exports) {

module.exports = {
  "wxc-progress": {
    "backgroundColor": "#f2f3f4"
  },
  "progress": {
    "position": "absolute",
    "backgroundColor": "#FFC900"
  }
}

/***/ }),
/* 90 */
/***/ (function(module, exports) {

module.exports = {
  "content": {
    "flexDirection": "row",
    "flexFlow": "nowrap",
    "justifyContent": "space-between",
    "alignItems": "center",
    "paddingLeft": "30",
    "paddingRight": "30"
  },
  "title": {
    "fontSize": "34"
  }
}

/***/ }),
/* 91 */
/***/ (function(module, exports) {

module.exports = {
  "body": {
    "width": "750",
    "position": "absolute",
    "top": "200",
    "left": 0,
    "right": 0,
    "bottom": 0
  },
  "type": {
    "fontSize": "38",
    "color": "#636363",
    "fontWeight": "bold"
  },
  "content": {
    "position": "absolute",
    "top": "0",
    "left": 0,
    "right": 0,
    "bottom": 0,
    "paddingBottom": "180"
  },
  "netError": {
    "position": "absolute",
    "top": 102,
    "bottom": 0,
    "left": 0,
    "right": 0,
    "justifyContent": "center",
    "alignItems": "center"
  },
  "netError2": {
    "justifyContent": "center",
    "alignItems": "center"
  },
  "alreadyFollowList_album": {
    "width": "200",
    "height": "350",
    "marginLeft": "20",
    "marginTop": "37",
    "flexDirection": "column",
    "justifyContent": "flex-start",
    "alignItems": "center"
  },
  "alreadyFollowList_album_name": {
    "fontSize": "26",
    "marginTop": "26",
    "marginBottom": "12",
    "marginLeft": "12",
    "marginRight": "12",
    "color": "#505050"
  },
  "promptInformation": {
    "width": "750",
    "position": "absolute",
    "top": "0",
    "left": 0,
    "right": 0,
    "bottom": 0,
    "alignItems": "center",
    "justifyContent": "space-around"
  }
}

/***/ }),
/* 92 */
/***/ (function(module, exports) {

module.exports = {}

/***/ }),
/* 93 */
/***/ (function(module, exports) {

module.exports = {
  "suggestionList": {
    "height": "112",
    "flexDirection": "row",
    "justifyContent": "space-between",
    "alignItems": "center",
    "paddingLeft": "30",
    "paddingRight": "30",
    "backgroundColor": "#ffffff"
  },
  "album_image": {
    "width": "65.1",
    "height": "65.1",
    "borderRadius": "33",
    "marginRight": "15"
  },
  "albumInfo": {
    "width": "630",
    "height": "112",
    "flexDirection": "row",
    "justifyContent": "space-between",
    "alignItems": "center",
    "marginLeft": "10",
    "borderBottomWidth": "1",
    "borderBottomStyle": "solid",
    "borderBottomColor": "#e5e5e5"
  },
  "albumInfo_last": {
    "width": "630",
    "height": "112",
    "display": "flex",
    "flexDirection": "row",
    "justifyContent": "space-between",
    "alignItems": "center",
    "marginLeft": "10"
  },
  "albumInfoDetail": {
    "height": "100",
    "width": "430",
    "display": "flex",
    "flexDirection": "column",
    "justifyContent": "space-between",
    "alignItems": "flex-start",
    "paddingBottom": "10"
  },
  "article_read_recommend": {
    "color": "#B6B6B6",
    "fontSize": "21"
  },
  "album_NS": {
    "flexDirection": "row",
    "justifyContent": "flex-start",
    "alignItems": "center",
    "paddingTop": "5"
  },
  "album_subscribed": {
    "border": "1px solid grey",
    "borderWidth": "1",
    "borderStyle": "solid",
    "borderColor": "#D2D2D2",
    "borderRadius": "3",
    "paddingTop": "5",
    "paddingRight": "5",
    "paddingBottom": "5",
    "paddingLeft": "5",
    "marginLeft": "30"
  },
  "text_album_subscribed": {
    "fontSize": "18",
    "color": "#B6B6B6"
  },
  "text_album_name": {
    "fontSize": "29.4",
    "fontWeight": "bold",
    "color": "#545454",
    "paddingBottom": "8",
    "height": "40",
    "overflow": "hidden"
  },
  "album_des": {
    "color": "#888888",
    "fontSize": "21",
    "paddingTop": "8",
    "textOverflow": "ellipsis",
    "lines": 1
  },
  "subscribeBtn": {
    "width": "132",
    "height": "50",
    "backgroundColor": "#E15D53",
    "flexDirection": "row",
    "justifyContent": "space-around",
    "alignItems": "center",
    "marginRight": "30",
    "borderWidth": "1",
    "borderStyle": "solid",
    "borderColor": "#e5e5e5",
    "borderRadius": "15",
    "paddingLeft": "15",
    "paddingRight": "15"
  },
  "subscribeBtn-subscribed": {
    "width": "132",
    "height": "50",
    "backgroundColor": "#FFFFFF",
    "flexDirection": "row",
    "justifyContent": "space-around",
    "alignItems": "center",
    "marginRight": "30",
    "borderWidth": "1",
    "borderStyle": "solid",
    "borderColor": "#e5e5e5",
    "borderRadius": "15"
  },
  "text_subscribeBtn": {
    "color": "#FFFFFF",
    "fontSize": "24"
  },
  "text_subscribedBtn_": {
    "color": "#888888",
    "fontSize": "24"
  },
  "img_subscribeBtn": {
    "width": "24",
    "height": "24"
  }
}

/***/ }),
/* 94 */
/***/ (function(module, exports) {

module.exports = {
  "LoadingDots": {
    "position": "absolute",
    "top": "0",
    "left": 0,
    "right": 0,
    "bottom": 0,
    "justifyContent": "center",
    "alignItems": "center",
    "backgroundColor": "rgba(0,0,0,0.2)"
  },
  "loading": {
    "width": 750,
    "display": "flex",
    "MsFlexAlign": "center",
    "WebkitAlignItems": "center",
    "WebkitBoxAlign": "center",
    "alignItems": "center"
  },
  "indicator-text": {
    "color": "#E15D53",
    "fontSize": "42",
    "textAlign": "center"
  },
  "indicator": {
    "marginTop": "16",
    "height": "40",
    "width": "40",
    "color": "#E15D53"
  }
}

/***/ }),
/* 95 */
/***/ (function(module, exports) {

module.exports = {
  "netError": {
    "position": "absolute",
    "top": 300,
    "bottom": 300,
    "left": 0,
    "right": 0,
    "justifyContent": "center",
    "alignItems": "center"
  },
  "netError1": {
    "justifyContent": "center",
    "alignItems": "center"
  },
  "netError2": {
    "justifyContent": "center",
    "alignItems": "center"
  },
  "errorHint": {
    "fontSize": 30,
    "color": "#8f8f8f",
    "marginTop": "30"
  },
  "refresh": {
    "marginTop": "50",
    "fontSize": 35,
    "color": "#FFFFFF",
    "width": 550,
    "height": 80,
    "backgroundColor": "#E15D53",
    "textAlign": "center",
    "lineHeight": 80,
    "borderRadius": 30,
    "boxShadow": "0 15px 30px rgba(0, 0, 0, 0.2)"
  }
}

/***/ }),
/* 96 */
/***/ (function(module, exports) {

module.exports = {
  "body": {
    "position": "absolute",
    "left": 0,
    "top": "200",
    "right": 0,
    "bottom": 0,
    "width": "750"
  },
  "type": {
    "fontSize": "38",
    "color": "#636363",
    "fontWeight": "bold",
    "width": "160",
    "textAlign": "center"
  },
  "scroller": {
    "position": "absolute",
    "top": "0",
    "left": "0",
    "right": 0,
    "bottom": 0,
    "paddingBottom": "180"
  },
  "slider": {
    "height": "320",
    "width": "720",
    "marginLeft": "15",
    "marginTop": "20",
    "marginBottom": "20",
    "borderRadius": "10"
  },
  "image": {
    "width": "720",
    "height": "376"
  },
  "frame": {
    "width": "720",
    "height": "320",
    "position": "relative"
  },
  "indicator": {
    "position": "absolute",
    "top": "290",
    "left": "275",
    "width": "200",
    "height": "30"
  },
  "booklist": {
    "width": "690",
    "height": "250",
    "marginLeft": "30",
    "flexDirection": "row",
    "flexWrap": "nowrap",
    "justifyContent": "space-between",
    "alignItems": "center",
    "paddingTop": "40",
    "paddingBottom": "40",
    "borderBottomColor": "#e9e9e9",
    "borderBottomWidth": "2",
    "borderBottomStyle": "solid"
  },
  "mid": {
    "width": "460",
    "flexDirection": "column",
    "justifyContent": "center",
    "alignItems": "center",
    "marginLeft": "25"
  },
  "title": {
    "width": "460",
    "fontSize": "30",
    "fontWeight": "bold",
    "color": "#262626",
    "fontFamily": "\"Helvetica Neue\",sans-serif",
    "paddingRight": "20",
    "textAlign": "left",
    "marginBottom": "5"
  },
  "content": {
    "width": "460",
    "fontSize": "26",
    "color": "#757575",
    "lines": 2,
    "paddingRight": "20",
    "textAlign": "left",
    "textOverflow": "ellipsis",
    "marginTop": "5",
    "marginBottom": "5"
  },
  "browse": {
    "width": "460",
    "textAlign": "left",
    "fontSize": "26",
    "color": "#b6b6b6",
    "marginTop": "5"
  },
  "more": {
    "width": "30",
    "color": "#7e7e7e",
    "fontSize": "35",
    "textAlign": "right"
  },
  "promptInformation": {
    "width": "750",
    "position": "absolute",
    "top": "0",
    "left": 0,
    "right": 0,
    "bottom": 0,
    "alignItems": "center",
    "justifyContent": "space-around"
  }
}

/***/ }),
/* 97 */
/***/ (function(module, exports) {

module.exports = {
  "back": {
    "width": "40",
    "height": "90",
    "justifyContent": "center",
    "alignItems": "center"
  },
  "wxc-tab-page": {
    "height": "102",
    "width": "750",
    "flexDirection": "row",
    "justifyContent": "space-around",
    "alignItems": "center",
    "borderBottomStyle": "solid",
    "borderBottomWidth": "2",
    "borderBottomColor": "#f0f0f0"
  },
  "title-item": {
    "flexDirection": "row",
    "justifyContent": "center",
    "alignItems": "center",
    "borderBottomStyle": "solid",
    "borderBottomWidth": "3"
  },
  "tab-text": {
    "height": "102",
    "fontWeight": "bold",
    "lineHeight": "102"
  }
}

/***/ }),
/* 98 */
/***/ (function(module, exports) {

module.exports = {
  "LoadingGif": {
    "position": "absolute",
    "top": 300,
    "bottom": 300,
    "left": 0,
    "right": 0,
    "justifyContent": "center",
    "alignItems": "center"
  },
  "box": {
    "width": "50",
    "height": "50",
    "borderTopWidth": "6",
    "borderTopLeftRadius": "25",
    "borderTopColor": "#E15D53",
    "borderLeftWidth": "5",
    "borderLeftColor": "#E15D53",
    "borderRadius": "25"
  }
}

/***/ }),
/* 99 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: ["body"]
  }, [_c('list', {
    staticClass: ["list"]
  }, [_c('cell', {
    appendAsTree: true,
    attrs: {
      "append": "tree"
    }
  }, [_c('div', {
    staticClass: ["messagebox"]
  }, [_c('text', {
    staticClass: ["mes"]
  }, [_vm._v(_vm._s(_vm.message))])])]), _vm._l((_vm.tags), function(tag, index) {
    return (_vm.netStatus == 2) ? _c('cell', {
      key: index,
      appendAsTree: true,
      attrs: {
        "append": "tree"
      }
    }, [_c('div', {
      staticClass: ["category_box"]
    }, [_c('div', {
      staticClass: ["category_box_top"]
    }, [_c('text', {
      staticClass: ["category_title"]
    }, [_vm._v(_vm._s(tag.name))]), _c('div', {
      staticClass: ["more"],
      on: {
        "click": function($event) {
          _vm.jumpToCategoryDetail(tag.id, tag.name)
        }
      }
    }, [_c('text', {
      staticStyle: {
        fontSize: "26px",
        color: "#757575",
        marginRight: "30px"
      }
    }, [_vm._v("更多 >")])])]), _c('scroller', {
      staticClass: ["books"],
      attrs: {
        "loadmoreoffset": "10",
        "scrollDirection": "horizontal"
      }
    }, _vm._l((_vm.books), function(item, index) {
      return (item.tag_id == tag.id) ? _c('div', {
        key: index,
        staticClass: ["book"]
      }, [_c('image', {
        staticClass: ["bookimage"],
        attrs: {
          "src": item.img_url,
          "placeholder": _vm.getImgPath('chat.png')
        },
        on: {
          "click": function($event) {
            _vm.jumpToBookDetail(item.id)
          }
        }
      }), _c('text', {
        staticClass: ["booktitle"]
      }, [_vm._v(_vm._s(item.title))])]) : _vm._e()
    }))])]) : _vm._e()
  }), _c('cell', {
    staticClass: ["bottom-white"],
    appendAsTree: true,
    attrs: {
      "append": "tree"
    }
  })], 2), (_vm.netStatus == 0) ? _c('loadingCircle') : _vm._e(), (_vm.netStatus == 1) ? _c('netError', {
    attrs: {
      "errorType": _vm.errorType,
      "errorMsg": _vm.errorMsg
    },
    on: {
      "toNetError": _vm.refresh
    }
  }) : _vm._e()], 1)
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ }),
/* 100 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('video', {
    staticClass: ["video"],
    attrs: {
      "id": "videPlay",
      "src": _vm.src,
      "autoplay": "",
      "controls": ""
    }
  })
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ }),
/* 101 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: ["body"],
    style: {
      height: _vm.height
    }
  })
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ }),
/* 102 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('loading-indicator', {
    staticStyle: {
      height: "80px",
      width: "80px",
      color: "#7ec9c2"
    }
  })
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ }),
/* 103 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [(_vm.loadFinished) ? _c('image', {
    staticStyle: {
      width: "160px",
      height: "220px",
      borderRadius: "5px"
    },
    attrs: {
      "src": _vm.source
    },
    on: {
      "load": _vm.onImageLoad
    }
  }) : _vm._e(), (_vm.loadFinished == false) ? _c('image', {
    staticStyle: {
      width: "160px",
      height: "220px",
      borderRadius: "5px"
    },
    attrs: {
      "src": _vm.backgroup
    }
  }) : _vm._e()])
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ }),
/* 104 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [(_vm.show) ? _c('div', {
    ref: "wxc-overlay",
    staticClass: ["wxc-overlay"],
    style: _vm.overlayStyle,
    attrs: {
      "hack": _vm.shouldShow
    },
    on: {
      "click": _vm.overlayClicked
    }
  }) : _vm._e()])
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ }),
/* 105 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: ["body"]
  }, [_c('div', {
    staticClass: ["content_category"]
  }, [_c('div', {
    staticClass: ["left"],
    staticStyle: {
      width: "200px"
    }
  }, _vm._l((_vm.tags), function(item, index) {
    return _c('div', {
      key: index,
      on: {
        "click": function($event) {
          _vm.getBooksByCategory(index, item.id)
        }
      }
    }, [(item.id != 300) ? _c('text', {
      class: [index == _vm.Id ? 'category-selected' : 'category']
    }, [_vm._v(_vm._s(item.name))]) : _vm._e(), (item.id != 300) ? _c('text', {
      class: [index == _vm.Id ? 'count-selected' : 'count']
    }, [_vm._v(_vm._s(item.total) + "本")]) : _vm._e()])
  })), _c('waterfall', {
    ref: "waterfall",
    staticClass: ["waterfall"],
    staticStyle: {
      padding: "0",
      width: "550px",
      marginTop: "20px"
    },
    attrs: {
      "loadmoreoffset": "3000",
      "columnWidth": "auto",
      "columnCount": "2",
      "columnGap": "12",
      "showScrollbar": "false",
      "scrollable": "true"
    },
    on: {
      "loadmore": function($event) {
        _vm.getMoreInfo()
      }
    }
  }, _vm._l((_vm.books), function(item, index) {
    return _c('cell', {
      key: index,
      staticClass: ["book"],
      appendAsTree: true,
      attrs: {
        "append": "tree"
      }
    }, [_c('image', {
      staticStyle: {
        width: "160px",
        height: "220px",
        borderRadius: "5px"
      },
      attrs: {
        "src": item.img_url,
        "placeholder": _vm.getImgPath('chat.png')
      },
      on: {
        "click": function($event) {
          _vm.jumpToBookProfile(item.id)
        }
      }
    }), _c('text', {
      staticClass: ["bookname"]
    }, [_vm._v(_vm._s(item.title))])])
  }))], 1), (_vm.netStatus == 0) ? _c('loadingCircle') : _vm._e(), (_vm.netStatus == 1) ? _c('netError', {
    attrs: {
      "errorType": _vm.errorType,
      "errorMsg": _vm.errorMsg
    },
    on: {
      "toNetError": _vm.refresh
    }
  }) : _vm._e()], 1)
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ }),
/* 106 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: ["wx-tabbar"]
  }, [_c('div', {
    staticClass: ["tab-component"],
    style: {
      'transform': _vm.translateX,
      width: _vm.totalWidth + 'px'
    }
  }, [_vm._t("default")], 2), _c('div', {
    staticClass: ["tabbar"],
    style: _vm.getStyles()
  }, _vm._l((_vm.tabItems), function(item, index) {
    return _c('div', {
      key: index,
      staticClass: ["tabbar-item"],
      on: {
        "click": function($event) {
          _vm.changeTab(item, index)
        }
      }
    }, [_c('crIconFont', {
      attrs: {
        "name": item.iconFont,
        "size": "40",
        "color": _vm.selectedTab === item.index ? item.selectedColor : item.titleColor
      },
      on: {
        "click": function($event) {
          _vm.changeTab(item, index)
        }
      }
    }), _c('text', {
      staticClass: ["wx-text"],
      style: _vm.getTitleStyle(item)
    }, [_vm._v(_vm._s(item.title))])], 1)
  }))])
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ }),
/* 107 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: ["wrapper"]
  }, [_c('div', {
    staticClass: ["content"],
    style: {
      color: _vm.bgcolor,
      height: _vm.height,
      backgroundColor: _vm.bgcolor
    }
  }, [(_vm.secondaryPage) ? _c('crIconFont', {
    attrs: {
      "name": "ion-android-arrow-back",
      "size": "50px"
    },
    on: {
      "click": function($event) {
        _vm.goBack()
      }
    }
  }) : _c('crIconFont', {
    attrs: {
      "name": _vm.app,
      "size": "50px"
    },
    on: {
      "click": function($event) {
        _vm.jumpToMySubscribe()
      }
    }
  }), _c('text', {
    staticClass: ["title"],
    style: {
      color: _vm.textColor
    }
  }, [_vm._v(_vm._s(_vm.name))]), _c('crIconFont', {
    attrs: {
      "name": _vm.search,
      "size": "50px"
    },
    on: {
      "click": function($event) {
        _vm.jumpToSearch()
      }
    }
  })], 1)])
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ }),
/* 108 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: ["wrapper"]
  }, [_c('div', {
    staticClass: ["top"]
  }, [_c('crIconFont', {
    attrs: {
      "name": "ion-ios-book-outline",
      "size": "50px",
      "color": "#ffffff"
    }
  }), _c('text', {
    staticStyle: {
      color: "#ffffff",
      fontFamily: "Roboto",
      fontSize: "34px"
    }
  }, [_vm._v("图书")]), _c('crIconFont', {
    attrs: {
      "name": "ion-ios-search",
      "size": "50px",
      "color": "#ffffff"
    },
    on: {
      "click": function($event) {
        _vm.jumpToBookSearch()
      }
    }
  })], 1), _c('div', {
    staticClass: ["tab-bar"]
  }, _vm._l((_vm.tabItems), function(item, index) {
    return _c('text', {
      key: index,
      staticClass: ["tab-item"],
      style: {
        color: item.index == _vm.currentTab ? '#E15D53' : '#B6B6B6'
      },
      on: {
        "click": function($event) {
          _vm.changeTab(index)
        }
      }
    }, [_vm._v(_vm._s(item.title))])
  })), _c('div', {
    staticClass: ["tab-bar-border"],
    style: {
      left: _vm.left + 'px'
    }
  }), (_vm.currentTab == 0) ? _c('storeEntry') : _vm._e(), (_vm.currentTab == 1) ? _c('libraryCategory') : _vm._e(), (_vm.currentTab == 2) ? _c('libraryBookLists') : _vm._e(), (_vm.currentTab == 3) ? _c('libraryBookShelf') : _vm._e(), (_vm.netStatus == 1) ? _c('netError', {
    attrs: {
      "errorType": _vm.errorType,
      "errorMsg": _vm.errorMsg
    }
  }) : _vm._e()], 1)
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ }),
/* 109 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: ["topbar"],
    style: {
      backgroundColor: _vm.bgcolor
    }
  }, [_c('div', {
    staticClass: ["back"],
    on: {
      "click": _vm.goBack
    }
  }, [_c('crIconFont', {
    attrs: {
      "name": "ion-android-arrow-back",
      "size": "50px",
      "color": "#ffffff"
    },
    on: {
      "click": function($event) {
        _vm.goBack()
      }
    }
  })], 1), _c('text', {
    staticStyle: {
      color: "#ffffff",
      fontFamily: "Roboto",
      fontSize: "34px"
    }
  }, [_vm._v(_vm._s(_vm.topbarname))]), _c('text', {
    staticClass: ["right-part"],
    style: {
      color: _vm.bgcolor
    }
  }, [_vm._v(".")])])
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ }),
/* 110 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: ["container"]
  }, [(_vm.show) ? _c('div', {
    staticClass: ["dialog-box"],
    style: {
      top: _vm.top + 'px'
    }
  }, [_c('div', {
    staticClass: ["dialog-content"]
  }, [_vm._t("title", [_c('text', {
    staticClass: ["content-title"]
  }, [_vm._v(_vm._s(_vm.title))])]), _vm._t("content", [_c('text', {
    staticClass: ["content-subtext"]
  }, [_vm._v(_vm._s(_vm.content))])])], 2), _c('div', {
    staticClass: ["dialog-footer"]
  }, [(!_vm.single) ? _c('div', {
    staticClass: ["footer-btn", "cancel"],
    on: {
      "click": _vm.secondaryClicked
    }
  }, [_c('text', {
    staticClass: ["btn-text"],
    style: {
      color: _vm.secondBtnColor
    }
  }, [_vm._v(_vm._s(_vm.cancelText))])]) : _vm._e(), _c('div', {
    staticClass: ["footer-btn", "confirm"],
    on: {
      "click": _vm.primaryClicked
    }
  }, [_c('text', {
    staticClass: ["btn-text"],
    style: {
      color: _vm.mainBtnColor
    }
  }, [_vm._v(_vm._s(_vm.confirmText))])])])]) : _vm._e()])
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ }),
/* 111 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('web', {
    attrs: {
      "src": _vm.src
    }
  })
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ }),
/* 112 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: ["netDivError"]
  }, [_c('text', {
    staticClass: ["netTextError"],
    attrs: {
      "lines": "2"
    }
  }, [_vm._v(_vm._s(_vm.global.display('networkError')))]), _c('text', {
    staticClass: ["button"],
    on: {
      "click": _vm.Refresh
    }
  }, [_vm._v(_vm._s(_vm.global.display('refresh')))])])
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ }),
/* 113 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [(_vm.mod === 'default') ? _c('div', {
    class: ['wxc-search-bar', 'wxc-search-bar-' + _vm.theme],
    style: _vm.barStyle
  }, [_c('crIconFont', {
    staticClass: ["search-bar-icon"],
    attrs: {
      "name": "ion-android-arrow-back",
      "size": "50px",
      "color": "#101010"
    },
    on: {
      "click": function($event) {
        _vm.goBack()
      }
    }
  }), _c('input', {
    ref: "search-input",
    class: ['search-bar-input', 'search-bar-input-' + _vm.theme],
    style: {
      width: _vm.needShowCancel ? '594px' : '670px'
    },
    attrs: {
      "autofocus": _vm.autofocus,
      "disabled": _vm.disabled,
      "value": _vm.value,
      "type": _vm.inputType,
      "placeholder": _vm.placeholder
    },
    on: {
      "blur": _vm.onBlur,
      "focus": _vm.onFocus,
      "input": _vm.onInput
    }
  }), (_vm.disabled) ? _c('div', {
    staticClass: ["disabled-input"],
    on: {
      "click": _vm.inputDisabledClicked
    }
  }) : _vm._e(), (_vm.showClose) ? _c('image', {
    staticClass: ["search-bar-close"],
    attrs: {
      "ariaHidden": true,
      "src": _vm.getImgPath('close.png')
    },
    on: {
      "click": _vm.closeClicked
    }
  }) : _vm._e(), _c('crIconFont', {
    staticClass: ["search-bar-button"],
    attrs: {
      "name": "ion-search",
      "size": "50px",
      "color": "#101010"
    },
    on: {
      "click": _vm.searchClicked
    }
  })], 1) : _vm._e()])
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ }),
/* 114 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: ["icon-block"],
    style: {
      width: _vm.size,
      height: _vm.size
    },
    on: {
      "click": function($event) {
        _vm._click($event)
      }
    }
  }, [_c('text', {
    staticClass: ["icon"],
    style: _vm.getStyle,
    attrs: {
      "value": _vm.getFontName
    },
    on: {
      "click": function($event) {
        _vm._click($event)
      }
    }
  })])
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ }),
/* 115 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: ["wxc-progress"],
    style: _vm.runWayStyle,
    attrs: {
      "accessible": true,
      "ariaLabel": ("进度为百分之" + _vm.value)
    }
  }, [_c('div', {
    staticClass: ["progress"],
    style: _vm.progressStyle
  })])
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ }),
/* 116 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: ["wrapper"]
  }, [_c('div', {
    staticClass: ["content"],
    style: {
      color: _vm.bgcolor,
      height: _vm.height,
      backgroundColor: _vm.bgcolor
    }
  }, [_c('crIconFont', {
    attrs: {
      "name": "ion-android-arrow-back",
      "size": "50px"
    },
    on: {
      "click": function($event) {
        _vm.goBack()
      }
    }
  }), _c('text', {
    staticClass: ["title"],
    style: {
      color: _vm.textColor
    }
  }, [_vm._v(_vm._s(_vm.name))]), _c('crIconFont', {
    attrs: {
      "name": "ion-android-share-alt",
      "size": "40px",
      "color": _vm.shareColor
    },
    on: {
      "click": function($event) {
        _vm.share(_vm.albumInfo)
      }
    }
  })], 1), _c('commonFun', {
    ref: "commonFun"
  })], 1)
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ }),
/* 117 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: ["body"]
  }, [(_vm.netStatus == 1) ? _c('netError', {
    attrs: {
      "errorType": _vm.errorType,
      "errorMsg": _vm.errorMsg
    },
    on: {
      "toNetError": _vm.refresh
    }
  }) : _vm._e(), _c('waterfall', {
    ref: "waterfall",
    staticClass: ["content"],
    attrs: {
      "loadmoreoffset": "3000",
      "columnWidth": "auto",
      "columnCount": "3",
      "columnGap": "5px",
      "showScrollbar": "false",
      "scrollable": "true"
    }
  }, _vm._l((_vm.books), function(item, index) {
    return _c('cell', {
      key: index,
      appendAsTree: true,
      attrs: {
        "append": "tree"
      }
    }, [_c('div', {
      key: index,
      staticClass: ["alreadyFollowList_album"],
      on: {
        "click": function($event) {
          _vm.jumpToBookProfile(item.id, item.title, item.address, item.img_url)
        },
        "longpress": function($event) {
          _vm.openPopUp(item.id, index)
        }
      }
    }, [_c('image', {
      staticStyle: {
        width: "160px",
        height: "220px",
        borderRadius: "5px"
      },
      attrs: {
        "src": item.img_url,
        "placeholder": _vm.getImgPath('cover_default_new.png')
      }
    }), (item.progress.visible == false) ? _c('text', {
      staticClass: ["alreadyFollowList_album_name"]
    }, [_vm._v(_vm._s(item.title))]) : _vm._e(), (item.progress.visible) ? _c('progress', {
      staticStyle: {
        marginTop: "20px"
      },
      attrs: {
        "value": item.progress.value,
        "barWidth": 200,
        "barHeight": 20
      }
    }) : _vm._e()], 1)])
  })), (_vm.netStatus == 0) ? _c('loadingCircle') : _vm._e(), (_vm.show) ? _c('weexOverlay', {
    attrs: {
      "show": true,
      "hasAnimation": false
    }
  }) : _vm._e(), _c('weexDialogue', {
    attrs: {
      "title": _vm.title,
      "content": _vm.content,
      "confirmText": _vm.confirmText,
      "cancelText": _vm.cancelText,
      "show": _vm.show,
      "single": _vm.single
    },
    on: {
      "wxcDialogCancelBtnClicked": _vm.dialogCancelBtnClick,
      "wxcDialogConfirmBtnClicked": function($event) {
        _vm.dialogConfirmBtnClick()
      }
    }
  })], 1)
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ }),
/* 118 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticStyle: {
      display: "none"
    }
  })
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ }),
/* 119 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: ["wrapper"]
  }, _vm._l((_vm.albums), function(item, index) {
    return _c('div', {
      staticClass: ["suggestionList"]
    }, [_c('image', {
      staticClass: ["album_image"],
      attrs: {
        "src": item.img_url
      },
      on: {
        "click": function($event) {
          _vm.jumpToAuthorColumn(item.id, item.status)
        }
      }
    }), _c('div', {
      class: [_vm.Id === index + 1 ? 'albumInfo_last' : 'albumInfo']
    }, [_c('div', {
      staticClass: ["albumInfoDetail"],
      on: {
        "click": function($event) {
          _vm.jumpToAuthorColumn(item.id, item.status)
        }
      }
    }, [_c('div', {
      staticClass: ["album_NS"]
    }, [_c('div', {
      staticClass: ["album_name"]
    }, [_c('text', {
      staticClass: ["text_album_name"],
      staticStyle: {
        fontFamily: "Roboto"
      }
    }, [_vm._v(_vm._s(item.name))])]), _c('div', {
      staticClass: ["album_subscribed"]
    }, [_c('text', {
      staticClass: ["text_album_subscribed"],
      staticStyle: {
        fontFamily: "Roboto"
      }
    }, [_vm._v(_vm._s(item.follow_count) + "人订阅")])])]), _c('div', [_c('text', {
      staticClass: ["album_des"],
      staticStyle: {
        fontFamily: "Roboto"
      }
    }, [_vm._v(_vm._s(item.author))])])]), (item.status == 1) ? _c('div', {
      staticClass: ["subscribeBtn-subscribed"],
      on: {
        "click": function($event) {
          _vm.toggleSubscribe(item.id, index)
        }
      }
    }, [_c('image', {
      staticClass: ["img_subscribeBtn"],
      attrs: {
        "src": _vm.getImgPath('followok.png')
      }
    }), _c('text', {
      staticClass: ["text_subscribedBtn_"],
      staticStyle: {
        fontFamily: "Roboto"
      }
    }, [_vm._v("已订阅")])]) : _c('div', {
      staticClass: ["subscribeBtn"],
      on: {
        "click": function($event) {
          _vm.toggleSubscribe(item.id, index)
        }
      }
    }, [_c('crIconFont', {
      attrs: {
        "name": "ion-android-add-circle",
        "size": "30px"
      }
    }), _c('text', {
      staticClass: ["text_subscribeBtn"],
      staticStyle: {
        fontFamily: "Roboto"
      }
    }, [_vm._v("订阅")])], 1)])])
  }))
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ }),
/* 120 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: ["LoadingDots"]
  }, [_c('div', {
    staticStyle: {
      backgroundColor: "rgba(255,255,255,0.3)",
      height: "300px",
      width: "300px",
      justifyContent: "center",
      alignItems: "center",
      borderRadius: "10px"
    }
  }, [_c('image', {
    staticStyle: {
      width: "80px",
      height: "80px"
    },
    attrs: {
      "src": _vm.getImgPath('load.png')
    }
  }), _c('text', {
    staticStyle: {
      fontSize: "30px",
      color: "#8f8f8f",
      marginTop: "20px"
    }
  }, [_vm._v("提交中...")])])])
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ }),
/* 121 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: ["netError"]
  }, [(_vm.errorType == _vm.neterror) ? _c('div', {
    staticClass: ["netError1"]
  }, [_c('image', {
    staticStyle: {
      width: "100px",
      height: "100px"
    },
    attrs: {
      "src": _vm.getImgPath('neterror.png')
    }
  }), _c('text', {
    staticClass: ["errorHint"]
  }, [_vm._v("网络不给力，请稍后重试")]), _c('text', {
    staticClass: ["refresh"],
    on: {
      "click": _vm.Refresh
    }
  }, [_vm._v("刷新")])]) : _vm._e(), (_vm.errorType == _vm.noResult) ? _c('div', {
    staticClass: ["netError2"]
  }, [_c('image', {
    staticStyle: {
      width: "100px",
      height: "100px"
    },
    attrs: {
      "src": _vm.getImgPath('noresult.png')
    }
  }), _c('text', {
    staticStyle: {
      fontSize: "30px",
      color: "#8f8f8f",
      marginTop: "50px"
    }
  }, [_vm._v(_vm._s(_vm.errorMsg) + "...")])]) : _vm._e()])
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ }),
/* 122 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: ["body"]
  }, [(_vm.netStatus == 2) ? _c('waterfall', {
    ref: "waterfall",
    staticClass: ["scroller"],
    staticStyle: {
      padding: "0"
    },
    attrs: {
      "columnWidth": "auto",
      "columnCount": 1,
      "columnGap": 12,
      "showScrollbar": false,
      "scrollable": "true"
    }
  }, _vm._l((_vm.bookLists), function(item, index) {
    return _c('cell', {
      key: index,
      staticStyle: {
        marginTop: "30px"
      },
      appendAsTree: true,
      attrs: {
        "append": "tree"
      }
    }, [_c('div', {
      staticClass: ["booklist"],
      on: {
        "click": function($event) {
          _vm.goToBookList(item.id)
        }
      }
    }, [_c('image', {
      staticStyle: {
        width: "168px",
        height: "168px",
        borderRadius: "5px"
      },
      attrs: {
        "src": item.img_url
      }
    }), _c('div', {
      staticClass: ["mid"]
    }, [_c('text', {
      staticClass: ["title"]
    }, [_vm._v(_vm._s(item.name))]), _c('text', {
      staticClass: ["content"]
    }, [_vm._v(_vm._s(item.content))]), _c('text', {
      staticClass: ["browse"]
    }, [_vm._v("共" + _vm._s(item.total) + "本  " + _vm._s(item.browse) + "人看过")])]), _c('text', {
      staticClass: ["more"]
    }, [_vm._v(">")])])])
  })) : _vm._e(), (_vm.netStatus == 0) ? _c('loadingCircle') : _vm._e(), (_vm.netStatus == 1) ? _c('netError', {
    attrs: {
      "errorType": _vm.errorType,
      "errorMsg": _vm.errorMsg
    },
    on: {
      "toNetError": _vm.refresh
    }
  }) : _vm._e()], 1)
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ }),
/* 123 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: ["wxc-tab-page"]
  }, [(_vm.isShelf) ? _c('div', {
    staticClass: ["back"],
    on: {
      "click": _vm.back
    }
  }, [(_vm.isShelf) ? _c('image', {
    staticStyle: {
      width: "90px",
      height: "90px",
      borderRadius: "5px"
    },
    attrs: {
      "src": _vm.getImgPath('backarrow.png')
    },
    on: {
      "click": _vm.back
    }
  }) : _vm._e()]) : _vm._e(), _vm._l((_vm.topNav), function(item, index) {
    return _c('div', {
      key: index,
      staticClass: ["title-item"],
      style: {
        borderBottomColor: _vm.currentPosition == index ? _vm.tabStyles.activeBorderBottomColor : 'rgba(0, 0, 0, 0)'
      },
      on: {
        "click": function($event) {
          _vm.setPage(index)
        }
      }
    }, [_c('text', {
      staticClass: ["tab-text"],
      style: {
        fontSize: _vm.tabStyles.fontSize + 'px',
        fontWeight: (_vm.currentPosition == index && _vm.tabStyles.isActiveTitleBold) ? 'bold' : 'bold',
        color: _vm.currentPosition == index ? _vm.tabStyles.activeTitleColor : _vm.tabStyles.titleColor,
        paddingLeft: _vm.tabStyles.textPaddingLeft + 'px',
        paddingRight: _vm.tabStyles.textPaddingRight + 'px'
      }
    }, [_vm._v(_vm._s(item))])])
  }), _c('div', {
    staticClass: ["back"],
    on: {
      "click": function($event) {
        _vm.setPage(4)
      }
    }
  }, [_c('image', {
    staticStyle: {
      width: "35px",
      height: "35px",
      borderRadius: "5px"
    },
    attrs: {
      "src": _vm.getImgPath('search.png')
    }
  })])], 2)
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ }),
/* 124 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: ["LoadingGif"]
  }, [_c('image', {
    staticStyle: {
      width: "60px",
      height: "60px"
    },
    attrs: {
      "src": _vm.getImgPath('loading.gif')
    }
  })])
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ }),
/* 125 */
/***/ (function(module, exports) {

module.exports = {"ion-alert":"&#xf101;","ion-alert-circled":"&#xf100;","ion-android-add":"&#xf2c7;","ion-android-add-circle":"&#xf359;","ion-android-alarm-clock":"&#xf35a;","ion-android-alert":"&#xf35b;","ion-android-apps":"&#xf35c;","ion-android-archive":"&#xf2c9;","ion-android-arrow-back":"&#xf2ca;","ion-android-arrow-down":"&#xf35d;","ion-android-arrow-dropdown":"&#xf35f;","ion-android-arrow-dropdown-circle":"&#xf35e;","ion-android-arrow-dropleft":"&#xf361;","ion-android-arrow-dropleft-circle":"&#xf360;","ion-android-arrow-dropright":"&#xf363;","ion-android-arrow-dropright-circle":"&#xf362;","ion-android-arrow-dropup":"&#xf365;","ion-android-arrow-dropup-circle":"&#xf364;","ion-android-arrow-forward":"&#xf30f;","ion-android-arrow-up":"&#xf366;","ion-android-attach":"&#xf367;","ion-android-bar":"&#xf368;","ion-android-bicycle":"&#xf369;","ion-android-boat":"&#xf36a;","ion-android-bookmark":"&#xf36b;","ion-android-bulb":"&#xf36c;","ion-android-bus":"&#xf36d;","ion-android-calendar":"&#xf2d1;","ion-android-call":"&#xf2d2;","ion-android-camera":"&#xf2d3;","ion-android-cancel":"&#xf36e;","ion-android-car":"&#xf36f;","ion-android-cart":"&#xf370;","ion-android-chat":"&#xf2d4;","ion-android-checkbox":"&#xf374;","ion-android-checkbox-blank":"&#xf371;","ion-android-checkbox-outline":"&#xf373;","ion-android-checkbox-outline-blank":"&#xf372;","ion-android-checkmark-circle":"&#xf375;","ion-android-clipboard":"&#xf376;","ion-android-close":"&#xf2d7;","ion-android-cloud":"&#xf37a;","ion-android-cloud-circle":"&#xf377;","ion-android-cloud-done":"&#xf378;","ion-android-cloud-outline":"&#xf379;","ion-android-color-palette":"&#xf37b;","ion-android-compass":"&#xf37c;","ion-android-contact":"&#xf2d8;","ion-android-contacts":"&#xf2d9;","ion-android-contract":"&#xf37d;","ion-android-create":"&#xf37e;","ion-android-delete":"&#xf37f;","ion-android-desktop":"&#xf380;","ion-android-document":"&#xf381;","ion-android-done":"&#xf383;","ion-android-done-all":"&#xf382;","ion-android-download":"&#xf2dd;","ion-android-drafts":"&#xf384;","ion-android-exit":"&#xf385;","ion-android-expand":"&#xf386;","ion-android-favorite":"&#xf388;","ion-android-favorite-outline":"&#xf387;","ion-android-film":"&#xf389;","ion-android-folder":"&#xf2e0;","ion-android-folder-open":"&#xf38a;","ion-android-funnel":"&#xf38b;","ion-android-globe":"&#xf38c;","ion-android-hand":"&#xf2e3;","ion-android-hangout":"&#xf38d;","ion-android-happy":"&#xf38e;","ion-android-home":"&#xf38f;","ion-android-image":"&#xf2e4;","ion-android-laptop":"&#xf390;","ion-android-list":"&#xf391;","ion-android-locate":"&#xf2e9;","ion-android-lock":"&#xf392;","ion-android-mail":"&#xf2eb;","ion-android-map":"&#xf393;","ion-android-menu":"&#xf394;","ion-android-microphone":"&#xf2ec;","ion-android-microphone-off":"&#xf395;","ion-android-more-horizontal":"&#xf396;","ion-android-more-vertical":"&#xf397;","ion-android-navigate":"&#xf398;","ion-android-notifications":"&#xf39b;","ion-android-notifications-none":"&#xf399;","ion-android-notifications-off":"&#xf39a;","ion-android-open":"&#xf39c;","ion-android-options":"&#xf39d;","ion-android-people":"&#xf39e;","ion-android-person":"&#xf3a0;","ion-android-person-add":"&#xf39f;","ion-android-phone-landscape":"&#xf3a1;","ion-android-phone-portrait":"&#xf3a2;","ion-android-pin":"&#xf3a3;","ion-android-plane":"&#xf3a4;","ion-android-playstore":"&#xf2f0;","ion-android-print":"&#xf3a5;","ion-android-radio-button-off":"&#xf3a6;","ion-android-radio-button-on":"&#xf3a7;","ion-android-refresh":"&#xf3a8;","ion-android-remove":"&#xf2f4;","ion-android-remove-circle":"&#xf3a9;","ion-android-restaurant":"&#xf3aa;","ion-android-sad":"&#xf3ab;","ion-android-search":"&#xf2f5;","ion-android-send":"&#xf2f6;","ion-android-settings":"&#xf2f7;","ion-android-share":"&#xf2f8;","ion-android-share-alt":"&#xf3ac;","ion-android-star":"&#xf2fc;","ion-android-star-half":"&#xf3ad;","ion-android-star-outline":"&#xf3ae;","ion-android-stopwatch":"&#xf2fd;","ion-android-subway":"&#xf3af;","ion-android-sunny":"&#xf3b0;","ion-android-sync":"&#xf3b1;","ion-android-textsms":"&#xf3b2;","ion-android-time":"&#xf3b3;","ion-android-train":"&#xf3b4;","ion-android-unlock":"&#xf3b5;","ion-android-upload":"&#xf3b6;","ion-android-volume-down":"&#xf3b7;","ion-android-volume-mute":"&#xf3b8;","ion-android-volume-off":"&#xf3b9;","ion-android-volume-up":"&#xf3ba;","ion-android-walk":"&#xf3bb;","ion-android-warning":"&#xf3bc;","ion-android-watch":"&#xf3bd;","ion-android-wifi":"&#xf305;","ion-aperture":"&#xf313;","ion-archive":"&#xf102;","ion-arrow-down-a":"&#xf103;","ion-arrow-down-b":"&#xf104;","ion-arrow-down-c":"&#xf105;","ion-arrow-expand":"&#xf25e;","ion-arrow-graph-down-left":"&#xf25f;","ion-arrow-graph-down-right":"&#xf260;","ion-arrow-graph-up-left":"&#xf261;","ion-arrow-graph-up-right":"&#xf262;","ion-arrow-left-a":"&#xf106;","ion-arrow-left-b":"&#xf107;","ion-arrow-left-c":"&#xf108;","ion-arrow-move":"&#xf263;","ion-arrow-resize":"&#xf264;","ion-arrow-return-left":"&#xf265;","ion-arrow-return-right":"&#xf266;","ion-arrow-right-a":"&#xf109;","ion-arrow-right-b":"&#xf10a;","ion-arrow-right-c":"&#xf10b;","ion-arrow-shrink":"&#xf267;","ion-arrow-swap":"&#xf268;","ion-arrow-up-a":"&#xf10c;","ion-arrow-up-b":"&#xf10d;","ion-arrow-up-c":"&#xf10e;","ion-asterisk":"&#xf314;","ion-at":"&#xf10f;","ion-backspace":"&#xf3bf;","ion-backspace-outline":"&#xf3be;","ion-bag":"&#xf110;","ion-battery-charging":"&#xf111;","ion-battery-empty":"&#xf112;","ion-battery-full":"&#xf113;","ion-battery-half":"&#xf114;","ion-battery-low":"&#xf115;","ion-beaker":"&#xf269;","ion-beer":"&#xf26a;","ion-bluetooth":"&#xf116;","ion-bonfire":"&#xf315;","ion-bookmark":"&#xf26b;","ion-bowtie":"&#xf3c0;","ion-briefcase":"&#xf26c;","ion-bug":"&#xf2be;","ion-calculator":"&#xf26d;","ion-calendar":"&#xf117;","ion-camera":"&#xf118;","ion-card":"&#xf119;","ion-cash":"&#xf316;","ion-chatbox":"&#xf11b;","ion-chatbox-working":"&#xf11a;","ion-chatboxes":"&#xf11c;","ion-chatbubble":"&#xf11e;","ion-chatbubble-working":"&#xf11d;","ion-chatbubbles":"&#xf11f;","ion-checkmark":"&#xf122;","ion-checkmark-circled":"&#xf120;","ion-checkmark-round":"&#xf121;","ion-chevron-down":"&#xf123;","ion-chevron-left":"&#xf124;","ion-chevron-right":"&#xf125;","ion-chevron-up":"&#xf126;","ion-clipboard":"&#xf127;","ion-clock":"&#xf26e;","ion-close":"&#xf12a;","ion-close-circled":"&#xf128;","ion-close-round":"&#xf129;","ion-closed-captioning":"&#xf317;","ion-cloud":"&#xf12b;","ion-code":"&#xf271;","ion-code-download":"&#xf26f;","ion-code-working":"&#xf270;","ion-coffee":"&#xf272;","ion-compass":"&#xf273;","ion-compose":"&#xf12c;","ion-connection-bars":"&#xf274;","ion-contrast":"&#xf275;","ion-crop":"&#xf3c1;","ion-cube":"&#xf318;","ion-disc":"&#xf12d;","ion-document":"&#xf12f;","ion-document-text":"&#xf12e;","ion-drag":"&#xf130;","ion-earth":"&#xf276;","ion-easel":"&#xf3c2;","ion-edit":"&#xf2bf;","ion-egg":"&#xf277;","ion-eject":"&#xf131;","ion-email":"&#xf132;","ion-email-unread":"&#xf3c3;","ion-erlenmeyer-flask":"&#xf3c5;","ion-erlenmeyer-flask-bubbles":"&#xf3c4;","ion-eye":"&#xf133;","ion-eye-disabled":"&#xf306;","ion-female":"&#xf278;","ion-filing":"&#xf134;","ion-film-marker":"&#xf135;","ion-fireball":"&#xf319;","ion-flag":"&#xf279;","ion-flame":"&#xf31a;","ion-flash":"&#xf137;","ion-flash-off":"&#xf136;","ion-folder":"&#xf139;","ion-fork":"&#xf27a;","ion-fork-repo":"&#xf2c0;","ion-forward":"&#xf13a;","ion-funnel":"&#xf31b;","ion-gear-a":"&#xf13d;","ion-gear-b":"&#xf13e;","ion-grid":"&#xf13f;","ion-hammer":"&#xf27b;","ion-happy":"&#xf31c;","ion-happy-outline":"&#xf3c6;","ion-headphone":"&#xf140;","ion-heart":"&#xf141;","ion-heart-broken":"&#xf31d;","ion-help":"&#xf143;","ion-help-buoy":"&#xf27c;","ion-help-circled":"&#xf142;","ion-home":"&#xf144;","ion-icecream":"&#xf27d;","ion-image":"&#xf147;","ion-images":"&#xf148;","ion-information":"&#xf14a;","ion-information-circled":"&#xf149;","ion-ionic":"&#xf14b;","ion-ios-alarm":"&#xf3c8;","ion-ios-alarm-outline":"&#xf3c7;","ion-ios-albums":"&#xf3ca;","ion-ios-albums-outline":"&#xf3c9;","ion-ios-americanfootball":"&#xf3cc;","ion-ios-americanfootball-outline":"&#xf3cb;","ion-ios-analytics":"&#xf3ce;","ion-ios-analytics-outline":"&#xf3cd;","ion-ios-arrow-back":"&#xf3cf;","ion-ios-arrow-down":"&#xf3d0;","ion-ios-arrow-forward":"&#xf3d1;","ion-ios-arrow-left":"&#xf3d2;","ion-ios-arrow-right":"&#xf3d3;","ion-ios-arrow-thin-down":"&#xf3d4;","ion-ios-arrow-thin-left":"&#xf3d5;","ion-ios-arrow-thin-right":"&#xf3d6;","ion-ios-arrow-thin-up":"&#xf3d7;","ion-ios-arrow-up":"&#xf3d8;","ion-ios-at":"&#xf3da;","ion-ios-at-outline":"&#xf3d9;","ion-ios-barcode":"&#xf3dc;","ion-ios-barcode-outline":"&#xf3db;","ion-ios-baseball":"&#xf3de;","ion-ios-baseball-outline":"&#xf3dd;","ion-ios-basketball":"&#xf3e0;","ion-ios-basketball-outline":"&#xf3df;","ion-ios-bell":"&#xf3e2;","ion-ios-bell-outline":"&#xf3e1;","ion-ios-body":"&#xf3e4;","ion-ios-body-outline":"&#xf3e3;","ion-ios-bolt":"&#xf3e6;","ion-ios-bolt-outline":"&#xf3e5;","ion-ios-book":"&#xf3e8;","ion-ios-book-outline":"&#xf3e7;","ion-ios-bookmarks":"&#xf3ea;","ion-ios-bookmarks-outline":"&#xf3e9;","ion-ios-box":"&#xf3ec;","ion-ios-box-outline":"&#xf3eb;","ion-ios-briefcase":"&#xf3ee;","ion-ios-briefcase-outline":"&#xf3ed;","ion-ios-browsers":"&#xf3f0;","ion-ios-browsers-outline":"&#xf3ef;","ion-ios-calculator":"&#xf3f2;","ion-ios-calculator-outline":"&#xf3f1;","ion-ios-calendar":"&#xf3f4;","ion-ios-calendar-outline":"&#xf3f3;","ion-ios-camera":"&#xf3f6;","ion-ios-camera-outline":"&#xf3f5;","ion-ios-cart":"&#xf3f8;","ion-ios-cart-outline":"&#xf3f7;","ion-ios-chatboxes":"&#xf3fa;","ion-ios-chatboxes-outline":"&#xf3f9;","ion-ios-chatbubble":"&#xf3fc;","ion-ios-chatbubble-outline":"&#xf3fb;","ion-ios-checkmark":"&#xf3ff;","ion-ios-checkmark-empty":"&#xf3fd;","ion-ios-checkmark-outline":"&#xf3fe;","ion-ios-circle-filled":"&#xf400;","ion-ios-circle-outline":"&#xf401;","ion-ios-clock":"&#xf403;","ion-ios-clock-outline":"&#xf402;","ion-ios-close":"&#xf406;","ion-ios-close-empty":"&#xf404;","ion-ios-close-outline":"&#xf405;","ion-ios-cloud":"&#xf40c;","ion-ios-cloud-download":"&#xf408;","ion-ios-cloud-download-outline":"&#xf407;","ion-ios-cloud-outline":"&#xf409;","ion-ios-cloud-upload":"&#xf40b;","ion-ios-cloud-upload-outline":"&#xf40a;","ion-ios-cloudy":"&#xf410;","ion-ios-cloudy-night":"&#xf40e;","ion-ios-cloudy-night-outline":"&#xf40d;","ion-ios-cloudy-outline":"&#xf40f;","ion-ios-cog":"&#xf412;","ion-ios-cog-outline":"&#xf411;","ion-ios-color-filter":"&#xf414;","ion-ios-color-filter-outline":"&#xf413;","ion-ios-color-wand":"&#xf416;","ion-ios-color-wand-outline":"&#xf415;","ion-ios-compose":"&#xf418;","ion-ios-compose-outline":"&#xf417;","ion-ios-contact":"&#xf41a;","ion-ios-contact-outline":"&#xf419;","ion-ios-copy":"&#xf41c;","ion-ios-copy-outline":"&#xf41b;","ion-ios-crop":"&#xf41e;","ion-ios-crop-strong":"&#xf41d;","ion-ios-download":"&#xf420;","ion-ios-download-outline":"&#xf41f;","ion-ios-drag":"&#xf421;","ion-ios-email":"&#xf423;","ion-ios-email-outline":"&#xf422;","ion-ios-eye":"&#xf425;","ion-ios-eye-outline":"&#xf424;","ion-ios-fastforward":"&#xf427;","ion-ios-fastforward-outline":"&#xf426;","ion-ios-filing":"&#xf429;","ion-ios-filing-outline":"&#xf428;","ion-ios-film":"&#xf42b;","ion-ios-film-outline":"&#xf42a;","ion-ios-flag":"&#xf42d;","ion-ios-flag-outline":"&#xf42c;","ion-ios-flame":"&#xf42f;","ion-ios-flame-outline":"&#xf42e;","ion-ios-flask":"&#xf431;","ion-ios-flask-outline":"&#xf430;","ion-ios-flower":"&#xf433;","ion-ios-flower-outline":"&#xf432;","ion-ios-folder":"&#xf435;","ion-ios-folder-outline":"&#xf434;","ion-ios-football":"&#xf437;","ion-ios-football-outline":"&#xf436;","ion-ios-game-controller-a":"&#xf439;","ion-ios-game-controller-a-outline":"&#xf438;","ion-ios-game-controller-b":"&#xf43b;","ion-ios-game-controller-b-outline":"&#xf43a;","ion-ios-gear":"&#xf43d;","ion-ios-gear-outline":"&#xf43c;","ion-ios-glasses":"&#xf43f;","ion-ios-glasses-outline":"&#xf43e;","ion-ios-grid-view":"&#xf441;","ion-ios-grid-view-outline":"&#xf440;","ion-ios-heart":"&#xf443;","ion-ios-heart-outline":"&#xf442;","ion-ios-help":"&#xf446;","ion-ios-help-empty":"&#xf444;","ion-ios-help-outline":"&#xf445;","ion-ios-home":"&#xf448;","ion-ios-home-outline":"&#xf447;","ion-ios-infinite":"&#xf44a;","ion-ios-infinite-outline":"&#xf449;","ion-ios-information":"&#xf44d;","ion-ios-information-empty":"&#xf44b;","ion-ios-information-outline":"&#xf44c;","ion-ios-ionic-outline":"&#xf44e;","ion-ios-keypad":"&#xf450;","ion-ios-keypad-outline":"&#xf44f;","ion-ios-lightbulb":"&#xf452;","ion-ios-lightbulb-outline":"&#xf451;","ion-ios-list":"&#xf454;","ion-ios-list-outline":"&#xf453;","ion-ios-location":"&#xf456;","ion-ios-location-outline":"&#xf455;","ion-ios-locked":"&#xf458;","ion-ios-locked-outline":"&#xf457;","ion-ios-loop":"&#xf45a;","ion-ios-loop-strong":"&#xf459;","ion-ios-medical":"&#xf45c;","ion-ios-medical-outline":"&#xf45b;","ion-ios-medkit":"&#xf45e;","ion-ios-medkit-outline":"&#xf45d;","ion-ios-mic":"&#xf461;","ion-ios-mic-off":"&#xf45f;","ion-ios-mic-outline":"&#xf460;","ion-ios-minus":"&#xf464;","ion-ios-minus-empty":"&#xf462;","ion-ios-minus-outline":"&#xf463;","ion-ios-monitor":"&#xf466;","ion-ios-monitor-outline":"&#xf465;","ion-ios-moon":"&#xf468;","ion-ios-moon-outline":"&#xf467;","ion-ios-more":"&#xf46a;","ion-ios-more-outline":"&#xf469;","ion-ios-musical-note":"&#xf46b;","ion-ios-musical-notes":"&#xf46c;","ion-ios-navigate":"&#xf46e;","ion-ios-navigate-outline":"&#xf46d;","ion-ios-nutrition":"&#xf470;","ion-ios-nutrition-outline":"&#xf46f;","ion-ios-paper":"&#xf472;","ion-ios-paper-outline":"&#xf471;","ion-ios-paperplane":"&#xf474;","ion-ios-paperplane-outline":"&#xf473;","ion-ios-partlysunny":"&#xf476;","ion-ios-partlysunny-outline":"&#xf475;","ion-ios-pause":"&#xf478;","ion-ios-pause-outline":"&#xf477;","ion-ios-paw":"&#xf47a;","ion-ios-paw-outline":"&#xf479;","ion-ios-people":"&#xf47c;","ion-ios-people-outline":"&#xf47b;","ion-ios-person":"&#xf47e;","ion-ios-person-outline":"&#xf47d;","ion-ios-personadd":"&#xf480;","ion-ios-personadd-outline":"&#xf47f;","ion-ios-photos":"&#xf482;","ion-ios-photos-outline":"&#xf481;","ion-ios-pie":"&#xf484;","ion-ios-pie-outline":"&#xf483;","ion-ios-pint":"&#xf486;","ion-ios-pint-outline":"&#xf485;","ion-ios-play":"&#xf488;","ion-ios-play-outline":"&#xf487;","ion-ios-plus":"&#xf48b;","ion-ios-plus-empty":"&#xf489;","ion-ios-plus-outline":"&#xf48a;","ion-ios-pricetag":"&#xf48d;","ion-ios-pricetag-outline":"&#xf48c;","ion-ios-pricetags":"&#xf48f;","ion-ios-pricetags-outline":"&#xf48e;","ion-ios-printer":"&#xf491;","ion-ios-printer-outline":"&#xf490;","ion-ios-pulse":"&#xf493;","ion-ios-pulse-strong":"&#xf492;","ion-ios-rainy":"&#xf495;","ion-ios-rainy-outline":"&#xf494;","ion-ios-recording":"&#xf497;","ion-ios-recording-outline":"&#xf496;","ion-ios-redo":"&#xf499;","ion-ios-redo-outline":"&#xf498;","ion-ios-refresh":"&#xf49c;","ion-ios-refresh-empty":"&#xf49a;","ion-ios-refresh-outline":"&#xf49b;","ion-ios-reload":"&#xf49d;","ion-ios-reverse-camera":"&#xf49f;","ion-ios-reverse-camera-outline":"&#xf49e;","ion-ios-rewind":"&#xf4a1;","ion-ios-rewind-outline":"&#xf4a0;","ion-ios-rose":"&#xf4a3;","ion-ios-rose-outline":"&#xf4a2;","ion-ios-search":"&#xf4a5;","ion-ios-search-strong":"&#xf4a4;","ion-ios-settings":"&#xf4a7;","ion-ios-settings-strong":"&#xf4a6;","ion-ios-shuffle":"&#xf4a9;","ion-ios-shuffle-strong":"&#xf4a8;","ion-ios-skipbackward":"&#xf4ab;","ion-ios-skipbackward-outline":"&#xf4aa;","ion-ios-skipforward":"&#xf4ad;","ion-ios-skipforward-outline":"&#xf4ac;","ion-ios-snowy":"&#xf4ae;","ion-ios-speedometer":"&#xf4b0;","ion-ios-speedometer-outline":"&#xf4af;","ion-ios-star":"&#xf4b3;","ion-ios-star-half":"&#xf4b1;","ion-ios-star-outline":"&#xf4b2;","ion-ios-stopwatch":"&#xf4b5;","ion-ios-stopwatch-outline":"&#xf4b4;","ion-ios-sunny":"&#xf4b7;","ion-ios-sunny-outline":"&#xf4b6;","ion-ios-telephone":"&#xf4b9;","ion-ios-telephone-outline":"&#xf4b8;","ion-ios-tennisball":"&#xf4bb;","ion-ios-tennisball-outline":"&#xf4ba;","ion-ios-thunderstorm":"&#xf4bd;","ion-ios-thunderstorm-outline":"&#xf4bc;","ion-ios-time":"&#xf4bf;","ion-ios-time-outline":"&#xf4be;","ion-ios-timer":"&#xf4c1;","ion-ios-timer-outline":"&#xf4c0;","ion-ios-toggle":"&#xf4c3;","ion-ios-toggle-outline":"&#xf4c2;","ion-ios-trash":"&#xf4c5;","ion-ios-trash-outline":"&#xf4c4;","ion-ios-undo":"&#xf4c7;","ion-ios-undo-outline":"&#xf4c6;","ion-ios-unlocked":"&#xf4c9;","ion-ios-unlocked-outline":"&#xf4c8;","ion-ios-upload":"&#xf4cb;","ion-ios-upload-outline":"&#xf4ca;","ion-ios-videocam":"&#xf4cd;","ion-ios-videocam-outline":"&#xf4cc;","ion-ios-volume-high":"&#xf4ce;","ion-ios-volume-low":"&#xf4cf;","ion-ios-wineglass":"&#xf4d1;","ion-ios-wineglass-outline":"&#xf4d0;","ion-ios-world":"&#xf4d3;","ion-ios-world-outline":"&#xf4d2;","ion-ipad":"&#xf1f9;","ion-iphone":"&#xf1fa;","ion-ipod":"&#xf1fb;","ion-jet":"&#xf295;","ion-key":"&#xf296;","ion-knife":"&#xf297;","ion-laptop":"&#xf1fc;","ion-leaf":"&#xf1fd;","ion-levels":"&#xf298;","ion-lightbulb":"&#xf299;","ion-link":"&#xf1fe;","ion-load-a":"&#xf29a;","ion-load-b":"&#xf29b;","ion-load-c":"&#xf29c;","ion-load-d":"&#xf29d;","ion-location":"&#xf1ff;","ion-lock-combination":"&#xf4d4;","ion-locked":"&#xf200;","ion-log-in":"&#xf29e;","ion-log-out":"&#xf29f;","ion-loop":"&#xf201;","ion-magnet":"&#xf2a0;","ion-male":"&#xf2a1;","ion-man":"&#xf202;","ion-map":"&#xf203;","ion-medkit":"&#xf2a2;","ion-merge":"&#xf33f;","ion-mic-a":"&#xf204;","ion-mic-b":"&#xf205;","ion-mic-c":"&#xf206;","ion-minus":"&#xf209;","ion-minus-circled":"&#xf207;","ion-minus-round":"&#xf208;","ion-model-s":"&#xf2c1;","ion-monitor":"&#xf20a;","ion-more":"&#xf20b;","ion-mouse":"&#xf340;","ion-music-note":"&#xf20c;","ion-navicon":"&#xf20e;","ion-navicon-round":"&#xf20d;","ion-navigate":"&#xf2a3;","ion-network":"&#xf341;","ion-no-smoking":"&#xf2c2;","ion-nuclear":"&#xf2a4;","ion-outlet":"&#xf342;","ion-paintbrush":"&#xf4d5;","ion-paintbucket":"&#xf4d6;","ion-paper-airplane":"&#xf2c3;","ion-paperclip":"&#xf20f;","ion-pause":"&#xf210;","ion-person":"&#xf213;","ion-person-add":"&#xf211;","ion-person-stalker":"&#xf212;","ion-pie-graph":"&#xf2a5;","ion-pin":"&#xf2a6;","ion-pinpoint":"&#xf2a7;","ion-pizza":"&#xf2a8;","ion-plane":"&#xf214;","ion-planet":"&#xf343;","ion-play":"&#xf215;","ion-playstation":"&#xf30a;","ion-plus":"&#xf218;","ion-plus-circled":"&#xf216;","ion-plus-round":"&#xf217;","ion-podium":"&#xf344;","ion-pound":"&#xf219;","ion-power":"&#xf2a9;","ion-pricetag":"&#xf2aa;","ion-pricetags":"&#xf2ab;","ion-printer":"&#xf21a;","ion-pull-request":"&#xf345;","ion-qr-scanner":"&#xf346;","ion-quote":"&#xf347;","ion-radio-waves":"&#xf2ac;","ion-record":"&#xf21b;","ion-refresh":"&#xf21c;","ion-reply":"&#xf21e;","ion-reply-all":"&#xf21d;","ion-ribbon-a":"&#xf348;","ion-ribbon-b":"&#xf349;","ion-sad":"&#xf34a;","ion-sad-outline":"&#xf4d7;","ion-scissors":"&#xf34b;","ion-search":"&#xf21f;","ion-settings":"&#xf2ad;","ion-share":"&#xf220;","ion-shuffle":"&#xf221;","ion-skip-backward":"&#xf222;","ion-skip-forward":"&#xf223;","ion-social-android":"&#xf225;","ion-social-android-outline":"&#xf224;","ion-social-angular":"&#xf4d9;","ion-social-angular-outline":"&#xf4d8;","ion-social-apple":"&#xf227;","ion-social-apple-outline":"&#xf226;","ion-social-bitcoin":"&#xf2af;","ion-social-bitcoin-outline":"&#xf2ae;","ion-social-buffer":"&#xf229;","ion-social-buffer-outline":"&#xf228;","ion-social-chrome":"&#xf4db;","ion-social-chrome-outline":"&#xf4da;","ion-social-codepen":"&#xf4dd;","ion-social-codepen-outline":"&#xf4dc;","ion-social-css3":"&#xf4df;","ion-social-css3-outline":"&#xf4de;","ion-social-designernews":"&#xf22b;","ion-social-designernews-outline":"&#xf22a;","ion-social-dribbble":"&#xf22d;","ion-social-dribbble-outline":"&#xf22c;","ion-social-dropbox":"&#xf22f;","ion-social-dropbox-outline":"&#xf22e;","ion-social-euro":"&#xf4e1;","ion-social-euro-outline":"&#xf4e0;","ion-social-facebook":"&#xf231;","ion-social-facebook-outline":"&#xf230;","ion-social-foursquare":"&#xf34d;","ion-social-foursquare-outline":"&#xf34c;","ion-social-freebsd-devil":"&#xf2c4;","ion-social-github":"&#xf233;","ion-social-github-outline":"&#xf232;","ion-social-google":"&#xf34f;","ion-social-google-outline":"&#xf34e;","ion-social-googleplus":"&#xf235;","ion-social-googleplus-outline":"&#xf234;","ion-social-hackernews":"&#xf237;","ion-social-hackernews-outline":"&#xf236;","ion-social-html5":"&#xf4e3;","ion-social-html5-outline":"&#xf4e2;","ion-social-instagram":"&#xf351;","ion-social-instagram-outline":"&#xf350;","ion-social-javascript":"&#xf4e5;","ion-social-javascript-outline":"&#xf4e4;","ion-social-linkedin":"&#xf239;","ion-social-linkedin-outline":"&#xf238;","ion-social-markdown":"&#xf4e6;","ion-social-nodejs":"&#xf4e7;","ion-social-octocat":"&#xf4e8;","ion-social-pinterest":"&#xf2b1;","ion-social-pinterest-outline":"&#xf2b0;","ion-social-python":"&#xf4e9;","ion-social-reddit":"&#xf23b;","ion-social-reddit-outline":"&#xf23a;","ion-social-rss":"&#xf23d;","ion-social-rss-outline":"&#xf23c;","ion-social-sass":"&#xf4ea;","ion-social-skype":"&#xf23f;","ion-social-skype-outline":"&#xf23e;","ion-social-snapchat":"&#xf4ec;","ion-social-snapchat-outline":"&#xf4eb;","ion-social-tumblr":"&#xf241;","ion-social-tumblr-outline":"&#xf240;","ion-social-tux":"&#xf2c5;","ion-social-twitch":"&#xf4ee;","ion-social-twitch-outline":"&#xf4ed;","ion-social-twitter":"&#xf243;","ion-social-twitter-outline":"&#xf242;","ion-social-usd":"&#xf353;","ion-social-usd-outline":"&#xf352;","ion-social-vimeo":"&#xf245;","ion-social-vimeo-outline":"&#xf244;","ion-social-whatsapp":"&#xf4f0;","ion-social-whatsapp-outline":"&#xf4ef;","ion-social-windows":"&#xf247;","ion-social-windows-outline":"&#xf246;","ion-social-wordpress":"&#xf249;","ion-social-wordpress-outline":"&#xf248;","ion-social-yahoo":"&#xf24b;","ion-social-yahoo-outline":"&#xf24a;","ion-social-yen":"&#xf4f2;","ion-social-yen-outline":"&#xf4f1;","ion-social-youtube":"&#xf24d;","ion-social-youtube-outline":"&#xf24c;","ion-soup-can":"&#xf4f4;","ion-soup-can-outline":"&#xf4f3;","ion-speakerphone":"&#xf2b2;","ion-speedometer":"&#xf2b3;","ion-spoon":"&#xf2b4;","ion-star":"&#xf24e;","ion-stats-bars":"&#xf2b5;","ion-steam":"&#xf30b;","ion-stop":"&#xf24f;","ion-thermometer":"&#xf2b6;","ion-thumbsdown":"&#xf250;","ion-thumbsup":"&#xf251;","ion-toggle":"&#xf355;","ion-toggle-filled":"&#xf354;","ion-transgender":"&#xf4f5;","ion-trash-a":"&#xf252;","ion-trash-b":"&#xf253;","ion-trophy":"&#xf356;","ion-tshirt":"&#xf4f7;","ion-tshirt-outline":"&#xf4f6;","ion-umbrella":"&#xf2b7;","ion-university":"&#xf357;","ion-unlocked":"&#xf254;","ion-upload":"&#xf255;","ion-usb":"&#xf2b8;","ion-videocamera":"&#xf256;","ion-volume-high":"&#xf257;","ion-volume-low":"&#xf258;","ion-volume-medium":"&#xf259;","ion-volume-mute":"&#xf25a;","ion-wand":"&#xf358;","ion-waterdrop":"&#xf25b;","ion-wifi":"&#xf25c;","ion-wineglass":"&#xf2b9;","ion-woman":"&#xf25d;","ion-wrench":"&#xf2ba;","ion-xbox":"&#xf30c;"}

/***/ }),
/* 126 */,
/* 127 */,
/* 128 */,
/* 129 */
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(268)
)

/* script */
__vue_exports__ = __webpack_require__(201)

/* template */
var __vue_template__ = __webpack_require__(304)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "/Users/james/Desktop/creader/cread/wx-creader/src/04.views/aboutCheckIn.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-7e7b7bc0"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__


/***/ }),
/* 130 */,
/* 131 */,
/* 132 */,
/* 133 */,
/* 134 */,
/* 135 */,
/* 136 */,
/* 137 */,
/* 138 */,
/* 139 */,
/* 140 */,
/* 141 */,
/* 142 */,
/* 143 */,
/* 144 */,
/* 145 */,
/* 146 */,
/* 147 */,
/* 148 */,
/* 149 */,
/* 150 */,
/* 151 */,
/* 152 */,
/* 153 */,
/* 154 */,
/* 155 */,
/* 156 */,
/* 157 */,
/* 158 */,
/* 159 */,
/* 160 */,
/* 161 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var App = __webpack_require__(129);
var storeEntry = __webpack_require__(24);
Vue.component('storeEntry', storeEntry);
var LoadingDots = __webpack_require__(17);
Vue.component('LoadingDots', LoadingDots);
var videoPlay = __webpack_require__(34);
Vue.component('videoPlay', videoPlay);
var webView = __webpack_require__(37);
Vue.component('webView', webView);
var networkError = __webpack_require__(27);
Vue.component('networkError', networkError);
var topbar = __webpack_require__(33);
Vue.component('topbar', topbar);
var commonFun = __webpack_require__(14);
Vue.component('commonFun', commonFun);
var weextabbar = __webpack_require__(38);
Vue.component('weextabbar', weextabbar);
var libraryNav = __webpack_require__(23);
Vue.component('libraryNav', libraryNav);
var libraryBookShelf = __webpack_require__(21);
Vue.component('libraryBookShelf', libraryBookShelf);
var searchBar = __webpack_require__(29);
Vue.component('searchBar', searchBar);
var netError = __webpack_require__(26);
Vue.component('netError', netError);
var loadingCircle = __webpack_require__(25);
Vue.component('loadingCircle', loadingCircle);
var progress = __webpack_require__(28);
Vue.component('progress', progress);
var weexDialogue = __webpack_require__(35);
Vue.component('weexDialogue', weexDialogue);
var weexOverlay = __webpack_require__(36);
Vue.component('weexOverlay', weexOverlay);
var defaultImage = __webpack_require__(16);
Vue.component('defaultImage', defaultImage);
var submitting = __webpack_require__(30);
Vue.component('submitting', submitting);
var chat = __webpack_require__(13);
Vue.component('chat', chat);
var libraryBook = __webpack_require__(19);
Vue.component('libraryBook', libraryBook);
var libraryCategory = __webpack_require__(22);
Vue.component('libraryCategory', libraryCategory);
var libraryBookLists = __webpack_require__(20);
Vue.component('libraryBookLists', libraryBookLists);
var crIconFont = __webpack_require__(15);
Vue.component('crIconFont', crIconFont);
var topNavigationWidget = __webpack_require__(31);
Vue.component('topNavigationWidget', topNavigationWidget);
var topNavigationWidgetWithBack = __webpack_require__(32);
Vue.component('topNavigationWidgetWithBack', topNavigationWidgetWithBack);
var followAlbum = __webpack_require__(18);
Vue.component('followAlbum', followAlbum);
App.el = '#root';
new Vue(App);

/***/ }),
/* 162 */,
/* 163 */,
/* 164 */,
/* 165 */,
/* 166 */,
/* 167 */,
/* 168 */,
/* 169 */,
/* 170 */,
/* 171 */,
/* 172 */,
/* 173 */,
/* 174 */,
/* 175 */,
/* 176 */,
/* 177 */,
/* 178 */,
/* 179 */,
/* 180 */,
/* 181 */,
/* 182 */,
/* 183 */,
/* 184 */,
/* 185 */,
/* 186 */,
/* 187 */,
/* 188 */,
/* 189 */,
/* 190 */,
/* 191 */,
/* 192 */,
/* 193 */,
/* 194 */,
/* 195 */,
/* 196 */,
/* 197 */,
/* 198 */,
/* 199 */,
/* 200 */,
/* 201 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _navigator = __webpack_require__(0);

exports.default = {
    data: function data() {
        return {
            height: '',
            url: ''
        };
    },
    created: function created() {
        var deviceHeight = WXEnvironment.deviceHeight || unknown;
        var deviceWidth = WXEnvironment.deviceWidth || unknown;
        this.height = 750 / deviceWidth * deviceHeight;
        this.url = "http://read.biblemooc.net/display/index/aboutCheckIn";
    },
    methods: {
        goBack: function goBack() {
            (0, _navigator.back)();
        }
    }

}; //
//
//
//
//
//
//
//

/***/ }),
/* 202 */,
/* 203 */,
/* 204 */,
/* 205 */,
/* 206 */,
/* 207 */,
/* 208 */,
/* 209 */,
/* 210 */,
/* 211 */,
/* 212 */,
/* 213 */,
/* 214 */,
/* 215 */,
/* 216 */,
/* 217 */,
/* 218 */,
/* 219 */,
/* 220 */,
/* 221 */,
/* 222 */,
/* 223 */,
/* 224 */,
/* 225 */,
/* 226 */,
/* 227 */,
/* 228 */,
/* 229 */,
/* 230 */,
/* 231 */,
/* 232 */,
/* 233 */,
/* 234 */,
/* 235 */,
/* 236 */,
/* 237 */,
/* 238 */,
/* 239 */,
/* 240 */,
/* 241 */,
/* 242 */,
/* 243 */,
/* 244 */,
/* 245 */,
/* 246 */,
/* 247 */,
/* 248 */,
/* 249 */,
/* 250 */,
/* 251 */,
/* 252 */,
/* 253 */,
/* 254 */,
/* 255 */,
/* 256 */,
/* 257 */,
/* 258 */,
/* 259 */,
/* 260 */,
/* 261 */,
/* 262 */,
/* 263 */,
/* 264 */,
/* 265 */,
/* 266 */,
/* 267 */,
/* 268 */
/***/ (function(module, exports) {

module.exports = {
  "body": {
    "width": "750"
  },
  "web": {
    "position": "absolute",
    "width": "750",
    "top": "100"
  }
}

/***/ }),
/* 269 */,
/* 270 */,
/* 271 */,
/* 272 */,
/* 273 */,
/* 274 */,
/* 275 */,
/* 276 */,
/* 277 */,
/* 278 */,
/* 279 */,
/* 280 */,
/* 281 */,
/* 282 */,
/* 283 */,
/* 284 */,
/* 285 */,
/* 286 */,
/* 287 */,
/* 288 */,
/* 289 */,
/* 290 */,
/* 291 */,
/* 292 */,
/* 293 */,
/* 294 */,
/* 295 */,
/* 296 */,
/* 297 */,
/* 298 */,
/* 299 */,
/* 300 */,
/* 301 */,
/* 302 */,
/* 303 */,
/* 304 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: ["body"],
    style: {
      height: _vm.height
    }
  }, [_c('topbar', {
    attrs: {
      "topbarname": _vm.feedback
    }
  }), _c('webView', {
    ref: "webview",
    staticClass: ["web"],
    style: {
      height: _vm.height
    },
    attrs: {
      "src": _vm.url
    }
  }), _c('commonFun', {
    ref: "commonFun"
  })], 1)
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ })
/******/ ]);